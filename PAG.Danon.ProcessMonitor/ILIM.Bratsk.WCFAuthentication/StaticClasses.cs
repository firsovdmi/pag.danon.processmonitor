﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.WCFAuthentication
{
    static class StaticClasses
    {

        public static T GetValue<T>(this SqlDataReader reader, string fieldName, T defaultVal = default(T))
        {
            return reader.IsDBNull(reader.GetOrdinal(fieldName))
                ? defaultVal
                : (T)Convert.ChangeType(reader[fieldName], typeof(T));
        }

        public static T GetValue<T>(this SqlDataReader reader, int fieldNumber, T defaultVal = default(T))
        {
            return reader.IsDBNull(fieldNumber) ? defaultVal : (T)Convert.ChangeType(reader[fieldNumber], typeof(T));
        }
    }
}
