﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Authentication;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Xml;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;
using PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses;
using System.IdentityModel.Policy;


namespace PAG.WBD.Milk.WCFAuthentication
{
    public class MessageModifierServer : System.ServiceModel.Dispatcher.IDispatchMessageInspector
    {
        private Dictionary<int, MessageMem> _dictManagedThreadIdBad = new Dictionary<int, MessageMem>();

        [ThreadStatic]
        private static MessageMem memMsg;

        class MessageMem
        {
            public MessageMem(UniqueId messageId, string action)
            {
                MessageId = messageId;
                Action = action;
            }
            public CheckLoginResult LoginResult { get; set; }
            public UniqueId MessageId { get; set; }
            public string Action { get; set; }
        }

        #region IDispatchMessageInspector Members

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            memMsg = new MessageMem(request.Headers.MessageId, request.Headers.Action);
            IaaaManager.Instance.ContextMember = GetOperationName();
            memMsg.LoginResult = IsLoginOk(request);
            if (!memMsg.LoginResult.IsOk)
            {
                request = null;
            }
            else
            {
                IaaaManager.Instance.ContextLogin = memMsg.LoginResult.LoginItem;
            }
            return null;
        }

        private static CheckLoginResult IsLoginOk(Message msg)
        {
            var ret = new CheckLoginResult();
            ret.IsOk = false;
            ret.FaultType = FaultDetailTypes.Unknown;
            if (msg == null) return ret;
            int index = msg.Headers.FindHeader("Token", "http://my.namespace.com");
            if (index < 0)
            {
                ret.FaultType = FaultDetailTypes.NotLogined;
                return ret;
            }
            var contextToken = msg.Headers.GetHeader<Guid>(index);
            return IaaaManager.Instance.AuthenticatiorServerContract.CheckLogin(contextToken, IaaaManager.Instance.ContextMember);
        }


        ManualResetEvent _mre = new ManualResetEvent(true);
        object _dictLock = new object();

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            if (memMsg == null) return;
            if (memMsg.LoginResult.IsOk)
            {
                IaaaManager.Instance.AuthenticatiorServerContract.AddLog(IaaaManager.Instance.ContextLogin.Id, IaaaManager.Instance.ContextMember);
                return;
            }
            var fault = new FaultException<FaultDetailTypes>(memMsg.LoginResult.FaultType, new FaultReason(new FaultReasonText(memMsg.LoginResult.Message ?? "Ошибка авторизации"))).CreateMessageFault();
            reply = Message.CreateMessage(reply.Version, fault, memMsg.Action);
            reply.Headers.RelatesTo = memMsg.MessageId;
        }

        #endregion

        private static string GetOperationName()
        {
            return string.Join(".", OperationContext.Current.IncomingMessageHeaders.Action.Split('/').Skip(3));
        }
    }
}
