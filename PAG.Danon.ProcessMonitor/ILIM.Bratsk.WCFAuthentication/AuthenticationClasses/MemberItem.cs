﻿using System;
using System.Collections.Generic;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;
using WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    [Serializable]
    public class MemberItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CustomName { get; set; }
        public string Description { get; set; }
        public List<AcessGroup> AcessGroups { get; set; }
        public MembersClass MemberClass
        {
            get { return _memberClass; }
            set { _memberClass = value; }
        }
        private MembersClass _memberClass=new MembersClass();

        public string FullName
        {
            get { return MemberClass.Name + "." + Name; }
        }

        public bool IsShared { get; set; }
    }
}
