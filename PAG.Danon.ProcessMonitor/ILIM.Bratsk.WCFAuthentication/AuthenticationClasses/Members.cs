﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;
using WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    [Serializable]
    [KnownType(typeof(MemberItem))]
    [KnownType(typeof(AcessGroup))]
    public class Members : IEnumerable<MemberItem>
    {
        private List<MemberItem> _items = new List<MemberItem>();

        public Members()
        {

        }

        public Members(IEnumerable<MemberItem> members)
        {
            _items = members.ToList();
        }

        public void Add(MemberItem newItem)
        {
            _items.Add(newItem);
        }

        public List<AcessGroup> GetAcessGroupsByName(string name)
        {
            var item = _items.FirstOrDefault(p => p.FullName == name);
            if (item == null) return new List<AcessGroup>();
            return item.AcessGroups;
        }

        public bool CheckAcess(string memderName, IEnumerable<AcessGroup> acessGroups)
        {
            return GetAcessGroupsByName(memderName).Any(group => acessGroups.Any(p => p.Id == group.Id));
        }

        public int GetIdByName(string name, int defultId = 0)
        {
            var item = _items.FirstOrDefault(p => p.FullName == name);
            if (item != null) return item.Id;
            return defultId;
        }

        public IEnumerable<MemberItem> GetAllowedMembers(IEnumerable<AcessGroup> acessGroups)
        {
            return _items.Where(item => CheckAcess(item.FullName, acessGroups));
        }

        public IEnumerator<MemberItem> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }
    }
}
