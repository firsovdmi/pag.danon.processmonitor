﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    [Serializable]
    public class AuthenticationResult
    {
        public bool IsSucces { get; internal set; }
        public string Message { get; internal set; }
        internal  Guid Token { get; set; }
        public List<string> AllowedMembers { get; set; }
        public FullLoginInfo LoginInfo { get; set; }
    }
}
