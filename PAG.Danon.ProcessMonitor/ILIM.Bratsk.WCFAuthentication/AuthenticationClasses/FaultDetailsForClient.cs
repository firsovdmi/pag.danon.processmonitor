﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{
    public enum FaultDetailTypes
    {
        NotLogined,
        AcessDeny,
        Unknown
    }
}
