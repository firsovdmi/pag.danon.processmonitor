﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using PAG.WBD.Milk.Domain.Infrastructure.AAA;

namespace PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses
{


    public class IaaaManager : Domain.Infrastructure.AAA.IAaaManager
    {

        public  ActiveLoginItem ContextLogin { get; set; }

        public  string ContextMember { get; set; }

        public LoginInfo LastLogin { get; set; }

        #region Singletone Implementation

        private static object lockFlag = new object();

        /// <summary>
        /// Экземпляр синглтона
        /// </summary>
        private static IaaaManager instance = null;

        /// <summary>
        /// Get аксессор к экземпляру синглтона
        /// </summary>
        public static IaaaManager Instance
        {
            get
            {
                if (instance != null)
                    return instance;

                lock (lockFlag)
                {
                    instance = new IaaaManager();
                    return instance;
                }
            }
        }

        private IaaaManager()
        {
            var filePath = Path.GetTempPath() + @"\lodt.sys";
            if (File.Exists(filePath))
            {
                var loginData = File.ReadAllLines(filePath);
                if (loginData.Length == 2)
                {
                    LastLogin = new LoginInfo
                    {
                        Login = loginData[0],
                        Password = loginData[1]
                    };
                }
            }
        }

        /// <summary>
        /// Cброс коллекции
        /// </summary>
        public static void ResetInstance()
        {
            instance = null;
        }


        #endregion

        public IAuthenticatiorService AuthenticatiorServerContract { get; set; }
        public AuthenticationResult LoginSession { get; private set; }

        public AuthenticationResult Login(LoginInfo loginInfo)
        {
            CheckLoginServerInterface();
            LoginSession = AuthenticatiorServerContract.Login(loginInfo);
            if (LoginSession != null)
            {
                SaveLastLogin(loginInfo);
                LastLogin = loginInfo;
            }
            return LoginSession;
        }

        private void SaveLastLogin(LoginInfo loginInfo)
        {
            var filePath = Path.GetTempPath() + @"\lodt.sys";
            File.WriteAllText(filePath, loginInfo.Login + Environment.NewLine);
            File.AppendAllText(filePath, loginInfo.Password);
        }

        public AuthenticationResult Logoff()
        {
            CheckLoginServerInterface();
            LoginSession = AuthenticatiorServerContract.Logoff(LoginSession);
            return LoginSession;
        }

        private void CheckLoginServerInterface()
        {
            if (AuthenticatiorServerContract == null) throw new NullReferenceException("Не проинициализирован интерфес \"Login Server\"");
        }
    }
}
