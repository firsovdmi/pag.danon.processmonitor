﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace PAG.WBD.Milk.WCFAuthentication.MessageInterceptor
{

    public class ServiceMessageInspectorBehavior : Attribute, IServiceBehavior, IEndpointBehavior
    {
        #region Члены IEndpointBehavior

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.ClientMessageInspectors.Add(new MessageModifierClient());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }

        #endregion

        #region Члены IServiceBehavior

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher cDispatcher in serviceHostBase.ChannelDispatchers)
                foreach (EndpointDispatcher eDispatcher in cDispatcher.Endpoints)
                {
                    eDispatcher.DispatchRuntime.MessageInspectors.Add(new MessageModifierServer());
                    eDispatcher.DispatchRuntime.CallbackClientRuntime.ClientMessageInspectors.Add(new MessageModifierClient());
                }
        }

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        #endregion
    }


    public class InterceptorBehaviorExtension : BehaviorExtensionElement, IEndpointBehavior
    {
        public override System.Type BehaviorType
        {
            get { return typeof(InterceptorBehaviorExtension); }
        }

        protected override object CreateBehavior()
        {
            return new InterceptorBehaviorExtension();
        }

        public void AddBindingParameters(ServiceEndpoint endpoint,
               System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint,
               System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new MessageModifierClient());
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint,
               System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
        {

        }

        public void Validate(ServiceEndpoint endpoint)
        {

        }
    }

}
