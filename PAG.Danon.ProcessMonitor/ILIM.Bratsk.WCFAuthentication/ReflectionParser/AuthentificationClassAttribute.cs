﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.WCFAuthentication.ReflectionParser
{
    public class AuthentificationClassAttribute: Attribute
    {
        public string Name { get; set; }
        public string Description { get; set; }


        public AuthentificationClassAttribute(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public AuthentificationClassAttribute(string name) : this(name, "") { }
        public AuthentificationClassAttribute() : this("", "") { }
    }
}
