﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.WCFAuthentication
{
    public class MessageModifierClient : IClientMessageInspector
    {
        public MessageModifierClient()
        {
            
        }
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

            request.Headers.Add(MessageHeader.CreateHeader("Token", "http://my.namespace.com", IaaaManager.Instance.LoginSession == null ? Guid.Empty : IaaaManager.Instance.LoginSession.Token));
            return null;
        }

    }
}
