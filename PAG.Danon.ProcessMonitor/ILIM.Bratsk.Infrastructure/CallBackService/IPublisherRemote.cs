﻿using System.ServiceModel;

namespace PAG.Danon.ProcessMonitor.Infrastructure.CallBackService
{
    [ServiceContract]
    public interface IPublisherRemote
    {
        [OperationContract(IsOneWay = true)]
        void Subscribe();

        [OperationContract(IsOneWay = true)]
        void UnSubscribe();
    }
}