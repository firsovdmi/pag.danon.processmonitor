﻿using System.Collections.Generic;

namespace PAG.Danon.ProcessMonitor.Infrastructure.CallBackService.MilkReceiveData
{
    public interface IReceivingTaskPublisher
    {
        void Publish(List<ReceivingTaskItem> items);
        void Subscribe(IReceivingTaskSubscriber ReceivingTaskSubscriber);
        void UnSubscribe(IReceivingTaskSubscriber ReceivingTaskSubscriber);
    }
}