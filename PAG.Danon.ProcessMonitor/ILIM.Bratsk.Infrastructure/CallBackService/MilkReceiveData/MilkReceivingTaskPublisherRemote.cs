﻿using System.ServiceModel;

namespace PAG.Danon.ProcessMonitor.Infrastructure.CallBackService.MilkReceiveData
{
    public class ReceivingTaskPublisherRemote : IReceivingTaskPublisherRemote
    {
        private readonly IReceivingTaskPublisher _ReceivingTaskPublisher;

        public ReceivingTaskPublisherRemote(IReceivingTaskPublisher ReceivingTaskPublisher)
        {
            _ReceivingTaskPublisher = ReceivingTaskPublisher;
        }

        #region Implementation of IReceivingTaskRemotePublisher

        public void Subscribe()
        {
            _ReceivingTaskPublisher.Subscribe(OperationContext.Current.GetCallbackChannel<IReceivingTaskSubscriber>());
        }

        public void UnSubscribe()
        {
            _ReceivingTaskPublisher.UnSubscribe(OperationContext.Current.GetCallbackChannel<IReceivingTaskSubscriber>());
        }

        #endregion
    }
}