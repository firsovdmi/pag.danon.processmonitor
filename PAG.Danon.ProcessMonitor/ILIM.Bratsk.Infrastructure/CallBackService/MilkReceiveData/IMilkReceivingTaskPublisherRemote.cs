﻿using System.ServiceModel;

namespace PAG.Danon.ProcessMonitor.Infrastructure.CallBackService.MilkReceiveData
{
    [ServiceContract(CallbackContract = typeof (IReceivingTaskSubscriber))]
    public interface IReceivingTaskPublisherRemote
    {
        [OperationContract(IsOneWay = true)]
        void Subscribe();

        [OperationContract(IsOneWay = true)]
        void UnSubscribe();
    }
}