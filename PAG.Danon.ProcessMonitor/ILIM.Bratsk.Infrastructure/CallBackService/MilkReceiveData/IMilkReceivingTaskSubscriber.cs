﻿using System.Collections.Generic;
using System.ServiceModel;

namespace PAG.Danon.ProcessMonitor.Infrastructure.CallBackService.MilkReceiveData
{
    [ServiceContract]
    public interface IReceivingTaskSubscriber
    {
        [OperationContract(IsOneWay = true)]
        void Publish(List<ReceivingTaskItem> ReceivingTaskitems);

        [OperationContract]
        void CheckSubscriber();
    }
}