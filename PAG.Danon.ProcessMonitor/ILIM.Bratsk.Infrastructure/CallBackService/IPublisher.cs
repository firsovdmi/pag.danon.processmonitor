﻿using System.Collections.Generic;

namespace PAG.Danon.ProcessMonitor.Infrastructure.CallBackService
{
    public interface IPublisher<T>
    {
        void Publish(List<T> items);
        void Subscribe(ISubscriber<T> logSubscriber);
        void UnSubscribe(ISubscriber<T> logSubscriber);
    }
}