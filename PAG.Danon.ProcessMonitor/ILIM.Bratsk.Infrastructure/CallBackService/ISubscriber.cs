﻿using System.Collections.Generic;
using System.ServiceModel;

namespace PAG.Danon.ProcessMonitor.Infrastructure.CallBackService
{
    [ServiceContract]
    public interface ISubscriber<T>
    {
        [OperationContract(IsOneWay = true)]
        void Publish(List<T> items);

        [OperationContract]
        void CheckSubscriber();
    }
}