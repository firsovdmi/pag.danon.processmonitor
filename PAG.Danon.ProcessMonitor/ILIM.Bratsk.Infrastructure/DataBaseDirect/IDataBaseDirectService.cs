﻿using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;

namespace PAG.Danon.ProcessMonitor.Infrastructure.DataBaseDirect
{
    [ServiceContract]
    public interface IDataBaseDirectService
    {
        [OperationContract]
        bool AddPlan(string batchCode, int packFirst, int packLast, PrinterModeEnum empt);
        [OperationContract]
        bool EditPlan(string batchCode, int packFirst, int packLast, PrinterModeEnum empt);
        [OperationContract]
        bool Stop();
    }
}