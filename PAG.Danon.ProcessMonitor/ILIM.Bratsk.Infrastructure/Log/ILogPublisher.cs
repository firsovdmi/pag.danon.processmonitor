﻿using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Infrastructure.Log.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Log
{
    public interface ILogPublisher
    {
        void Publish(List<LogItem> items);
        void Subscribe(ILogSubscriber logSubscriber);
        void UnSubscribe(ILogSubscriber logSubscriber);
    }
}