﻿using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Infrastructure.Log.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Log
{
    public interface IReadLogService
    {
        List<LogItem> GetLog(LogRequestParameters logRequestParameters);
    }
}