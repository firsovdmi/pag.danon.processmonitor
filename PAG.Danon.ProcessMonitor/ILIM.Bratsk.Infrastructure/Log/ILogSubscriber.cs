using System.Collections.Generic;
using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Infrastructure.Log.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Log
{
    [ServiceContract]
    public interface ILogSubscriber
    {
        [OperationContract(IsOneWay = true)]
        void Publish(List<LogItem> items);

        [OperationContract]
        void CheckSubscriber();
    }
}