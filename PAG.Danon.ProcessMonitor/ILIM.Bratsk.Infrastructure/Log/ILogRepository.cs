﻿using System.Linq;
using PAG.Danon.ProcessMonitor.Infrastructure.Log.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Log
{
    public interface ILogRepository
    {
        IQueryable<LogItem> Get();
        void Delete(LogItem logItem);
        LogItem Update(LogItem logItem);
        void Delete(IQueryable<LogItem> items);
    }
}