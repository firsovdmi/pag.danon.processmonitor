using System.ServiceModel;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Log
{
    [ServiceContract(CallbackContract = typeof (ILogSubscriber))]
    public interface ILogPublisherRemote
    {
        [OperationContract(IsOneWay = true)]
        void Subscribe();

        [OperationContract(IsOneWay = true)]
        void UnSubscribe();
    }
}