using System.Linq;
using PAG.Danon.ProcessMonitor.Infrastructure.Log.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Log
{
    public interface IUnitOfWorkLog : IUnitOfWork
    {
        IQueryable<LogItem> Get();
        void Delete(LogItem entity);
        LogItem Create(LogItem entity);
        LogItem Update(LogItem entity);
        void DeleteBatch(IQueryable<LogItem> items);
    }
}