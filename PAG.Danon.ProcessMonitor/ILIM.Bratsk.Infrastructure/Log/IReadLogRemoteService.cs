﻿using System.Collections.Generic;
using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Infrastructure.Log.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Log
{
    [ServiceContract]
    public interface IReadLogRemoteService
    {
        [OperationContract]
        List<LogItem> GetLog(LogRequestParameters logRequestParameters);
    }
}