using System.Xml;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA
{
    internal sealed class MessageMem
    {
        public MessageMem(UniqueId messageId, string action)
        {
            MessageId = messageId;
            Action = action;
        }

        public AutorizationResult LoginResult { get; set; }
        public UniqueId MessageId { get; set; }
        public string Action { get; set; }
    }
}