namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA
{
    public enum FaultsCodes
    {
        AutorizationFault,
        ConcurencyFault
    }
}