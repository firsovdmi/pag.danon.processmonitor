using System;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA
{
    public interface IUnitOfWorkAAAFactory : IDisposable
    {
        IUnitOfWorkAAA Create();
        void Release(IUnitOfWorkAAA unitOfWork);
    }
}