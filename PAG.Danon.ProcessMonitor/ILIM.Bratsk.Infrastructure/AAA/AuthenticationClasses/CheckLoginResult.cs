﻿namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses
{
    public class CheckLoginResult
    {
        public ActiveAccount ActiveAccount { get; set; }
        public AutorizationResult AutorizationResult { get; set; }
    }
}