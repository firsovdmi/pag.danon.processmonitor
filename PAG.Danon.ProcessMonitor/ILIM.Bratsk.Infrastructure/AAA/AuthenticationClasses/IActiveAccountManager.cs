using System.Collections.Generic;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses
{
    public interface IActiveAccountManager
    {
        //Guid Token { get; }
        ActiveAccount ActiveAccount { get; set; }
        List<string> WhiteListObjects { get; set; }
        List<string> BlackListObjects { get; set; }
    }
}