﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses
{
    [DataContract]
    public class AuthenticationResult
    {
        [DataMember]
        public bool IsSucces { get; internal set; }

        [DataMember]
        public string Message { get; internal set; }

        [DataMember]
        public List<string> AllowedObjects { get; set; }

        [DataMember]
        public List<string> DeniedObjects { get; set; }

        [DataMember]
        public ActiveAccount AccountInfo { get; set; }
    }
}