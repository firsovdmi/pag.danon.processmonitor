﻿using System;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses
{
    public class ActiveAccount
    {
        public Account Account { get; set; }
        public DateTime LastActionTime { get; set; }
        public Guid Token { get; set; }
        public string MachineName { get; set; }
        public virtual bool IsExpire
        {
            get
            {
                return Account.AutoLogOffTimeMinutes != 0 &&
                       (DateTime.Now - LastActionTime).TotalMinutes > Account.AutoLogOffTimeMinutes;
            }
        }
    }
}