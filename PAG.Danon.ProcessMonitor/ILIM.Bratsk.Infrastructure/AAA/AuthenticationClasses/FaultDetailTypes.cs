﻿using System.Runtime.Serialization;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses
{
    [DataContract]
    public enum AutorizationResult
    {
        [EnumMember] Success,
        [EnumMember] NotLogined,
        [EnumMember] AcessDeny,
        [EnumMember] Unknown,
        [EnumMember] AccountIsExpire
    }
}