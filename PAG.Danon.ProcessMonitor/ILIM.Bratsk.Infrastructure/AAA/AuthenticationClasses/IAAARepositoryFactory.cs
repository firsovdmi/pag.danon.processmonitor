namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses
{
    public interface IAAARepositoryFactory
    {
        IAAARepository Create(IUnitOfWorkAAA uow);
        void Release(object obj);
    }
}