﻿using System;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses
{
    public class AnonimusActiveAccount : ActiveAccount
    {
        public AnonimusActiveAccount()
        {
            Account = new Account();
            LoginTime = DateTime.Now;
            Token = new Guid();
            Account.AutoLogOffTimeMinutes = 999999;
            Account.Login = "Anonimus";
        }

        public DateTime LoginTime { get; set; }
        public Guid Token { get; set; }

        public override bool IsExpire
        {
            get { return false; }
        }
    }
}