using System.Collections.Generic;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses
{
    public class ActiveAcountManagerClient : IActiveAccountManager
    {
        public ActiveAcountManagerClient()
        {
            ActiveAccount = new AnonimusActiveAccount();
        }

        public ActiveAccount ActiveAccount { get; set; }
        public List<string> WhiteListObjects { get; set; }
        public List<string> BlackListObjects { get; set; }
    }
}