using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.MapConfig
{
    public class AccountMapConfiguration : EntityMapConfiguration<Account>
    {
        private const string TableName = "Account";

        public AccountMapConfiguration()
        {
            Property(p => p.Login)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute {IsUnique = true}));
            HasMany(p => p.AccountInRoles).WithRequired(p => p.Account);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}