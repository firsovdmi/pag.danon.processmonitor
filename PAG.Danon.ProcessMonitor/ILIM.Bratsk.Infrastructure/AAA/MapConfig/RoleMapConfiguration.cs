using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.MapConfig
{
    public class RoleMapConfiguration : EntityMapConfiguration<Role>
    {
        private const string TableName = "Role";

        public RoleMapConfiguration()
        {
            Property(p => p.Name)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute {IsUnique = true}));
            HasMany(p => p.AuthorizeObjectsForRoles).WithRequired(p => p.Role);
            HasMany(p => p.UserInRoles).WithRequired(p => p.Role);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}