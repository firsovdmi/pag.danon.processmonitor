using PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.MapConfig
{
    public class AccounAuthenticationMapConfiguration : EntityBaseMapConfiguration<AccounAuthentication>
    {
        private const string TableName = "AccounAuthentication";

        public AccounAuthenticationMapConfiguration()
        {
            HasRequired(a => a.Account).WithRequiredDependent(a => a.AccounAuthentication);
            Map(p => { p.ToTable(TableName); });
        }
    }
}