using System.Collections;
using System.ServiceModel.Channels;
using Castle.Facilities.WcfIntegration.Behaviors;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA
{
    public class LifestyleClientMessageAction : AbstractMessageAction
    {
        private readonly IActiveAccountManager _manager;

        public LifestyleClientMessageAction(IActiveAccountManager manager)
            : base(MessageLifecycle.All)
        {
            _manager = manager;
        }

        public override bool Perform(ref Message message, MessageLifecycle lifecycle, IDictionary state)
        {
            if (lifecycle == MessageLifecycle.IncomingResponse)
            {
            }
            if (lifecycle == MessageLifecycle.OutgoingRequest)
            {
                message.Headers.Add(MessageHeader.CreateHeader("Token", "http://pagru.com", _manager.ActiveAccount.Token));
                return true;
            }


            return true;
        }
    }
}