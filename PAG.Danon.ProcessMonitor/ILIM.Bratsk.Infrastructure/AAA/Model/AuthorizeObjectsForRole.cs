using System;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model
{
    [DataContract]
    public class AuthorizeObjectsForRole : Entity
    {
        [DataMember]
        public Guid AuthorizeObjectID { get; set; }

        [DataMember]
        public AuthorizeObject AuthorizeObject { get; set; }

        [DataMember]
        public Guid RoleID { get; set; }

        [DataMember]
        public Role Role { get; set; }
    }
}