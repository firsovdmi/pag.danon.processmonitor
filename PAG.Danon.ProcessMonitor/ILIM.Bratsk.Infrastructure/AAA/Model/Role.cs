using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model
{
    public class Role : Entity
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "������� ���")]
        [StringLength(80, ErrorMessage = "��� �� ����� ���� ������ 80 ��������")]
        public override string Name
        {
            get { return _name; }
            set
            {
                if (value == _name) return;
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public virtual List<AccountInRole> UserInRoles { get; set; }
        public virtual List<AuthorizeObjectsForRole> AuthorizeObjectsForRoles { get; set; }
    }
}