using System.Collections.Generic;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model
{
    [DataContract]
    public class AuthorizeObject : EntityBase
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public List<AuthorizeObjectsForRole> AuthorizeObjectsForRoles { get; set; }
    }
}