using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model
{
    [DataContract]
    public class AccounAuthentication : EntityBase
    {
        [DataMember]
        public string PasswordHash { get; set; }

        [DataMember]
        public Account Account { get; set; }
    }
}