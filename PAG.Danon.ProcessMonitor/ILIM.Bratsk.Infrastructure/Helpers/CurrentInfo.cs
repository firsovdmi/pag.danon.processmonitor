﻿using System;
using System.Runtime.Serialization;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Helpers
{
    [DataContract]
    public class CurrentInfo
    {
        [DataMember]
        public string CurrentBatchCode { get; set; }
        [DataMember]
        public PrinterModeEnum CurrentEmpt { get; set; }
        [DataMember]
        public DateTime? CurrentMinTime { get; set; }
        [DataMember]
        public int CurrentMinPuckNum { get; set; }
        [DataMember]
        public int CurrentMaxPuckNum { get; set; }
        [DataMember]
        public int CurrentPuckNumCount { get; set; }
        [DataMember]
        public int CurrentPuckNumComplete { get; set; }
        [DataMember]
        public int CurrentPuckNumRemain { get; set; }
        [DataMember]
        public int CurrentProgress { get; set; }

    }
}
