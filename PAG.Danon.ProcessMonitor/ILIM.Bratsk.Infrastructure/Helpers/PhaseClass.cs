﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Helpers
{
    public enum PhaseClass

    {
        [Description ("xferout")]
        xferout,
        [Description("xferin")]
        xferIn,
        [Description("prod")]
        prod
     
    }
}
