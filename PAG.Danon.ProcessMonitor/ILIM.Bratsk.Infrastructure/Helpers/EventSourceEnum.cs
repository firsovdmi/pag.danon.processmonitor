﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Helpers
{
    public enum EventSourceEnum
    {
        [Description("Не известно (0)")]
        [EnumMember]
        Uncknow = 0,
        [Description("система Проконт")]
        [EnumMember]
        Procont = 1,
        [Description("панель оператора системы Профипринт")]
        [EnumMember]
        ARM = 2,
        [Description("сервисное сообщение системы Профипринт")]
        [EnumMember]
        Server = 3,
    }
}
