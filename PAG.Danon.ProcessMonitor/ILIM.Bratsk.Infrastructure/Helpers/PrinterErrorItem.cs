﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Helpers
{
    [DataContract]
    public class PrinterErrorItem
    {

        [DataMember]
        public string InstalationName { get; set; }

        public List<PrinterMessage> ErrorList
        {
            get { return _errorList ?? (_errorList = new List<PrinterMessage>()); }
            set { _errorList = value; }
        }

        [DataMember]
        private List<PrinterMessage> _errorList;


    }
    [DataContract]
    public class PrinterMessage
    {
        [DataMember]
        private static readonly List<string> CriticalErrors = new List<string>
            {
                "missing",
                "broken",
                "inkEmpty"
            };
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public bool IsCriticalError
        {
            get { return CriticalErrors.Contains(Text); }
        }
    }
}
