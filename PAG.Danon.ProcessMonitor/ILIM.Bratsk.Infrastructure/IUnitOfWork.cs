﻿namespace PAG.Danon.ProcessMonitor.Infrastructure
{
    public interface IUnitOfWork
    {
        void Save();
    }
}