﻿using System.Runtime.Serialization;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Model
{
    [DataContract]
    public class PlcShortEntity : ShortEntity
    {
        [DataMember] private int _plcId;

        public int PlcId
        {
            get { return _plcId; }
            set
            {
                if (value.Equals(_plcId)) return;
                _plcId = value;
                OnPropertyChanged("PlcId");
            }
        }
    }
}