﻿namespace PAG.Danon.ProcessMonitor.Infrastructure.Model
{
    public interface IMarkableForDelete
    {
        bool IsDeleted { get; set; }
    }
}