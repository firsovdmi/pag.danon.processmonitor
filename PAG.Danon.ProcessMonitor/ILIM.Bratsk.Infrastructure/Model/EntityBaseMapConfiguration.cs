using System.Data.Entity.ModelConfiguration;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Model
{
    public class EntityBaseMapConfiguration<T> : EntityTypeConfiguration<T> where T : EntityBase
    {
        private const string TableName = "EntityBase";

        public EntityBaseMapConfiguration()
        {
            HasKey(p => p.ID);
            //Map(p =>
            //{
            //    p.MapInheritedProperties();
            //    p.ToTable(TableName);
            //});
        }
    }
}