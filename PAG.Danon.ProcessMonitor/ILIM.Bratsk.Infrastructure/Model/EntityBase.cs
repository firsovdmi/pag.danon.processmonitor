﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Model
{
    [DataContract(IsReference = true)]
    [Serializable]
    public class EntityBase : IEntity, INotifyPropertyChanged
    {
        [DataMember] private Guid _id;

        #region Implementation of IEntity

        public Guid ID
        {
            get { return _id; }
            set { _id = value; }
        }

        #endregion

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}