﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Model
{
    [DataContract]
    public class PlcNEntity : Entity
    {
        [DataMember]
        public int PlcN { get; set; }
    }
}
