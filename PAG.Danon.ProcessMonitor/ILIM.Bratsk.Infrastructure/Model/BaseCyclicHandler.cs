﻿using System;
using System.Threading;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Model
{
    public abstract class BaseCyclicHandler
    {
        protected Timer Timer;
        protected bool FOff = false;
        protected readonly object Lock = new object();
        protected int ScanIntervalInMilliseconds;

        public void StartScan()
        {
            Initialize();
            Timer = new Timer(Callback, null, ScanIntervalInMilliseconds, Timeout.Infinite);
        }

        public void StopScan()
        {
            lock (Lock)
            {
                FOff = true;
                Timer.Change(Timeout.Infinite, Timeout.Infinite);
            }
        }

        private void Callback(Object state)
        {
            try
            {
                Handler();
            }
            catch (Exception ex)
            {
                
            }

            lock (Lock)
            {
                if (!FOff) Timer.Change(ScanIntervalInMilliseconds, Timeout.Infinite);
            }
        }

        protected abstract void Handler();

        protected virtual void Initialize()
        {
        }
    }
}
