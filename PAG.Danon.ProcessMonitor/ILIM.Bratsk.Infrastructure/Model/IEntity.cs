﻿using System;

namespace PAG.Danon.ProcessMonitor.Infrastructure.Model
{
    public interface IEntity
    {
        Guid ID { get; set; }
    }
}