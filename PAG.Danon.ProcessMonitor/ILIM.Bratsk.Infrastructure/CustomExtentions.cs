﻿namespace PAG.Danon.ProcessMonitor.Infrastructure
{
    public static class CustomExtentions
    {
        public static bool IsNumeric(this string s)
        {
            float output;
            return float.TryParse(s, out output);
        }
    }
}
