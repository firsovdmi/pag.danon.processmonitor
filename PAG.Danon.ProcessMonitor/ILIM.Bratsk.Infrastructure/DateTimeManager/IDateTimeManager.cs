using System;

namespace PAG.Danon.ProcessMonitor.Infrastructure.DateTimeManager
{
    public interface IDateTimeManager
    {
        DateTime GetDateTimeNow();
    }
}