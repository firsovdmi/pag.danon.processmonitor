using System;

namespace PAG.Danon.ProcessMonitor.Infrastructure.DateTimeManager
{
    public class DateTimeManager : IDateTimeManager
    {
        public DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }
    }
}