using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Domain
{
    public interface IModule
    {
    }

    public interface IRepositoryFactory
    {
        T Create<T>(IUnitOfWork unitOfWork) where T : IRepository;
        void Release(object obj);
    }
}