﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IMXODetailsRepository : IRepositoryWithMarkedAsDelete<MXODetails>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}