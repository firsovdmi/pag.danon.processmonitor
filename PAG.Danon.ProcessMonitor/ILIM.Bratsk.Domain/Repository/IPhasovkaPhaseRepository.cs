﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IPhasovkaPhaseRepository : IRepositoryWithMarkedAsDelete<PhasovkaPhase>
    {
    }
}
