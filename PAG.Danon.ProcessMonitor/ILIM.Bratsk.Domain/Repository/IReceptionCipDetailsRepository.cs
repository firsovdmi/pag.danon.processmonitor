﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IReceptionCipDetailsRepository : IRepositoryWithMarkedAsDelete<ReceptionCipDetail>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}