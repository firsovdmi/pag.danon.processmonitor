﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IPhasovkaCipDetailsRepository : IRepositoryWithMarkedAsDelete<PhasovkaCipDetails>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}