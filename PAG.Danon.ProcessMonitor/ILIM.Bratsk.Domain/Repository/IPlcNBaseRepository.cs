﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IPlcNBaseRepository<T>: IRepositoryWithMarkedAsDelete<T> where T : IEntity
    {
        T GetItemByPlcN(int plcN);
    }
}
