﻿namespace PAG.WBD.Milk.Domain.Repository
{
    public interface IUnitOfWork
    {
        void Save();
    }
}