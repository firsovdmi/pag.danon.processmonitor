﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface ICheeseCipDetailsRepository : IRepositoryWithMarkedAsDelete<CheeseCipDetail>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}