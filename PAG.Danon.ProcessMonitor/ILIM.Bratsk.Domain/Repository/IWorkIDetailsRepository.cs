﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IWorkDetailsRepository : IRepositoryWithMarkedAsDelete<WorkDetail>
    {
        WorkDetail GetLastItem(int plcIWorkID);
    }
}