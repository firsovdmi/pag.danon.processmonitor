using System.Linq;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IRepositoryEntity<T> : IRepository where T : IEntity
    {
        IQueryable<T> Get();
        IQueryable<T> GetTracked();
        T Create(T entity);
        T Update(T entity);
        void Delete(T entity);
        T CreateOrUpdate(T entity);
    }
}