﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IMXOCipDetailsRepository : IRepositoryWithMarkedAsDelete<MXOCipDetail>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}