﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IProcessReportItemsRepository : IRepositoryWithMarkedAsDelete<ProcessReportItem>
    {
        ProcessReportItem GetLastItem(int workID);
    }
}