﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IReceptionDetailsRepository : IRepositoryWithMarkedAsDelete<ReceptionDetails>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}