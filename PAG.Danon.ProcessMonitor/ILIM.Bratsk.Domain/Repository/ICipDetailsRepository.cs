﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface ICipDetailsRepository : IRepositoryWithMarkedAsDelete<CipDetail>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}