using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IRepositoryWithMarkedAsDelete<T> : IRepositoryEntity<T> where T : IEntity
    {
        T MarkAsDelete(T entity);
    }

    public interface IRepository
    {
    }
}