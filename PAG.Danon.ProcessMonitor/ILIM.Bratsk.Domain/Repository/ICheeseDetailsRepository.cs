﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface ICheeseDetailsRepository : IRepositoryWithMarkedAsDelete<CheesenDetails>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}