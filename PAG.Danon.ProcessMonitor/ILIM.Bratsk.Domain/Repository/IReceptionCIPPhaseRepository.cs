﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IReceptionCIPPhaseRepository : IRepositoryWithMarkedAsDelete<ReceptionCIPPhase>
    {
    }
}
