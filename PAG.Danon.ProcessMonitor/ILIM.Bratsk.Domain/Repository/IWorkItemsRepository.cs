﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IWorkItemsRepository : IRepositoryWithMarkedAsDelete<WorkItem>
    {
        WorkItem GetLastItem(int workID);
    }
}