﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IPhasovkaDetailsRepository : IRepositoryWithMarkedAsDelete<PhasovkaDetails>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}