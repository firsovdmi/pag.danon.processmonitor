﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IAppDetailsRepository : IRepositoryWithMarkedAsDelete<AppDetails>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}