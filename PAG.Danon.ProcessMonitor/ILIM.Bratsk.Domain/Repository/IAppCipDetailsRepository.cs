﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface IAppCipDetailsRepository : IRepositoryWithMarkedAsDelete<AppCipDetail>
    {
       // CipDetail GetLastItem(int plcIWorkID);
    }
}