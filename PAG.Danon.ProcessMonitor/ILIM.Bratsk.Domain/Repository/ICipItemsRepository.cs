﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface ICipItemsRepository : IRepositoryWithMarkedAsDelete<CipItem>
    {
        CipItem GetLastItem(int plcID);
    }
}