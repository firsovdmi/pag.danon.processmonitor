﻿using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Repository
{
    public interface ICheeseCIPPhaseRepository : IRepositoryWithMarkedAsDelete<CheeseCIPPhase>
    {
    }
}
