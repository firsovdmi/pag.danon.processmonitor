﻿using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;


public interface ICipConturRepository 
   : IRepositoryWithMarkedAsDelete<CipContur>
    {
    }

