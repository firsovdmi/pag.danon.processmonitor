﻿using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public class MXORequestParameters : INotifyPropertyChanged
    {

        [DataMember] public List<ShortEntity> Phases = new List<ShortEntity>();
        [DataMember] public List<string> XferIn = new List<string>();
        
        [DataMember]
        private ReportParameterPeriodSelect _period = new ReportParameterPeriodSelect();

        [DataMember]
        private bool _tanksIsSelected;

        public bool tanksIsSelected
        {
            get { return _tanksIsSelected; }
            set
            {
                if (value.Equals(_tanksIsSelected)) return;
                _tanksIsSelected = value;
                OnPropertyChanged("tanksIsSelected");
            }
        }



        [DataMember]
        private bool _phasesIsSelected;

        public bool phasesIsSelected
        {
            get { return _phasesIsSelected; }
            set
            {
                if (value.Equals(_phasesIsSelected)) return;
                _phasesIsSelected = value;
                OnPropertyChanged("phasesIsSelected");
            }
        }

        [DataMember]
        private bool _detailReport;

        public bool detailReport
        {
            get { return _detailReport; }
            set
            {
                if (value.Equals(_detailReport)) return;
                _detailReport = value;
                OnPropertyChanged("detailReport");
            }
        }


        [DataMember]
        public ReportParameterPeriodSelect Period
        {
            get { return _period; }
            set
            {
                if (value.Equals(_period)) return;
                _period = value;
                OnPropertyChanged("Period");
            }
        }




        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}