﻿using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
   public class CIPItems : Entity
    {
  
        //
        [DataMember]
        public Guid? PhaseID { get; set; }
        [DataMember]
        public Phase Phase { get; set; }


        [DataMember]
        public Guid? WorkMessageID { get; set; }
        [DataMember]
        public WorkMessage WorkMessage { get; set; }

        [DataMember]
        public Guid? PhaseStatusID { get; set; }
        [DataMember]
        public PhaseStatus PhaseStatus { get; set; }

        [DataMember]
        public Guid? EventInfoID { get; set; }
        [DataMember]
        public EventInfo EventInfo { get; set; }

        [DataMember]
        public Guid? OperatorID { get; set; }
        [DataMember]
        public Operator Operator { get; set; }

        [DataMember]
        public Guid? StepNoID { get; set; }
        [DataMember]
        public StepNoProd StepNoProd { get; set; }
        
        [DataMember]
        public Guid? UnitStatusID { get; set; }
        [DataMember]
        public UnitStatus UnitStatus { get; set; }        
        //
        [DataMember]
        public  int Ret_TEMP_MV_Lye{ get;set;}
        [DataMember]
        public  int Ret_CONC_MV_Lye { get; set; }
        [DataMember]
        public  int Ret_TEMP_MV_Acid { get; set; }
        [DataMember]
        public  int Ret_CONC_MV_Acid { get; set; }
        [DataMember]
        public  int Ret_TEMP_MV_HW { get; set; }
        [DataMember]
        public  int ALCIP_PrNo { get; set; }
        [DataMember]
        public  int Step_Time_Volume_Preset { get; set; }
        [DataMember]
        public  int Step_Time_Volume_Run { get; set; }



        [DataMember]
        public int WorkID { get; set; }



      








        [DataMember]
        public DateTime RecordDate { get; set; }
    }
}
