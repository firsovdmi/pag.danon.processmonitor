﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public enum RequestType
    {
        [Description("Процесс")]
        [EnumMember]
        Process,

        [Description("Процесс")]
        [EnumMember]
        CIP,

        [Description("Приемка")]
        [EnumMember]
        Reception,


        [Description("Приемка мойка")]
        [EnumMember]
        ReceptionCIP,


         [Description("MXO")]
        [EnumMember]
        MXO,


        [Description("МХО мойка")]
        [EnumMember]
        MXOCIP,


        [Description("Аппаратный")]
        [EnumMember]
        App,


        [Description("Аппаратный мойка")]
        [EnumMember]
        AppCIP,


        [Description("Творожный")]
        [EnumMember]
        Cheese,


        [Description("Творожный мойка")]
        [EnumMember]
        CheeseCIP

        }
}