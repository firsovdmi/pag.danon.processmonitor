﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public class WorkItem : Entity
    {
        [DataMember]
        public Guid? PhaseID { get; set; }
        [DataMember]
        public Phase Phase { get; set; }
        [DataMember]
        public int PlcItemID { get; set; }
        [DataMember]
        public Guid? ParentItemID { get; set; }
        [DataMember]
        public int PlcParentItemID { get; set; }
        [DataMember]
        public WorkItem ParentItem { get; set; }
        [DataMember]
        public WorkTypeEnum Type { get; set; }
        [DataMember]
        public List<WorkDetail> Details { get; set; }
        [DataMember]
        public DateTime PlcDateTime { get; set; }
        [DataMember]
        public DateTime MaxPhaseTime { get; set; }
        [DataMember]
        public bool IsAutoCreate { get; set; }
    }
}