﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    public class WriteBoolValue : WriteDirectJob
    {
        public WriteBoolValue(Adress adres, int dbn) : base(adres, dbn) { }
        public WriteBoolValue(Adress adres, int dbn, string id) : base(adres, dbn, id) { }

        [DataMember]
        public bool Value { get; set; }

        internal override VarEnum InteropOpcType
        {
            get { return System.Runtime.InteropServices.VarEnum.VT_BOOL; }
        }

        internal override void SetValue(object value)
        {
            Value = (bool)value;
        }

        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
