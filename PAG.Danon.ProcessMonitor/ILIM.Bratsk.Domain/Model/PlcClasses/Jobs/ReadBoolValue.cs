﻿using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    [KnownType(typeof(ReadDirectJob))]
    public class ReadBoolValue : ReadDirectJob
    {
        public ReadBoolValue(Adress adres, int dbn) : base(adres, dbn) { }
        public ReadBoolValue(Adress adres, int dbn, string id) : base(adres, dbn, id) { }

        [DataMember]
        public bool Value { get; internal set; }

        internal override void SetValue(object value)
        {
            Value = (bool)value;
        }

        internal override VarEnum InteropOpcType
        {
            get { return VarEnum.VT_BOOL; }
        }

        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
