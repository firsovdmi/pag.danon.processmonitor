﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    [KnownType(typeof(ReadDirectJob))]
    public class ReadDateTimeValue : ReadDirectJob
    {
        public ReadDateTimeValue(Adress adres, int dbn) : base(adres, dbn) { }
        public ReadDateTimeValue(Adress adres, int dbn, string id) : base(adres, dbn, id) { }

        [DataMember]
        public DateTime Value { get; internal set; }

        internal override void SetValue(object value)
        {
            Value = (DateTime)value;
        }

        internal override VarEnum InteropOpcType
        {
            get { return VarEnum.VT_DATE; }
        }

        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
