﻿using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;

namespace PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs.BaseClases
{
    [DataContract]
    [KnownType(typeof(WriteBoolValue))]
    [KnownType(typeof(WriteDIntValue))]
    [KnownType(typeof(WriteIntValue))]
    [KnownType(typeof(WriteRealValue))]
    [KnownType(typeof(WriteObjectValue))]
    public abstract class WriteDirectJob : BaseDirectJob
    {
        protected WriteDirectJob(Adress adres, int dbn) : base(adres, dbn) { }
        protected WriteDirectJob(Adress adres, int dbn, string id) : base(adres, dbn, id) { }
    }
}
