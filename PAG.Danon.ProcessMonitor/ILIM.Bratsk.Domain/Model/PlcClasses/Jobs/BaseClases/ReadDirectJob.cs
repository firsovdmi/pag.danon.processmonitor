﻿using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;

namespace PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs.BaseClases
{
    [DataContract]
    [KnownType(typeof(ReadBoolValue))]
    [KnownType(typeof(ReadDateTimeValue))]
    [KnownType(typeof(ReadDIntValue))]
    [KnownType(typeof(ReadIntValue))]
    [KnownType(typeof(ReadRealValue))]
    [KnownType(typeof(ReadObjectValue))]
    public abstract class ReadDirectJob : BaseDirectJob
    {
        protected ReadDirectJob(Adress adres, int dbn) : base(adres, dbn) { }
        protected ReadDirectJob(Adress adres, int dbn, string id) : base(adres, dbn, id) { }
    }
}
