﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs
{
    [DataContract]
    [KnownType(typeof(WriteDirectJob))]
    public class WriteIntValue : WriteDirectJob
    {
        public WriteIntValue(Adress adres, int dbn) : base(adres, dbn) { }
        public WriteIntValue(Adress adres, int dbn, string id) : base(adres, dbn, id) { }

        [DataMember]
        public Int16 Value { get; set; }

        internal override VarEnum InteropOpcType
        {
            get { return VarEnum.VT_I2; }
        }

        internal override void SetValue(object value)
        {
            Value = (Int16)value;
        }

        public override object GetValueAsObject()
        {
            return Value;
        }
    }
}
