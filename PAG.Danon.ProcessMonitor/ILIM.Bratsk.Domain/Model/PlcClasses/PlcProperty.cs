﻿using System;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses
{
    public class PlcProperty<T>
    {
        string Id => $"DB{Dbn}ADRESS{Adress}";
        public Adress Adress { get; set; }
        public int Dbn { get; set; }
        public PlcProperty(Adress adress, int dbn)
        {
            Adress = adress;
            Dbn = dbn;
        }
        public void FindValue(BatchDirectJobs jobs)
        {
            var val = jobs.GetElementById(Id);
            if (val == null) return;
            var data = val.GetValueAsObject();
            if (data == null) return;
            Value = (T)data;
        }

        ReadDirectJob _readJobs;
        public ReadDirectJob ReadJobs
        {
            get
            {
                if (_readJobs == null) _readJobs = new ReadObjectValue(Adress, Dbn, PlcTypeName) { Id = Id };
                return _readJobs;

            }
        }

        WriteDirectJob _writeJobs;
        public WriteDirectJob WriteJobs
        {
            get
            {
                if (_writeJobs == null) _writeJobs = new WriteObjectValue(Adress, Dbn, PlcTypeName);
                ((WriteObjectValue)_writeJobs).Value = Value;
                return _writeJobs;
            }
        }

        public T Value { get; set; }

        protected string PlcTypeName
        {
            get
            {
                if (Value is byte) return "BYTE";
                if (Value is Int16) return "INT";
                if (Value is Int32) return "DINT";
                if (Value is float) return "REAL";
                if (Value is bool) return "BOOL";
                if (Value is DateTime) return "DATE_AND_TIME";
                return Value.GetType().Name;
            }
        }
    }
}
