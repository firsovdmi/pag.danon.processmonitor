﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public class CIPRequestParameters : INotifyPropertyChanged
    {


        [DataMember]
        public List<ShortEntity> Conturs = new List<ShortEntity>();

        [DataMember]
        public  List<ShortEntity> Phases = new List<ShortEntity>();
        [DataMember]
        private ReportParameterPeriodSelect _period = new ReportParameterPeriodSelect();
        [DataMember]
        private bool _isWithActsOnly;

        public bool IsWithActsOnly
        {
            get { return _isWithActsOnly; }
            set
            {
                if (value.Equals(_isWithActsOnly)) return;
                _isWithActsOnly = value;
                OnPropertyChanged("IsWithActsOnly");
            }
        }

        

        [DataMember]
        private bool _isWithActsOnly1;

        public bool IsWithActsOnly1
        {
            get { return _isWithActsOnly1; }
            set
            {
                if (value.Equals(_isWithActsOnly1)) return;
                _isWithActsOnly1 = value;
                OnPropertyChanged("IsWithActsOnly1");
            }
        }

        public ReportParameterPeriodSelect Period
        {
            get { return _period; }
            set
            {
                if (value.Equals(_period)) return;
                _period = value;
                OnPropertyChanged("Period");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}