﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public  class CipDetail : Entity
    {

        [DataMember]
        public int IP { get; set; }

        [DataMember]
        public Guid? PhaseID { get; set; }
        [DataMember]
        public Phase Phase { get; set; }


        [DataMember]
        public  int WorkID { set; get; }


        [DataMember]
        public Guid? MessageID { get; set; }
        [DataMember]
        public WorkMessage Message { get; set; }

        [DataMember]
        public Guid? PhaseStatusID { get; set; }
        [DataMember]
        public PhaseStatus PhaseStatus { get; set; }

        [DataMember]
        public Guid? EventInfoID { get; set; }
        [DataMember]
        public EventInfo EventInfo { get; set; }

        [DataMember]
        public Guid? OperatorID { get; set; }
        [DataMember]
        public Operator Operator { get; set; }

        [DataMember]
        public Guid? StepNoCIPID { get; set; }
        [DataMember]
        public StepNoCip StepNoCIP { get; set; }

        [DataMember]
        public Guid? UnitStatusID { get; set; }
        [DataMember]
        public UnitStatus UnitStatus { get; set; }
        [DataMember]

        public int RetTEMPMVLye { get; set; }   // (температура щелочи на возврате)
        [DataMember]
        public int RetCONCMVLye { get; set; }   // (концентрация щелочи на возврате)
        [DataMember]
        public int RetTEMPMVAcid { get; set; }  // (температура кислоты на возврате)
        [DataMember]
        public int RetCONCMVAcid { get; set; }  // (концентрация кислоты на возврате)
        [DataMember]
        public int RetTEMPMVHW { get; set; }    // (температура горячей воды на возврате)
        [DataMember]
        public int RetCONCMV3 { get; set; }     // (резерв)
        [DataMember]
        public int ALCIPPrNo { get; set; }
        [DataMember]
        public int StepTimeVolumePreset { get; set; }
        [DataMember]
        public int StepTimeVolumeRun { get; set; }


        [DataMember]
        public DateTime RecordDate { get; set; }
        [DataMember]
        public DateTime RecordDateStart { get; set; }
        [DataMember]
        public DateTime RecordDateEnd { get; set; }


        [DataMember]
        public Guid? CipConturID { get; set; }
        [DataMember]
        public CipContur CipContur { get; set; }




        [DataMember]
        public List<CipDetail> ListSD { set; get; }
    }
}
