﻿using System;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public class AccountData : EntityBase
    {
        [DataMember]
        public Guid UserID { get; set; }

        [DataMember]
        public string DataID { get; set; }

        [DataMember]
        public string Data { get; set; }
    }
}