﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public class ProcessReportItem : Entity
    {
        [DataMember]
        public Phase phase;

        [DataMember]
        public WorkMessage workMessage;

        [DataMember]
        public PhaseStatus phaseStatus;

        [DataMember]
        public EventInfo eventInfo;

        [DataMember]
        public Operator operatorID;

        [DataMember]
        public StepNoProd stepNo;

        [DataMember]
        public UnitStatus unitStatus;

        [DataMember]
        public List<MyPLCBuffer> SourceDestions;



    }
}