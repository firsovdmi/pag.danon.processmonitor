﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public class PlcBuffer : Entity
    {
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int PlcIp { get; set; }
        [DataMember]
        public int PhaseID { get; set; }
        [DataMember]
        public int WorkID { get; set; }
        [DataMember]
        public int WorkID_SD { get; set; }
        [DataMember]
        public int PlcID { get; set; }
        [DataMember]
        public int PhaseStatus { get; set; }
        [DataMember]
        public int EventInfo { get; set; }
        [DataMember]
        public int OperatedID { get; set; }
        [DataMember]
        public int Step_No { get; set; }
        [DataMember]
        public int UnitStatus { get; set; }
        [DataMember]
        public bool Bool_0 { get; set; }
        [DataMember]
        public bool Bool_1 { get; set; }
        [DataMember]
        public bool Bool_2 { get; set; }
        [DataMember]
        public bool Bool_3 { get; set; }
        [DataMember]
        public bool Bool_4 { get; set; }
        [DataMember]
        public bool Bool_5 { get; set; }
        [DataMember]
        public bool Bool_6 { get; set; }
        [DataMember]
        public bool Bool_7 { get; set; }
        [DataMember]
        public int Byte_1 { get; set; }
        [DataMember]
        public int Int_1 { get; set; }
        [DataMember]
        public int Int_2 { get; set; }
        [DataMember]
        public int Int_3 { get; set; }
        [DataMember]
        public int Int_4 { get; set; }
        [DataMember]
        public int Int_5 { get; set; }
        [DataMember]
        public int Int_6 { get; set; }
        [DataMember]
        public int Int_7 { get; set; }
        [DataMember]
        public int Int_8 { get; set; }
        [DataMember]
        public int Int_9 { get; set; }
        [DataMember]
        public int Dint_1 { get; set; }
        [DataMember]
        public float Real_1 { get; set; }
        [DataMember]
        public float Real_2 { get; set; }
        [DataMember]
        public DateTime RecordDate { get; set; }
    }
}
