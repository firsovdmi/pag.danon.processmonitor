﻿using System;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Log;

namespace PAG.Danon.ProcessMonitor.Domain.Model.Alarms
{
    [Serializable]
    [DataContract]
    public class AlarmItem
    {
        public AlarmItem()
        {
            Id = Guid.NewGuid();
        }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public DateTime CreateTime { get; set; }
        [DataMember]
        public string MessageSource { get; set; }
        [DataMember]
        public LogingLevel AlarmLevel { get; set; }
        [DataMember]
        public bool IsAcknowledge { get; set; }
    }
}
