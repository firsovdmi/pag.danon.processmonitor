﻿using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Infrastructure.Log;

namespace PAG.Danon.ProcessMonitor.Domain.Model.Alarms
{
    public interface IAlarmCash
    {
        List<AlarmItem> Items { get; set; }
        bool AddItem(AlarmItem newItme);
        bool AddItem(string text, LogingLevel level, string messageSource);
        void RemoveItem(AlarmItem newItme);
    }
}
