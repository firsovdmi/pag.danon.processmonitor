﻿using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public class PhasovkaCIPRequestParameters : INotifyPropertyChanged
    {

        [DataMember] public List<ShortEntity> Phases = new List<ShortEntity>();
        
        [DataMember]
        private ReportParameterPeriodSelect _period = new ReportParameterPeriodSelect();

       

        [DataMember]
        public ReportParameterPeriodSelect Period
        {
            get { return _period; }
            set
            {
                if (value.Equals(_period)) return;
                _period = value;
                OnPropertyChanged("Period");
            }
        }




        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}