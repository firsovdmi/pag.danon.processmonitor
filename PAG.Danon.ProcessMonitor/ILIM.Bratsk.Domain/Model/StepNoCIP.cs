﻿using System;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [Serializable]
    [DataContract]
    public class StepNoCip : PlcNEntity
    {
        public int Ip { get; set; }
    }
}
