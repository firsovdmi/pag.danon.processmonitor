﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public  class WorkDetail : Entity
    {
        [DataMember]
        public Guid? PhaseID { get; set; }
        [DataMember]
        public Phase Phase { get; set; }
        [DataMember]
        public DateTime PlcDateTime { get; set; }
        [DataMember]
        public int PlcIWorkID { get; set; }
        [DataMember]
        public Guid? WorkItemId { get; set; }
        [DataMember]
        public WorkItem WorkItem { get; set; }
        [DataMember]
        public Guid? MessageID { get; set; }
        [DataMember]
        public WorkMessage Message { get; set; }
        [DataMember]
        public Guid? EventInfoID { get; set; }
        [DataMember]
        public EventInfo EventInfo { get; set; }
        [DataMember]
        public Guid? PhaseStatusID { get; set; }
        [DataMember]
        public PhaseStatus PhaseStatus { get; set; }
        [DataMember]
        public int OperatedID { get; set; }
        [DataMember]
        public Guid? StepNoProdID { get; set; }
        [DataMember]
        public StepNoProd StepNoProd { get; set; }
        [DataMember]
        public Guid? UnitStatusID { get; set; }
        [DataMember]
        public UnitStatus UnitStatus { get; set; }
        [DataMember]
        public int MaterialID { get; set; }
        [DataMember]
        public float TargetAmount { get; set; }
        [DataMember]
        public float ActualAmount { get; set; }
      
    }
}
