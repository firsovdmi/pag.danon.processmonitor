﻿using System;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [Serializable]
    [DataContract]
    public class ApplicationSettings : Entity
    {
        [DataMember]
        public string SystemName { get; set; }
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Value { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}
