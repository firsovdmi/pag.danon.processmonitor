﻿using System;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [Serializable]
    [DataContract]
    public class PhaseStatus : PlcNEntity
    {
        public int Ip { get; set; }
        
        public  string Description { get; set; }
    }
}
