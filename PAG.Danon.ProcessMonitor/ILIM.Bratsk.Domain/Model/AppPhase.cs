﻿using System;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [Serializable]
    [DataContract]
    public class AppPhase : PlcNEntity
    {
        [DataMember] PhaseType _type;

        public PhaseType Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                }
                OnPropertyChanged(nameof(Type));
            }
        }

        [DataMember]
        PhaseClass _class;

        public PhaseClass Class
        {
            get { return _class; }
            set
            {
                if (_class != value)
                {
                    _class = value;
                }
                OnPropertyChanged(nameof(Type));
            }
        }
        [DataMember]
        public int Ip { get; set; }

        [DataMember]
        private string _description;

        public string Description
        {
            get { return _description; }
            set
            {
                if (_description!= value)
                {
                    _description = value;
                }
                OnPropertyChanged(nameof(Type));
            } }

       
    }
}
