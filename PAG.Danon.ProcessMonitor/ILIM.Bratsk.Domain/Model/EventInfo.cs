﻿using System;
using System.Runtime.Serialization;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [Serializable]
    [DataContract]
    public class EventInfo : PlcNEntity
    {
        public int Ip { get; set; }
    }
}
