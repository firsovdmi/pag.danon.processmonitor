﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
    [DataContract]
    public enum PlanStatus
    {
        
        [Description("Печать выполнена")]
        [EnumMember]
        Queue,
        [Description("Отправлена на печать")]
        [EnumMember]
        SendedToPrint
    }
}
