﻿using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PAG.Danon.ProcessMonitor.Domain.Model
{
   public class MXODetails : Entity
    {   

        [DataMember]
        public int IP { get; set; }

        //
        [DataMember]
        public Guid? PhaseID { get; set; }
        [DataMember]
        public MXOPhase Phase { get; set; }


        [DataMember]
        public Guid? WorkMessageID { get; set; }
        [DataMember]
        public WorkMessage WorkMessage { get; set; }

        [DataMember]
        public Guid? PhaseStatusID { get; set; }
        [DataMember]
        public PhaseStatus PhaseStatus { get; set; }

        [DataMember]
        public Guid? EventInfoID { get; set; }
        [DataMember]
        public EventInfo EventInfo { get; set; }

        [DataMember]
        public Guid? OperatorID { get; set; }
        [DataMember]
        public Operator Operator { get; set; }

        [DataMember]
        public Guid? StepNoProdID { get; set; }
        [DataMember]
        public StepNoProd StepNoProd { get; set; }
        
        [DataMember]
        public Guid? UnitStatusID { get; set; }
        [DataMember]
        public UnitStatus UnitStatus { get; set; }        
        //

        [DataMember]
        public int MaterialID { get; set; }
        [DataMember]
        public float TargetAmount { get; set; }
        [DataMember]
        public float ActualAmount { get; set; }


        [DataMember]
        public int WorkID { get; set; }
        [DataMember]
        public int WorkID_SD { get; set; }


        [DataMember]
        public List<MXODetails> ListSD { set; get; }
        








        [DataMember]
    //    [Column(TypeName = "DateTime2")]
        public DateTime? RecordDate { get; set; }
        [DataMember]
    //    [Column(TypeName = "DateTime2")]
        public DateTime? RecordDateStart { get; set; }
        [DataMember]
    //    [Column(TypeName = "DateTime2")]
        public DateTime? RecordDateEnd { get; set; }
    }
}
