﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    [ServiceContract]
    public interface ICheeseCIPRequest
    {
        //[OperationContract]
        //List<WorkItem> Request(ProcessRequestParameters requestParameters);

        [OperationContract]
        List<CheeseCipDetail> Request(CheeseCIPRequestParameters requestParameters);
    }
}
