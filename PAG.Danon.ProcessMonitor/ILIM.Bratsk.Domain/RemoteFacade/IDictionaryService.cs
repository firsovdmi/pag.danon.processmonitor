using System;
using System.Collections.Generic;
using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IDictionaryService<T> : IEntityService<T> where T : IEntity
    {
        [OperationContract]
        List<ShortEntity> GetShortList();

        [OperationContract]
        ShortEntity GetShort(Guid id);
    }
}