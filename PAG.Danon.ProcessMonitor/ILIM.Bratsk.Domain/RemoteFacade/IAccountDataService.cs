using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IAccountDataService : IEntityService<AccountData>
    {
        [OperationContract]
        AccountData GetCurrentUserData(string dataID);

        [OperationContract]
        void UpdateOrCreateCurrentUserDataByDataID(AccountData accountData);
    }
}