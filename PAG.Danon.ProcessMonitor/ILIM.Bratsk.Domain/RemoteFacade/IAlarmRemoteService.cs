﻿using System.Collections.Generic;
using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Domain.Model.Alarms;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IAlarmRemoteService
    {
        [OperationContract]
        List<AlarmItem> GetAlarmItems();
        [OperationContract]
        AlarmItem GetLastAlarm();
        [OperationContract]
        void ResetAlarm(AlarmItem item);
    }
}