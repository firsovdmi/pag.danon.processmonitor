﻿using System.Collections.Generic;
using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IPlcAcessRemoteService
    {
        [OperationContract]
        BatchDirectJobs ProcessJobs(BatchDirectJobs jobs);
    }
}