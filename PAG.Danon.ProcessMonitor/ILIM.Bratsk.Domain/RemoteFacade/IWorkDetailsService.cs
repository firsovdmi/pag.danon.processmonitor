using System;
using System.Collections.Generic;
using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    /// <summary>
    ///     ������ ������� � �������
    /// </summary>
    [ServiceContract]
    public interface IWorkDetailsService : IEntityService<WorkDetail>
    {
        /// <summary>
        ///     �������� ������ ������� � ����������� ����
        /// </summary>
        /// <returns>������ �������</returns>
        [OperationContract]
        List<ShortEntity> GetShortList();
        
    }
}