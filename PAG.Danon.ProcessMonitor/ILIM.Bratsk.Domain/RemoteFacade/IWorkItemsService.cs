using System;
using System.Collections.Generic;
using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    /// <summary>
    ///     ������ ������� � �������
    /// </summary>
    [ServiceContract]
    public interface IWorkItemsService : IEntityService<WorkItem>
    {
        /// <summary>
        ///     �������� ������ ������� � ����������� ����
        /// </summary>
        /// <returns>������ �������</returns>
        [OperationContract]
        List<ShortEntity> GetShortList();

        /// <summary>
        ///     �������� ����� � ����������� ����
        /// </summary>
        /// <param name="id">������������� ������</param>
        /// <returns>����� � ����������� ����</returns>
        [OperationContract]
        ShortEntity GetLastByWorkID(int id);
        
    }
}