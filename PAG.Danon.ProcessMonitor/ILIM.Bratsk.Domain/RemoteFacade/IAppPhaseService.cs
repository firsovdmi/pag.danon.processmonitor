﻿using System.ServiceModel;
using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IAppPhaseService : IDictionaryService<AppPhase>
    {
    }
}