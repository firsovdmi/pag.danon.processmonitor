﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PAG.Danon.ProcessMonitor.Domain.RemoteFacade
{
    [ServiceContract]
    public interface IPlcBufferHandlerService
    {
        [OperationContract]
        void HandleBufferRecords();


        [OperationContract]
        void MyHandlerBufferRecords();
    }
}
