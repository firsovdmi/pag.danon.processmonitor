﻿using System.Collections.Generic;
using System.ServiceModel;

using PAG.WBD.Milk.PlcWCF;
using System.Runtime.Serialization;

namespace PAG.WBD.Milk.Service.Services
{

    [KnownType(typeof(TtnSectionItem))]
    public class TtnsLoadRemoteService : ITtnsLoadRemoteService
    {
        protected ITtnsLoadService _worker;

        public TtnsLoadRemoteService(ITtnsLoadService worker)
        {
            _worker = worker;
        }

        public string LoadTtns(List<TtnSectionItem> sections)
        {
            return _worker.LoadTtns(sections);
        }

    }
}
