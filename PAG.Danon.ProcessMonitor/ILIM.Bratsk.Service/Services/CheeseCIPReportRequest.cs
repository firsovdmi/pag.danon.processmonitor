﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using Castle.Core;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class CheeseCIPRequest : ICheeseCIPRequest
    {
        readonly ICheeseCipDetailsRepository _receptionRepository;
        private readonly ICheeseCIPPhaseRepository _phaseRepository;

        public CheeseCIPRequest(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
        {
            _receptionRepository = repositoryFactory.Create<ICheeseCipDetailsRepository>(unitOfWork);
            _phaseRepository = repositoryFactory.Create<ICheeseCIPPhaseRepository>(unitOfWork);
        }

        public List<CheeseCipDetail> Request(CheeseCIPRequestParameters requestParameters)
            {
            List<CheeseCipDetail> retyrnList = null;

            var ret = _receptionRepository.Get()
                .Include(p => p.Phase)
                .Include(p => p.Message)
                .Include(p => p.PhaseStatus)
                .Include(p => p.EventInfo)
                .Include(p => p.Operator)
                .Include(p => p.StepNoCIP)
                .Include(p => p.UnitStatus)
                .Include(p => p.CipContur)
               .Include(p => p.ALCIPPrNo)

                .Where(
                    p =>
                         //p.RecordDate > requestParameters.Period.StartTime &&
                         //p.RecordDate < requestParameters.Period.EndTime)
                         p.RecordDate > requestParameters.Period.StartTime &&
                        p.RecordDate < requestParameters.Period.EndTime)
                .ToList();



            if (requestParameters.detailReport)
                {
                retyrnList =
                 ret.Where(e => requestParameters.Phases.Select(a => a.ID).Contains(e.PhaseID.Value))
                     .GroupBy(e => new { e.WorkID })
                     .Select(q => new CheeseCipDetail()
                         {

                         Phase = q.ToList().FirstOrDefault().Phase,
                         RecordDateStart = requestParameters.Period.StartTime,
                         RecordDateEnd = requestParameters.Period.EndTime,
                         ALCIPPrNo = ret.Where(e => e.WorkID == q.Key.WorkID).Select(xx => xx.ALCIPPrNo).FirstOrDefault(),

                         ListSD = ret.Where(e => e.WorkID == q.Key.WorkID)
                             .OrderBy(e => e.RecordDate)
                             //.OrderBy(e => e.Phase)
                             //.ThenBy(e=>e.RecordDate)
                             .ToList()

                         }).ToList();
                }
            else
                {
                retyrnList = new List<CheeseCipDetail>();

                var shortReceptionCipReport = new CheeseCipDetail()
                    {
                    ListSD = ret.GroupBy(k => k.WorkID)
                   .Select(q => new CheeseCipDetail()
                       {
                       RecordDate = ret
                     .Where(ee => ee.WorkID == q.Key)
                     .OrderBy(qwer => qwer.RecordDate)
                     .Select(qaz => qaz.RecordDate)
                     .FirstOrDefault(),

                       RecordDateEnd = ret
                     .Where(ee => ee.WorkID == q.Key)
                     .OrderByDescending(qwer => qwer.RecordDate)
                     .Select(qaz => qaz.RecordDate)
                     .FirstOrDefault(),

                       Phase = ret
                     .Where(ee => ee.WorkID == q.Key)
                     .OrderByDescending(qwer => qwer.RecordDate)
                     .Select(qaz => qaz.Phase)
                     .FirstOrDefault(),

                       ALCIPPrNo = ret
                     .Where(ee => ee.WorkID == q.Key)
                     .OrderByDescending(qwer => qwer.RecordDate)
                     .Select(qaz => qaz.ALCIPPrNo)
                     .FirstOrDefault()
                       }

                   ).OrderBy(kk => kk.RecordDate).ToList()
                    };


                retyrnList.Add(shortReceptionCipReport);

                }









            return retyrnList;

            }
        }
}
