using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public abstract class EntityWithMarkAsDeletedService<T, TK> : EntityService<T, TK>
        where T : EntityBase
        where TK : class, IRepositoryWithMarkedAsDelete<T>
    {
        public EntityWithMarkAsDeletedService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }

        public override void Delete(T entity)
        {
            Repository.MarkAsDelete(entity);
            UnitOfWork.Save();
        }

        public override void DeleteList(List<T> entity)
        {
            foreach (var farm in entity)
            {
                Repository.MarkAsDelete(farm);
            }
            UnitOfWork.Save();
        }
    }
}