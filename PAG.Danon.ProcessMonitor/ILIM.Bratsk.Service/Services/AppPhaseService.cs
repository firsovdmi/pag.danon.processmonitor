﻿using System.Collections.Generic;
using System.Linq;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.ReflectionParser;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    [AuthentificationClass(Name = "AppPhaseService", Description = "Справочник линий")]
    public class AppPhaseService : DictionaryService<AppPhase, IAppPhaseRepository>, IAppPhaseService
    {
        public AppPhaseService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {

        }

        public override List<AppPhase> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }
        
    }
}