﻿using System.Collections.Generic;
using System.Linq;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.ReflectionParser;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    [AuthentificationClass(Name = "CheesePhaseService", Description = "Справочник линий")]
    public class CheesePhaseService : DictionaryService<CheesePhase, ICheesePhaseRepository>, ICheesePhaseService
    {
        public CheesePhaseService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {

        }

        public override List<CheesePhase> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }
        
    }
}