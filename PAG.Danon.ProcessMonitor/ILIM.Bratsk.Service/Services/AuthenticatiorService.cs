﻿using PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.Service.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class AuthenticatiorService : IAuthenticatiorService
    {
        IAuthenticatiorFactory _worker;
        public AuthenticatiorService(IAuthenticatiorFactory worker)
        {
            _worker = worker;
        }

        public AuthenticationResult Login(LoginInfo loginInfo)
        {
            return _worker.Login(loginInfo);
        }

        public AuthenticationResult Logoff(AuthenticationResult loginInfo)
        {
            return _worker.Logoff(loginInfo);
        }

        public ActiveLoginItem CheckLogin(Guid token, string memberName)
        {
            return _worker.CheckLogin(token, memberName);
        }

        public ActiveLoginItem CheckWindowsLogin(string UserName, string memberName)
        {
            return _worker.CheckWindowsLogin(UserName, memberName);
        }

        public void SaveLogin(FullLoginInfo login)
        {
            _worker.SaveLogin(login);
        }

        public void RemoveLogin(FullLoginInfo login)
        {
            _worker.RemoveLogin(login);
        }

        public void AddLog(int loginId, string memberName)
        {
            _worker.AddLog(loginId, memberName);
        }
    }
}
