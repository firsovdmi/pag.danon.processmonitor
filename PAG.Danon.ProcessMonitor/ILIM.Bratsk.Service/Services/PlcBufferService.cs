﻿using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class PlcBufferService : EntityWithMarkAsDeletedService<PlcBuffer, IPlcBufferRepository>, IPlcBufferService
    {
        public PlcBufferService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory) : base(unitOfWork, repositoryFactory)
        {
        }
    }
}
