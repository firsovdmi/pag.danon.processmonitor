﻿using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class PlcAcessRemoteService : IPlcAcessRemoteService
    {
        private IPlcAcessService _plcAcessService;

        public PlcAcessRemoteService(IPlcAcessService plcAcessService)
        {
            _plcAcessService = plcAcessService;
        }

        public BatchDirectJobs ProcessJobs(BatchDirectJobs jobs)
        {
            return _plcAcessService.ProcessJobs(jobs);
        }
    }
}