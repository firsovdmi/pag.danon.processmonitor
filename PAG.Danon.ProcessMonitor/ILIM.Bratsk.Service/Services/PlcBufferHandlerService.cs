﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class PlcBufferHandlerService : IPlcBufferHandlerService 
    {
        private static readonly object Lock = new object();
        protected readonly IUnitOfWork UnitOfWork;
        readonly IPlcBufferRepository _plcBufferRepository;
        readonly IWorkItemsRepository _workItemsRepository;
        IWorkDetailsRepository _workDetailsRepository;
        IWorkMessageRepository _workMessageRepository;
        readonly IPhaseRepository _phaseRepository;
        IEventInfoRepository _eventInfoRepository;
        IPhaseStatusRepository _phaseStatusRepository;
        IStepNoProdRepository _stepNoProdRepository;
        IStepNoCipRepository _stepNoCipRepository;
        IUnitStatusRepository _unitStatusRepository;
        ICipItemsRepository _cipItemsRepository;
        ICipDetailsRepository _cipDetailsRepository;
        IOperatorRepository _operatorRepository;
         ICipConturRepository _cipConturRepository;
         IReceptionDetailsRepository _receptionDetails;
        IMyPlcBufferRepository _myPlcBufferRepository;
         IReceptionPhaseRepository _receptionPhase;
         IReceptionCIPPhaseRepository _receptionCipPhase;

        private IMXOPhaseRepository _mxoPhaseRepository;
        private IMXOCIPPhaseRepository _mxocipPhaseRepository;
        private IAppPhaseRepository _appPhaseRepository;
        private IAppCIPPhaseRepository _appCipPhaseRepository;
        private ICheesePhaseRepository _cheesePhaseRepository;
        private ICheeseCIPPhaseRepository _cheeseCipPhaseRepository;

        private ICheeseDetailsRepository _cheeseDetailsRepository;
        private IAppDetailsRepository _AppDetailsRepository;
        private IMXODetailsRepository _MXODetailsRepository;

        private IReceptionCipDetailsRepository _receptionCipDeatilsRepository;
        private IMXOCipDetailsRepository _MXOCipDeatilsRepository;
        private IAppCipDetailsRepository _AppCipDeatilsRepository;
        private ICheeseCipDetailsRepository _cheeseCipDeatilsRepository;
        private IProgrammNumberRepository _programmNumberRepository;

        public PlcBufferHandlerService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
        {
            UnitOfWork = unitOfWork;
            _plcBufferRepository = repositoryFactory.Create<IPlcBufferRepository>(UnitOfWork);
            _workItemsRepository = repositoryFactory.Create<IWorkItemsRepository>(UnitOfWork);
            _workDetailsRepository = repositoryFactory.Create<IWorkDetailsRepository>(UnitOfWork);
            _phaseRepository = repositoryFactory.Create<IPhaseRepository>(UnitOfWork);
            _workMessageRepository = repositoryFactory.Create<IWorkMessageRepository>(UnitOfWork);
            _eventInfoRepository = repositoryFactory.Create<IEventInfoRepository>(UnitOfWork);
            _phaseStatusRepository = repositoryFactory.Create<IPhaseStatusRepository>(UnitOfWork);
            _stepNoProdRepository = repositoryFactory.Create<IStepNoProdRepository>(UnitOfWork);
            _unitStatusRepository = repositoryFactory.Create<IUnitStatusRepository>(UnitOfWork);
            _cipItemsRepository = repositoryFactory.Create<ICipItemsRepository>(UnitOfWork);
            _cipDetailsRepository = repositoryFactory.Create<ICipDetailsRepository>(UnitOfWork);
           _myPlcBufferRepository = repositoryFactory.Create<IMyPlcBufferRepository>(UnitOfWork);
            _operatorRepository = repositoryFactory.Create<IOperatorRepository>(UnitOfWork);
            _stepNoCipRepository = repositoryFactory.Create<IStepNoCipRepository>(UnitOfWork);
            _cipConturRepository = repositoryFactory.Create<ICipConturRepository>(UnitOfWork);
            _receptionDetails = repositoryFactory.Create<IReceptionDetailsRepository>(UnitOfWork);
            _receptionPhase = repositoryFactory.Create<IReceptionPhaseRepository>(UnitOfWork);
            _receptionCipPhase = repositoryFactory.Create<IReceptionCIPPhaseRepository>(UnitOfWork);

            _mxoPhaseRepository = repositoryFactory.Create<IMXOPhaseRepository>(UnitOfWork);
            _mxocipPhaseRepository = repositoryFactory.Create<IMXOCIPPhaseRepository>(UnitOfWork);
            _appPhaseRepository = repositoryFactory.Create<IAppPhaseRepository>(UnitOfWork);
            _appCipPhaseRepository = repositoryFactory.Create<IAppCIPPhaseRepository>(UnitOfWork);
            _cheesePhaseRepository = repositoryFactory.Create<ICheesePhaseRepository>(UnitOfWork);
            _cheeseCipPhaseRepository = repositoryFactory.Create<ICheeseCIPPhaseRepository>(UnitOfWork);

            _cheeseDetailsRepository = repositoryFactory.Create<ICheeseDetailsRepository>(UnitOfWork);
            _AppDetailsRepository = repositoryFactory.Create<IAppDetailsRepository>(UnitOfWork);
            _MXODetailsRepository = repositoryFactory.Create<IMXODetailsRepository>(UnitOfWork);

            _receptionCipDeatilsRepository = repositoryFactory.Create<IReceptionCipDetailsRepository>(UnitOfWork);
            _MXOCipDeatilsRepository = repositoryFactory.Create<IMXOCipDetailsRepository>(UnitOfWork);
            _AppCipDeatilsRepository = repositoryFactory.Create<IAppCipDetailsRepository>(UnitOfWork);
            _cheeseCipDeatilsRepository = repositoryFactory.Create<ICheeseCipDetailsRepository>(UnitOfWork);
            _programmNumberRepository = repositoryFactory.Create<IProgrammNumberRepository>(UnitOfWork);
            }

        #region CommonHandler
        public void HandleBufferRecords()
        {
            lock (Lock)
            {   

                var newPlcRecords = _plcBufferRepository
                    .Get().Where(p => p.Status == 0)
                    .OrderBy(p => p.DateTimeCreate)
                    .Take(500).ToList();

                
                if (newPlcRecords.Count == 0) return;
                currentItems.Clear();
                foreach (var plcRecord in newPlcRecords)
                {
                    try
                    {
                        var phase = _phaseRepository
                            .Get()
                            .FirstOrDefault(p => p.PlcN == plcRecord.PhaseID);


                        SelectHandler(plcRecord, phase);
                    }
                    catch (Exception ex)
                    {
                    }
                    plcRecord.Status = 1;
                    _plcBufferRepository.Update(plcRecord);
                    UnitOfWork.Save();
                }

            }
        }

        private void SelectHandler(PlcBuffer plcRecord, Phase phase)
        {
            if (phase == null) return;
            var phaseID = phase?.ID;
            if (plcRecord.PlcID == 1)
            {
                if (phase.Type == PhaseType.Prod) CreateOrUpdateWorkItem(plcRecord, phaseID);
                if (phase.Type == PhaseType.Cip) CreateOrUpdateCipItem(plcRecord, phaseID);
            }
            else
            {
                if (phase.Type == PhaseType.Prod) AddWorkDetails(plcRecord, phaseID);
                if (phase.Type == PhaseType.Cip) AddCipDetails(plcRecord, phaseID);
            }
        }



        #endregion

        #region ProcessHandler

        List<WorkItem> currentItems = new List<WorkItem>();

        private void CreateOrUpdateWorkItem(PlcBuffer plcRecord, Guid? phaseID)
        {
            var workItem = _workItemsRepository.GetLastItem(plcRecord.WorkID);
            // создаем новый, только если с таким WorkID не существует
            // есил WorkDetail прилетели раньше WorkItem, то создается пустой  WorkItem (чтобы к нему можно было привязать Detail) с IsAutoCreate == true. Если потом прилетает нормаль
            // ный WorkID, то обновляем данные для ранее созданного пустого WorkID
            if (workItem == null || !workItem.IsAutoCreate)
            {
                CreateNewWorkItem(plcRecord.WorkID, plcRecord.RecordDate, phaseID, plcRecord.WorkID_SD);
                return;
            }
            var parentWork = _workItemsRepository.GetLastItem(plcRecord.WorkID_SD);
            workItem.ParentItemID = parentWork?.ID;
            workItem.PlcParentItemID = plcRecord.WorkID_SD;
            workItem.PlcItemID = plcRecord.WorkID;
            workItem.PlcDateTime = plcRecord.RecordDate;
            workItem.PhaseID = phaseID;
            workItem.IsAutoCreate = false;
            currentItems.Add(_workItemsRepository.Update(workItem));
        }

        private void AddWorkDetails(PlcBuffer plcRecord, Guid? phaseID)
        {
            var workItem = _workItemsRepository.GetLastItem(plcRecord.WorkID) ?? CreateNewWorkItem(plcRecord.WorkID, plcRecord.DateTimeCreate, null, 0, true);
            var message = _workMessageRepository.GetItemByPlcN(plcRecord.PlcID);
            var eventInfo = _eventInfoRepository.GetItemByPlcN(plcRecord.EventInfo);
            var phaseStatus = _phaseStatusRepository.GetItemByPlcN(plcRecord.PhaseStatus);
            var stepNoProd = _stepNoProdRepository.GetItemByPlcN(plcRecord.Step_No);
            var unitStatus = _unitStatusRepository.GetItemByPlcN(plcRecord.UnitStatus);

            var newDetail = new WorkDetail
            {
                PhaseID = phaseID,
                PlcDateTime = plcRecord.RecordDate,
                PlcIWorkID = plcRecord.WorkID,
                WorkItemId = workItem.ID,
                MessageID = message?.ID,
                EventInfoID = eventInfo?.ID,
                PhaseStatusID = phaseStatus?.ID,
                OperatedID = plcRecord.OperatedID,
                StepNoProdID = stepNoProd?.ID,
                UnitStatusID = unitStatus?.ID,
                MaterialID = plcRecord.Dint_1,
                TargetAmount = plcRecord.Real_1,
                ActualAmount = plcRecord.Real_2,

            };
            _workDetailsRepository.Create(newDetail);
            if (workItem.MaxPhaseTime < newDetail.PlcDateTime)
            {
                workItem.MaxPhaseTime = newDetail.PlcDateTime;
                _workItemsRepository.Update(workItem);
                // UnitOfWork.Save();
            }
        }

        WorkItem CreateNewWorkItem(int workID, DateTime recordDateTime, Guid? phaseID, int parentID = 0, bool isAutoCreate = false)
        {
            var parentWork = parentID != 0 ? _workItemsRepository.GetLastItem(parentID) : null;

            var ret = _workItemsRepository.Create(new WorkItem
            {
                PlcItemID = workID,
                PlcParentItemID = parentID,
                ParentItemID = parentWork?.ID,
                PlcDateTime = recordDateTime,
                MaxPhaseTime = recordDateTime,
                PhaseID = phaseID,
                IsAutoCreate = isAutoCreate
            });
            return ret;

        }
        #endregion

        #region CipHandler
        private void CreateOrUpdateCipItem(PlcBuffer plcRecord, Guid? phaseID)
        {
            var cipItem = _cipItemsRepository.GetLastItem(plcRecord.WorkID);
            if (cipItem == null || !cipItem.IsAutoCreate)
            {
                CreateNewCipItem(plcRecord.WorkID, plcRecord.RecordDate, phaseID, plcRecord.WorkID_SD);
                return;
            }
            var parentWork = _workItemsRepository.GetLastItem(plcRecord.WorkID_SD);
            cipItem.ParentItemID = parentWork?.ID;
            cipItem.PlcParentItemID = plcRecord.WorkID_SD;
            cipItem.PlcItemID = plcRecord.WorkID;
            cipItem.PlcDateTime = plcRecord.RecordDate;
            cipItem.PhaseID = phaseID;
            cipItem.IsAutoCreate = false;
            _cipItemsRepository.Update(cipItem);
        }

        private void AddCipDetails(PlcBuffer plcRecord, Guid? phaseID)
        {
            var workItem = _workItemsRepository.GetLastItem(plcRecord.WorkID) ??
                                     CreateNewWorkItem(plcRecord.WorkID, plcRecord.DateTimeCreate, null, 0, true);
            var message = _workMessageRepository.GetItemByPlcN(plcRecord.PlcID);
            var eventInfo = _eventInfoRepository.GetItemByPlcN(plcRecord.EventInfo);
            var phaseStatus = _phaseStatusRepository.GetItemByPlcN(plcRecord.PhaseStatus);
            var stepNoProd = _stepNoProdRepository.GetItemByPlcN(plcRecord.Step_No);
            var unitStatus = _unitStatusRepository.GetItemByPlcN(plcRecord.UnitStatus);

            var newDetail = new CipDetail
            {
                PhaseID = phaseID,
                //PlcDateTime = plcRecord.RecordDate,
              //  PlcIWorkID = plcRecord.WorkID,
              //  CipItemId = workItem.ID,
                MessageID = message?.ID,
                EventInfoID = eventInfo?.ID,
                PhaseStatusID = phaseStatus?.ID,
              //  OperatedID = plcRecord.OperatedID,
            //    StepNoProdID = stepNoProd?.ID,
                UnitStatusID = unitStatus?.ID,
                RetTEMPMVLye = plcRecord.Int_1,
                RetCONCMVLye = plcRecord.Int_2,
                RetTEMPMVAcid = plcRecord.Int_3,
                RetCONCMVAcid = plcRecord.Int_4,
                RetTEMPMVHW = plcRecord.Int_5,
                RetCONCMV3 = plcRecord.Int_6,
                ALCIPPrNo = plcRecord.Int_7,
                StepTimeVolumePreset = plcRecord.Int_8,
                StepTimeVolumeRun = plcRecord.Int_9,

            };
            _cipDetailsRepository.Create(newDetail);
        }

        private CipItem CreateNewCipItem(int workID, DateTime recordDateTime, Guid? phaseID, int parentID, bool isAutoCreate = false)
        {
            var parentWork = parentID != 0 ? _workItemsRepository.GetLastItem(parentID) : null;

            var ret = _cipItemsRepository.Create(new CipItem
            {
                PlcItemID = workID,
                PlcParentItemID = parentID,
                ParentItemID = parentWork?.ID,
                PlcDateTime = recordDateTime,
                PhaseID = phaseID,
                IsAutoCreate = isAutoCreate
            });
            return ret;
        }








        #endregion

        public void MyHandlerBufferRecords()
        {

            lock (Lock)
            {

                var newPlcRecords = _plcBufferRepository
                    .Get().Where(p => p.Status == 0)
                    .OrderBy(p => p.DateTimeCreate)
                    .Take(100).ToList();


                foreach (var v in newPlcRecords)
                {

                    if ( v.PhaseID!=0) { 
                    if (_receptionCipPhase.Get()
                        .Select(e => e.PlcN)
                        .Where(x => x > 0)
                        .Contains(v.PhaseID)
                    )
                    {
                        ReceptionCIPHandler(v);
                        //     UnitOfWork.Save();
                        //        continue;
                    }
                    if (_appPhaseRepository.Get()
                        .Select(e => e.PlcN)
                        .Where(x => x > 0)
                        .Contains(v.PhaseID)
                    )
                    {
                        AppProdHandler(v);
                        //UnitOfWork.Save();
                        //     continue;
                    }
                    if (_appCipPhaseRepository.Get()
                        .Select(e => e.PlcN)
                        .Where(x => x > 0)
                        .Contains(v.PhaseID)
                    )
                    {
                        AppCIPHandler(v);
                        //UnitOfWork.Save();
                        //     continue;
                    }
                    if (_receptionPhase.Get()
                            .Select(e => e.PlcN)
                            .Where(x => x > 0)
                            .Contains(v.PhaseID)
                        //  .Where(x=>x.Ip==v.Ip || x.PlcN==v.PhaseID).ToList().Count() > 0
                    )
                    {
                        ReceptionProdHandler(v);
                        //    UnitOfWork.Save();
                        //   continue;
                    }
                    if (_mxoPhaseRepository.Get()
                        .Select(e => e.PlcN)
                        .Where(x => x > 0)
                        .Contains(v.PhaseID)
                    )
                    {
                        MXOProdHandler(v);
                        //UnitOfWork.Save();
                        //     continue;
                    }

                    if (_mxocipPhaseRepository.Get()
                        .Select(e => e.PlcN)
                        .Where(x => x > 0)
                        .Contains(v.PhaseID)
                    )
                    {
                        MXOCIPHandler(v);
                        //UnitOfWork.Save();
                        //     continue;
                    }

                    if (_cheesePhaseRepository.Get()
                        .Select(e => e.PlcN)
                        .Where(x => x > 0)
                        .Contains(v.PhaseID)
                    )
                    {
                        CheeseProdHandler(v);
                        //UnitOfWork.Save();
                        //     continue;
                    }

                    if (_cheeseCipPhaseRepository.Get()
                        .Select(e => e.PlcN)
                        .Where(x => x > 0)
                        .Contains(v.PhaseID)
                    )
                    {
                        CheeseCIPHandler(v);
                        //UnitOfWork.Save();
                        // continue;
                    }
                }
                v.Status = 1;
                    _plcBufferRepository.Update(v);
                    UnitOfWork.Save();
                }
            }
        }






        #region ReceptionProdHandler
        public void ReceptionProdHandler(PlcBuffer v)
            {
            ReceptionDetails myPlcBuffer = new ReceptionDetails();


                var phase = _receptionPhase.Get()
                    .FirstOrDefault(p => p.PlcN == v.PhaseID); //& p.Ip == v.Ip);
            if (phase != null)
                { myPlcBuffer.PhaseID = phase.ID; }



            var workMessage = _workMessageRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
                { myPlcBuffer.WorkMessageID = workMessage.ID; }


            var phaseStatus = _phaseStatusRepository.Get()
                                 .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
                { myPlcBuffer.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                               .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
                { myPlcBuffer.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                myPlcBuffer.OperatorID = operatorID.ID;


            var stepNo = _stepNoProdRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
                { myPlcBuffer.StepNoProdID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
                { myPlcBuffer.UnitStatusID = unitStatus.ID; }

            myPlcBuffer.WorkID = v.WorkID;
            myPlcBuffer.WorkID_SD = v.WorkID_SD;
            myPlcBuffer.ActualAmount = v.Step_No > 25 ? v.Real_2 :0;
            myPlcBuffer.TargetAmount = v.Real_1 ;
            myPlcBuffer.MaterialID = v.Dint_1;
            myPlcBuffer.IP = v.Ip;
            myPlcBuffer.RecordDate = v.RecordDate;

            //v.Status = 1;

            //_plcBufferRepository.Update(v);
            _receptionDetails.Create(myPlcBuffer);
            UnitOfWork.Save();

            
          
            }


        #endregion
        #region ReceptionCIPHandler
        public void ReceptionCIPHandler(PlcBuffer v)
            {
            ReceptionCipDetail cipDetail = new ReceptionCipDetail();



            var phase = _receptionCipPhase.Get()
                       .FirstOrDefault(p => p.PlcN == v.PhaseID);
            if (phase != null)
                { cipDetail.PhaseID = phase.ID; }


            cipDetail.WorkID = v.WorkID;


            var PrNo = _programmNumberRepository.Get()
                             .FirstOrDefault(p => p.PlcN == v.Int_7);
            if (PrNo != null)
                { cipDetail.ALCIPPrNoID = PrNo.ID; }

            var workMessage = _workMessageRepository.Get()
                             .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
                { cipDetail.MessageID = workMessage.ID; }


            var phaseStatus = _phaseStatusRepository.Get()
                              .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
                { cipDetail.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                           .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
                { cipDetail.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                    .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                cipDetail.OperatorID = operatorID.ID;



            var stepNo = _stepNoCipRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
                { cipDetail.StepNoCIPID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                          .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
                { cipDetail.UnitStatusID = unitStatus.ID; }

            var CipContur = _cipConturRepository.Get().FirstOrDefault(p => p.PlcN == v.Dint_1);
            if (CipContur != null)
                {
                cipDetail.CipConturID = CipContur.ID;
                }

                var progNo = _programmNumberRepository.Get().FirstOrDefault(p => p.PlcN == v.Int_7);
                if (progNo != null)
                {
                    cipDetail.ALCIPPrNoID = progNo.ID;
                }


            cipDetail.RetTEMPMVLye = v.Int_1;
            cipDetail.RetCONCMVLye = v.Int_2;
            cipDetail.RetTEMPMVAcid = v.Int_3;
            cipDetail.RetCONCMVAcid = v.Int_4;
            cipDetail.RetTEMPMVHW = v.Int_5;
          //  cipDetail.ALCIPPrNo = v.Int_7;
            cipDetail.StepTimeVolumePreset = v.Int_8;
            cipDetail.StepTimeVolumeRun = v.Int_9;

            cipDetail.RecordDate = v.RecordDate;
            cipDetail.IP = v.Ip;
            //v.Status = 1;

            //_plcBufferRepository.Update(v);


            _receptionCipDeatilsRepository.Create(cipDetail);
         
          //  _plcBufferRepository.Update(v);
          UnitOfWork.Save();
            }


        #endregion

        #region MXOProdHandler
        public void MXOProdHandler(PlcBuffer v)
            {
            MXODetails myPlcBuffer = new MXODetails();


            var phase = _mxoPhaseRepository.Get()
                .FirstOrDefault(p => p.PlcN == v.PhaseID);
            if (phase != null)
                { myPlcBuffer.PhaseID = phase.ID; }



            var workMessage = _workMessageRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
                { myPlcBuffer.WorkMessageID = workMessage.ID; }


            var phaseStatus = _phaseStatusRepository.Get()
                                 .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
                { myPlcBuffer.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                               .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
                { myPlcBuffer.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                myPlcBuffer.OperatorID = operatorID.ID;


            var stepNo = _stepNoProdRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
                { myPlcBuffer.StepNoProdID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
                { myPlcBuffer.UnitStatusID = unitStatus.ID; }

            myPlcBuffer.WorkID = v.WorkID;
            myPlcBuffer.WorkID_SD = v.WorkID_SD;
            myPlcBuffer.ActualAmount = v.Step_No > 25 ? v.Real_2 : 0;
            myPlcBuffer.TargetAmount =  v.Real_1;
            myPlcBuffer.MaterialID = v.Dint_1;
            myPlcBuffer.IP = v.Ip;
            myPlcBuffer.RecordDate = v.RecordDate;

            //v.Status = 1;

            //_plcBufferRepository.Update(v);


            _MXODetailsRepository.Create(myPlcBuffer);
            //UnitOfWork.Save();
         
            }


        #endregion
        #region MXOCIPHandler
        public void MXOCIPHandler(PlcBuffer v)
            {
            MXOCipDetail cipDetail = new MXOCipDetail();



            var phase = _mxocipPhaseRepository.Get()
                       .FirstOrDefault(p => p.PlcN == v.PhaseID );
            if (phase != null)
                { cipDetail.PhaseID = phase.ID; }


            cipDetail.WorkID = v.WorkID;

            var PrNo = _programmNumberRepository.Get()
                           .FirstOrDefault(p => p.PlcN == v.Int_7);
            if (PrNo != null)
                { cipDetail.ALCIPPrNoID = PrNo.ID; }
            var workMessage = _workMessageRepository.Get()
                             .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
                { cipDetail.MessageID = workMessage.ID; }


            var phaseStatus = _phaseStatusRepository.Get()
                              .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
                { cipDetail.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                           .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
                { cipDetail.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                    .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                cipDetail.OperatorID = operatorID.ID;



            var stepNo = _stepNoCipRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
                { cipDetail.StepNoCIPID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                          .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
                { cipDetail.UnitStatusID = unitStatus.ID; }

            var CipContur = _cipConturRepository.Get().FirstOrDefault(p => p.PlcN == v.Dint_1);
            if (CipContur != null)
                {
                cipDetail.CipConturID = CipContur.ID;
                }

            var progNo = _programmNumberRepository.Get().FirstOrDefault(p => p.PlcN == v.Int_7);
            if (progNo != null)
                {
                cipDetail.ALCIPPrNoID = progNo.ID;
                }

            cipDetail.RetTEMPMVLye = v.Int_1;
            cipDetail.RetCONCMVLye = v.Int_2;
            cipDetail.RetTEMPMVAcid = v.Int_3;
            cipDetail.RetCONCMVAcid = v.Int_4;
            cipDetail.RetTEMPMVHW = v.Int_5;
         //   cipDetail.ALCIPPrNo = v.Int_7;
            cipDetail.StepTimeVolumePreset = v.Int_8;
            cipDetail.StepTimeVolumeRun = v.Int_9;

            cipDetail.RecordDate = v.RecordDate;
            cipDetail.IP = v.Ip;
            v.Status = 1;

            _plcBufferRepository.Update(v);


            _MXOCipDeatilsRepository.Create(cipDetail);
            //v.Status = 1;
            //_plcBufferRepository.Update(v);
            //UnitOfWork.Save();
            }


        #endregion

        #region AppProdHandler
        public void AppProdHandler(PlcBuffer v)
            {
            AppDetails myPlcBuffer = new AppDetails();


            var phase = _appPhaseRepository.Get()
                .FirstOrDefault(p => p.PlcN == v.PhaseID );
            if (phase != null)
                { myPlcBuffer.PhaseID = phase.ID; }



            var workMessage = _workMessageRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
                { myPlcBuffer.WorkMessageID = workMessage.ID; }


            var phaseStatus = _phaseStatusRepository.Get()
                                 .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
                { myPlcBuffer.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                               .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
                { myPlcBuffer.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                myPlcBuffer.OperatorID = operatorID.ID;


            var stepNo = _stepNoProdRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
                { myPlcBuffer.StepNoProdID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
                { myPlcBuffer.UnitStatusID = unitStatus.ID; }

            myPlcBuffer.WorkID = v.WorkID;
            myPlcBuffer.WorkID_SD = v.WorkID_SD;
            myPlcBuffer.ActualAmount = v.Step_No > 25 ? v.Real_2 : 0;
            myPlcBuffer.TargetAmount = v.Real_1;
            myPlcBuffer.MaterialID = v.Dint_1;
            myPlcBuffer.IP = v.Ip;
            myPlcBuffer.RecordDate = v.RecordDate;

            //v.Status = 1;

            //_plcBufferRepository.Update(v);


            _AppDetailsRepository.Create(myPlcBuffer);
           //UnitOfWork.Save();

            }


        #endregion
        #region AppCIPHandler
        public void AppCIPHandler(PlcBuffer v)
            {
            AppCipDetail cipDetail = new AppCipDetail();



            var phase = _appCipPhaseRepository.Get()
                       .FirstOrDefault(p => p.PlcN == v.PhaseID);
            if (phase != null)
                { cipDetail.PhaseID = phase.ID; }


            cipDetail.WorkID = v.WorkID;


            var workMessage = _workMessageRepository.Get()
                             .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
                { cipDetail.MessageID = workMessage.ID; }
            var PrNo = _programmNumberRepository.Get()
                          .FirstOrDefault(p => p.PlcN == v.Int_7);
            if (PrNo != null)
                { cipDetail.ALCIPPrNoID = PrNo.ID; }

            var phaseStatus = _phaseStatusRepository.Get()
                              .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
                { cipDetail.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                           .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
                { cipDetail.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                    .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                cipDetail.OperatorID = operatorID.ID;



            var stepNo = _stepNoCipRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
                { cipDetail.StepNoCIPID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                          .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
                { cipDetail.UnitStatusID = unitStatus.ID; }

            var CipContur = _cipConturRepository.Get().FirstOrDefault(p => p.PlcN == v.Dint_1);
            if (CipContur != null)
                {
                cipDetail.CipConturID = CipContur.ID;
                }

            var progNo = _programmNumberRepository.Get().FirstOrDefault(p => p.PlcN == v.Int_7);
            if (progNo != null)
                {
                cipDetail.ALCIPPrNoID = progNo.ID;
                }

            cipDetail.RetTEMPMVLye = v.Int_1;
            cipDetail.RetCONCMVLye = v.Int_2;
            cipDetail.RetTEMPMVAcid = v.Int_3;
            cipDetail.RetCONCMVAcid = v.Int_4;
            cipDetail.RetTEMPMVHW = v.Int_5;
          //  cipDetail.ALCIPPrNo = v.Int_7;
            cipDetail.StepTimeVolumePreset = v.Int_8;
            cipDetail.StepTimeVolumeRun = v.Int_9;

            cipDetail.RecordDate = v.RecordDate;
            cipDetail.IP = v.Ip;
            v.Status = 1;

            _plcBufferRepository.Update(v);


            
            //v.Status = 1;
            //_plcBufferRepository.Update(v);
            _AppCipDeatilsRepository.Create(cipDetail);
            //UnitOfWork.Save();
            }


        #endregion

        #region CheeseProdHandler
        public void CheeseProdHandler(PlcBuffer v)
            {
            CheesenDetails myPlcBuffer = new CheesenDetails();


            var phase = _cheesePhaseRepository.Get()
                .FirstOrDefault(p => p.PlcN == v.PhaseID );
            if (phase != null)
                { myPlcBuffer.PhaseID = phase.ID; }



            var workMessage = _workMessageRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
                { myPlcBuffer.WorkMessageID = workMessage.ID; }


            var phaseStatus = _phaseStatusRepository.Get()
                                 .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
                { myPlcBuffer.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                               .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
                { myPlcBuffer.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                myPlcBuffer.OperatorID = operatorID.ID;


            var stepNo = _stepNoProdRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
                { myPlcBuffer.StepNoProdID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
                { myPlcBuffer.UnitStatusID = unitStatus.ID; }

            myPlcBuffer.WorkID = v.WorkID;
            myPlcBuffer.WorkID_SD = v.WorkID_SD;
            myPlcBuffer.ActualAmount = v.Step_No > 25 ? v.Real_2 : 0;
            myPlcBuffer.TargetAmount = v.Real_1;
            myPlcBuffer.MaterialID = v.Dint_1;
            myPlcBuffer.IP = v.Ip;
            myPlcBuffer.RecordDate = v.RecordDate;

            //v.Status = 1;

            //_plcBufferRepository.Update(v);


            _cheeseDetailsRepository.Create(myPlcBuffer);
           //UnitOfWork.Save();
            }


        #endregion
        #region CheeseCIPHandler
        public void CheeseCIPHandler(PlcBuffer v)
            {
            CheeseCipDetail cipDetail = new CheeseCipDetail();



            var phase = _cheeseCipPhaseRepository.Get()
                       .FirstOrDefault(p => p.PlcN == v.PhaseID );
            if (phase != null)
                { cipDetail.PhaseID = phase.ID; }


            cipDetail.WorkID = v.WorkID;


            var workMessage = _workMessageRepository.Get()
                             .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
                { cipDetail.MessageID = workMessage.ID; }

            var PrNo = _programmNumberRepository.Get()
                          .FirstOrDefault(p => p.PlcN == v.Int_7);
            if (PrNo != null)
                { cipDetail.ALCIPPrNoID = PrNo.ID; }
            var phaseStatus = _phaseStatusRepository.Get()
                              .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
                { cipDetail.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                           .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
                { cipDetail.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                    .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                cipDetail.OperatorID = operatorID.ID;



            var stepNo = _stepNoCipRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
                { cipDetail.StepNoCIPID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                          .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
                { cipDetail.UnitStatusID = unitStatus.ID; }

            var CipContur = _cipConturRepository.Get().FirstOrDefault(p => p.PlcN == v.Dint_1);
            if (CipContur != null)
                {
                cipDetail.CipConturID = CipContur.ID;
                }

            var progNo = _programmNumberRepository.Get().FirstOrDefault(p => p.PlcN == v.Int_7);
            if (progNo != null)
                {
                cipDetail.ALCIPPrNoID = progNo.ID;
                }


            cipDetail.RetTEMPMVLye = v.Int_1;
            cipDetail.RetCONCMVLye = v.Int_2;
            cipDetail.RetTEMPMVAcid = v.Int_3;
            cipDetail.RetCONCMVAcid = v.Int_4;
            cipDetail.RetTEMPMVHW = v.Int_5;
           
            cipDetail.StepTimeVolumePreset = v.Int_8;
            cipDetail.StepTimeVolumeRun = v.Int_9;

            cipDetail.RecordDate = v.RecordDate;
            cipDetail.IP = v.Ip;
            v.Status = 1;

            _plcBufferRepository.Update(v);


           
            //v.Status = 1;
            //_plcBufferRepository.Update(v);
            _cheeseCipDeatilsRepository.Create(cipDetail);
            //UnitOfWork.Save();
            }


        #endregion


        public void ProdHandler(PlcBuffer v)
        {
            MyPLCBuffer myPlcBuffer = new MyPLCBuffer();


            var phase = _phaseRepository.Get()
                         .FirstOrDefault(p => p.PlcN == v.PhaseID & p.Ip==v.Ip);
            if (phase != null)
            { myPlcBuffer.PhaseID = phase.ID; }



            var workMessage =   _workMessageRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.PlcID );
            if (workMessage != null)
            { myPlcBuffer.WorkMessageID = workMessage.ID; }


            var phaseStatus = _phaseStatusRepository.Get()
                                 .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
            { myPlcBuffer.PhaseStatusID = phaseStatus.ID; }
            

            var eventInfo = _eventInfoRepository.Get()
                               .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
            { myPlcBuffer.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                myPlcBuffer.OperatorID = operatorID.ID;


            var stepNo = _stepNoProdRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No);
            if (stepNo != null)
            { myPlcBuffer.StepNoProdID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
            { myPlcBuffer.UnitStatusID = unitStatus.ID; }

            myPlcBuffer.WorkID = v.WorkID;
            myPlcBuffer.WorkID_SD = v.WorkID_SD;
            myPlcBuffer.ActualAmount = v.Real_2;
            myPlcBuffer.TargetAmount = v.Real_1;
            myPlcBuffer.MaterialID = v.Dint_1;
            myPlcBuffer.IP = v.Ip;
            myPlcBuffer.RecordDate = v.RecordDate;
      
            v.Status = 1;

            _plcBufferRepository.Update(v);

           
            _myPlcBufferRepository.Create(myPlcBuffer);
           
        }




        public void CipHandler(PlcBuffer v )
        {
            CipDetail cipDetail = new CipDetail();



            var phase = _phaseRepository.Get()
                       .FirstOrDefault(p => p.PlcN == v.PhaseID & p.Ip == v.Ip);
            if (phase != null)
            { cipDetail.PhaseID = phase.ID; }


            cipDetail.WorkID = v.WorkID;


            var workMessage = _workMessageRepository.Get()
                             .FirstOrDefault(p => p.PlcN == v.PlcID);
            if (workMessage != null)
            { cipDetail.MessageID = workMessage.ID; }


            var phaseStatus = _phaseStatusRepository.Get()
                              .FirstOrDefault(p => p.PlcN == v.PhaseStatus);
            if (phaseStatus != null)
            { cipDetail.PhaseStatusID = phaseStatus.ID; }


            var eventInfo = _eventInfoRepository.Get()
                           .FirstOrDefault(p => p.PlcN == v.EventInfo);
            if (eventInfo != null)
            { cipDetail.EventInfoID = eventInfo.ID; }


            var operatorID = _operatorRepository.Get()
                                    .FirstOrDefault(p => p.PlcN == v.OperatedID);
            if (operatorID != null)
                cipDetail.OperatorID = operatorID.ID;



            var stepNo = _stepNoCipRepository.Get()
                            .FirstOrDefault(p => p.PlcN == v.Step_No );
            if (stepNo != null)
            { cipDetail.StepNoCIPID = stepNo.ID; }


            var unitStatus = _unitStatusRepository.Get()
                          .FirstOrDefault(p => p.PlcN == v.UnitStatus);
            if (unitStatus != null)
            { cipDetail.UnitStatusID = unitStatus.ID; }

            var CipContur = _cipConturRepository.Get().FirstOrDefault(p => p.PlcN == v.Dint_1);
            if (CipContur != null)
            {
                cipDetail.CipConturID = CipContur.ID;
            }



            cipDetail.RetTEMPMVLye = v.Int_1;
            cipDetail.RetCONCMVLye = v.Int_2;
            cipDetail.RetTEMPMVAcid = v.Int_3;
            cipDetail.RetCONCMVAcid = v.Int_4;
            cipDetail.RetTEMPMVHW = v.Int_5;
            cipDetail.ALCIPPrNo = v.Int_7;
            cipDetail.StepTimeVolumePreset = v.Int_8;
            cipDetail.StepTimeVolumeRun = v.Int_9;

            cipDetail.RecordDate = v.RecordDate;
            cipDetail.IP = v.Ip;
            v.Status = 1;

            _plcBufferRepository.Update(v);


           _cipDetailsRepository.Create(cipDetail);
        }
    }
 
}



