﻿using PAG.WBD.Milk.WCFAuthentication.AuthenticationClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCFAuthentication.AuthenticationClasses;

namespace PAG.WBD.Milk.Service.Services
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class AuthenticatiorRemoteFasade : IAuthenticatiorRemoteFasade
    {
        IAuthenticatiorService _worker;
        public AuthenticatiorRemoteFasade(IAuthenticatiorService worker)
        {
            _worker = worker;
        }

        public AuthenticationResult Login(LoginInfo loginInfo)
        {
            return _worker.Login(loginInfo);
        }

        public AuthenticationResult Logoff(AuthenticationResult loginInfo)
        {
            return _worker.Logoff(loginInfo);
        }

        public ActiveLoginItem CheckLogin(Guid token, string memberName)
        {
            return _worker.CheckLogin(token, memberName);
        }

        public ActiveLoginItem CheckWindowsLogin(string UserName, string memberName)
        {
            return _worker.CheckWindowsLogin(UserName, memberName);
        }

        public IEnumerable<FullLoginInfo> GetAllLogins()
        {
            return _worker.GetAllLogins();
        }

        public void SaveLogin(FullLoginInfo login)
        {
            _worker.SaveLogin(login);
        }

        public void SaveLogins(IEnumerable<FullLoginInfo> logins)
        {
            _worker.SaveLogins(logins);
        }

        public void ChangePassword(FullLoginInfo login)
        {
            _worker.ChangePassword(login);
        }

        public void RemoveLogins(IEnumerable<FullLoginInfo> logins)
        {
            _worker.RemoveLogins(logins);
        }

        public void AddLog(int loginId, string memberName)
        {
            _worker.AddLog(loginId, memberName);
        }
        public List<AcessGroup> GetAcessGroups()
        {
            return _worker.GetAcessGroups();
        }

        public void SaveAcessGroups(IEnumerable<AcessGroup> items)
        {
            _worker.AddAcessGroups(items);
        }

        public void DeleteAcessGroups(IEnumerable<AcessGroup> items)
        {
            _worker.DeleteAcessGroups(items);
        }

        public void AssignAcessGroupsToUser(FullLoginInfo login, IEnumerable<AcessGroup> acessGroups)
        {
            _worker.AssignAcessGroupsToUser(login, acessGroups);
        }

        public void AssignAcessGroupsToMembers(Members members)
        {
            _worker.AssignAcessGroupsToMembers(members);
        }

        public Members GetMembers()
        {
            return _worker.GetMembers();
        }
    }
}
