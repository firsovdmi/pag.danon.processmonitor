﻿using System;
using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class WorkDetailsService : EntityWithMarkAsDeletedService<WorkDetail, IWorkDetailsRepository>, IWorkDetailsService
    {
        public WorkDetailsService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory) : base(unitOfWork, repositoryFactory)
        {
        }

        public List<ShortEntity> GetShortList()
        {
            throw new NotImplementedException();
        }
    }
}
