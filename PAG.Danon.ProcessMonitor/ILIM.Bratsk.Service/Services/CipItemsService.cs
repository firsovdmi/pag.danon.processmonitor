﻿using System;
using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class CipItemsService : EntityWithMarkAsDeletedService<CipItem, ICipItemsRepository>, ICipItemsService
    {
        public CipItemsService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory) : base(unitOfWork, repositoryFactory)
        {
        }

        public ShortEntity GetLastByWorkID(int id)
        {
            throw new NotImplementedException();
        }

        public List<ShortEntity> GetShortList()
        {
            throw new NotImplementedException();
        }
    }
}
