﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using Castle.Core;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class ProcessRequest : IProcessRequest
    {
        readonly IMyPlcBufferRepository _myPLCBufferRepository;
        private readonly IPhaseRepository _phaseRepository;

        public ProcessRequest(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
        {
            _myPLCBufferRepository = repositoryFactory.Create<IMyPlcBufferRepository>(unitOfWork);
            _phaseRepository = repositoryFactory.Create<IPhaseRepository>(unitOfWork);
        }
        public List<MyPLCBuffer> Request(ProcessRequestParameters requestParameters)
        {


      

            var ret = _myPLCBufferRepository.Get()
                .Include(p => p.Phase)
                .Include(p => p.WorkMessage)
                .Include(p => p.PhaseStatus)
                .Include(p => p.EventInfo)
                .Include(p => p.Operator)
                .Include(p => p.StepNoProd)
                .Include(p => p.UnitStatus)
                .Where(
                    p =>
                        p.RecordDate > requestParameters.Period.StartTime &&
                        p.RecordDate < requestParameters.Period.EndTime)
                .ToList();

         

            var ret2 = ret.Where(e => e.WorkID_SD == 0 &&  requestParameters.Phases.Select(a=>a.ID).Contains(e.PhaseID.Value) )
                 .GroupBy(ord => new { ord.IP, ord.WorkID })
                 .Select(q => new MyPLCBuffer()
                 {
                     WorkID = q.Key.WorkID,
                     Phase = q.ToList().FirstOrDefault().Phase,
                     //RecordDateStart = requestParameters.Period.StartTime,
                   //  RecordDateEnd = requestParameters.Period.EndTime,
                     ListSD = ret
                                                 .Where(e => e.IP == q.Key.IP & (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID))
                                                 .OrderBy(k => k.RecordDate)
                                                 .ToList()
                 })
                 .OrderBy(e => e.DateTimeCreate)
                 .ToList()
                 ;


          
            return ret2;
        }
    }
}
