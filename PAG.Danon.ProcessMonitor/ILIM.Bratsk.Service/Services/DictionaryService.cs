using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper.QueryableExtensions;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public abstract class DictionaryService<T, TK> : EntityWithMarkAsDeletedService<T, TK>, IDictionaryService<T>
        where T : Entity, IMarkableForDelete
        where TK : class, IRepositoryWithMarkedAsDelete<T>
    {
        protected DictionaryService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {
        }

        public List<ShortEntity> GetShortList()
        {
            return Repository.Get().Where(p => !p.IsDeleted).Select(e=>new ShortEntity { ID = e.ID, Name = e.Name}).ToList();
        }

        public ShortEntity GetShort(Guid id)
        {
            return Repository.Get().Where(p => !p.IsDeleted && p.ID == id).Project().To<ShortEntity>().FirstOrDefault();
        }

        public override List<T> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }
    }
}