using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Infrastructure.Log;
using PAG.Danon.ProcessMonitor.Infrastructure.Log.Model;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class ReadLogRemoteService : IReadLogRemoteService
    {
        private readonly IReadLogService _readLogService;

        public ReadLogRemoteService(IReadLogService readLogService)
        {
            _readLogService = readLogService;
        }

        #region Implementation of IReadLogService

        public List<LogItem> GetLog(LogRequestParameters logRequestParameters)
        {
            return _readLogService.GetLog(logRequestParameters);
        }

        #endregion
    }
}