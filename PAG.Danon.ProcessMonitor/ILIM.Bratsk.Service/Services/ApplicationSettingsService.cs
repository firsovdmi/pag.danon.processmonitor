﻿using System.Collections.Generic;
using System.Linq;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.ReflectionParser;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    [AuthentificationClass(Name = "ApplicationSettingsService", Description = "Настройки системы")]
    public class ApplicationSettingsService : DictionaryService<ApplicationSettings, IApplicationSettingsRepository>, IApplicationSettingsService
    {
        public ApplicationSettingsService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {

        }

        public override List<ApplicationSettings> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }
        public string GetByName(string name)
        {
            var firstOrDefault = Repository.Get().FirstOrDefault(p => p.SystemName == name);
            return firstOrDefault != null ? firstOrDefault.Value : "";
        }
    }
}