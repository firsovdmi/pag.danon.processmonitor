﻿using System.Collections.Generic;
using System.Linq;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.ReflectionParser;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    [AuthentificationClass(Name = "StepNoOVATService", Description = "Справочник линий")]
    public class StepNoOVATService : DictionaryService<StepNoOVAT, IStepNoOVATRepository>, IStepNoOVATService
    {
        public StepNoOVATService(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
            : base(unitOfWork, repositoryFactory)
        {

        }

        public override List<StepNoOVAT> Get()
        {
            return Repository.Get().Where(p => !p.IsDeleted).ToList();
        }
        

    }
}