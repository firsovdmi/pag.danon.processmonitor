﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using Castle.Core;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class ReceptionRequest : IReceptionRequest
    {
        //readonly IMyPlcBufferRepository _myPLCBufferRepository;
        readonly IReceptionDetailsRepository _receptionRepository;
        private readonly IReceptionPhaseRepository _phaseRepository;

        public ReceptionRequest(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
        {
            _receptionRepository = repositoryFactory.Create<IReceptionDetailsRepository>(unitOfWork);
            _phaseRepository = repositoryFactory.Create<IReceptionPhaseRepository>(unitOfWork);
            // _myPLCBufferRepository= repositoryFactory.Create<IMyPlcBufferRepository>(unitOfWork);
        }

        public List<ReceptionDetails> Request(ReceptionRequestParameters requestParameters)
        {

            if (requestParameters.detailReport)
            {
                if (requestParameters.tanksIsSelected)
                {
                    return TankFull(requestParameters);
                }
                if (requestParameters.phasesIsSelected)
                {
                    return PhaseFull(requestParameters);
                }
            }

            if (requestParameters.detailReport == false)
            {
                if (requestParameters.tanksIsSelected)
                {
                    return TankEmb(requestParameters);
                }
                if (requestParameters.phasesIsSelected)
                {
                    return PhasEmb(requestParameters);
                }
            }


            return null;


        }


        #region PhaseFull

        List<ReceptionDetails> PhaseFull(ReceptionRequestParameters requestParameters)
        {
            var TimeFltrList = TimeFiltr(requestParameters);
            // List phases 
            return TimeFltrList.Where(
                        e =>
                        {
                            return e.PhaseID != null && (e.WorkID_SD == 0 &&
                                                         requestParameters.Phases.Select(a => a.ID)
                                                             .Contains(e.PhaseID.Value));
                        })


                    .GroupBy(ord => new {ord.IP, ord.WorkID})
                    .Select(q => new ReceptionDetails()
                    {
                        RecordDateStart = requestParameters.Period.StartTime,
                        RecordDateEnd = requestParameters.Period.EndTime,
                        Phase =
                            new ReceptionPhase()
                            {
                                Name =


                                (TimeFltrList
                                    .Select((x) => new StringBuilder("Из ")
                                        .Append((TimeFltrList
                                                     .Where(
                                                         e =>
                                                             e.IP == q.Key.IP &
                                                             (e.WorkID == q.Key.WorkID ||
                                                              e.WorkID_SD == q.Key.WorkID))
                                                     .Where(k => k.Phase.Class == PhaseClass.xferIn).FirstOrDefault() !=
                                                 null)
                                            ? Regex.Split(TimeFltrList
                                                    .Where(
                                                        e =>
                                                        {
                                                            return q != null && e.IP == q.Key.IP &
                                                                   (e.WorkID == q.Key.WorkID ||
                                                                    e.WorkID_SD == q.Key.WorkID);
                                                        })
                                                    .Where(k => k.Phase.Class == PhaseClass.xferIn)
                                                    .Select(p => p.Phase.Description)
                                                    .FirstOrDefault(), @"(Из)|(в)")
                                                .Skip(2)
                                                .FirstOrDefault()
                                            : "null"

                                        )
                                        .Append(" в ")
                                        .Append((TimeFltrList
                                                     .Where(
                                                         e =>
                                                             e.IP == q.Key.IP &
                                                             (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID) &
                                                             e.PhaseID != null)
                                                     .Where(k => k.Phase.Class == PhaseClass.xferout).FirstOrDefault() !=
                                                 null)
                                            ? Regex.Split(TimeFltrList
                                                    .Where(
                                                        e =>
                                                            e.IP == q.Key.IP &
                                                            (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID) &
                                                            e.PhaseID != null)
                                                    .Where(k => k.Phase.Class == PhaseClass.xferout)
                                                    .Select(p => p.Phase.Description)
                                                    .FirstOrDefault(), @"(Из)|(в)")
                                                .Last()
                                            : "null")
                                        .Append(" по ")
                                        .Append((TimeFltrList
                                                     .Where(
                                                         e =>
                                                             e.IP == q.Key.IP &
                                                             (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID) &
                                                             e.PhaseID != null)
                                                     .Where(k => k.Phase.Class == PhaseClass.prod).FirstOrDefault() !=
                                                 null)
                                            ? TimeFltrList
                                                .Where(
                                                    e =>
                                                        e.IP == q.Key.IP &
                                                        (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID))
                                                .Where(k => k.Phase.Class == PhaseClass.prod)
                                                .Select(p => p.Phase.Description)
                                                .FirstOrDefault()
                                            : "null")
                                        .ToString()).FirstOrDefault())
                            },

                        ListSD =
                             _receptionRepository.Get().Include(p => p.Phase)
                .Include(p => p.WorkMessage)
                .Include(p => p.PhaseStatus)
                .Include(p => p.EventInfo)
                .Include(p => p.Operator)
                .Include(p => p.StepNoProd)
                .Include(p => p.UnitStatus)
                                .Where(
                                    e =>
                                       
                                        (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID)).ToList()
                                .Select(ee =>
                                {
                                    ee.RecordDateEnd = null;
                                    return ee;
                                })
                                .OrderBy(k => k.RecordDate)
                                .ToList()
                    }).OrderBy(e => e.RecordDate)
                    .ToList()
                ;



        }

        #endregion

        #region TankFull

        List<ReceptionDetails> TankFull(ReceptionRequestParameters requestParameters)
        {   

            // TankFiltr(requestParameters)
            List<ReceptionDetails> aList = TankFiltr(requestParameters);
            List<ReceptionDetails> timefiltr = TimeFiltr(requestParameters);
            return aList.GroupBy
                (ord
                    =>

                        new
                        {
                            ord.IP,
                            ord.WorkID
                        })
                .

                Select(q =>

                    new ReceptionDetails()
                    {
                        RecordDateStart = requestParameters.Period.StartTime,
                        RecordDateEnd = requestParameters.Period.EndTime,
                        Phase =
                            new ReceptionPhase()
                            {
                                Name =


                                (timefiltr
                                    .Select((x) => new StringBuilder("Из ")
                                        .Append((timefiltr
                                                     .Where(
                                                         e =>
                                                             e.IP == q.Key.IP &
                                                             (e.WorkID == q.Key.WorkID ||
                                                              e.WorkID_SD == q.Key.WorkID))
                                                     .Where(k => k.Phase.Class == PhaseClass.xferIn).FirstOrDefault() !=
                                                 null)
                                            ? Regex.Split(timefiltr
                                                    .Where(
                                                        e =>
                                                            e.IP == q.Key.IP &
                                                            (e.WorkID == q.Key.WorkID ||
                                                             e.WorkID_SD == q.Key.WorkID))
                                                    .Where(k => k.Phase.Class == PhaseClass.xferIn)
                                                    .Select(p => p.Phase.Description)
                                                    .FirstOrDefault(), @"(Из)|(в)")
                                                .Skip(2)
                                                .FirstOrDefault()
                                            : "null"

                                        )
                                        .Append(" в ")
                                        .Append((timefiltr
                                                     .Where(
                                                         e =>
                                                             e.IP == q.Key.IP &
                                                             (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID))
                                                     .Where(k => k.Phase.Class == PhaseClass.xferout).FirstOrDefault() !=
                                                 null)
                                            ? Regex.Split(timefiltr
                                                    .Where(
                                                        e =>
                                                            e.IP == q.Key.IP &
                                                            (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID))
                                                    .Where(k => k.Phase.Class == PhaseClass.xferout)
                                                    .Select(p => p.Phase.Description)
                                                    .FirstOrDefault(), @"(Из)|(в)")
                                                .Last()
                                            : "null")
                                        .Append(" по ")
                                        .Append((timefiltr
                                                     .Where(
                                                         e =>
                                                             e.IP == q.Key.IP &
                                                             (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID))
                                                     .Where(k => k.Phase.Class == PhaseClass.prod).FirstOrDefault() !=
                                                 null)
                                            ? timefiltr
                                                .Where(
                                                    e =>
                                                        e.IP == q.Key.IP &
                                                        (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID))
                                                .Where(k => k.Phase.Class == PhaseClass.prod)
                                                .Select(p => p.Phase.Description)
                                                .FirstOrDefault()
                                            : "null")
                                        .ToString()).FirstOrDefault())
                            },

                        ListSD =
                             _receptionRepository.Get().Include(p => p.Phase)
                .Include(p => p.WorkMessage)
                .Include(p => p.PhaseStatus)
                .Include(p => p.EventInfo)
                .Include(p => p.Operator)
                .Include(p => p.StepNoProd)
                .Include(p => p.UnitStatus)
                                .Where(
                                    e =>
                                        
                                        (e.WorkID == q.Key.WorkID || e.WorkID_SD == q.Key.WorkID)).ToList()
                                .Select(ee =>
                                {
                                    ee.RecordDateEnd = null;
                                    return ee;
                                })
                                .OrderBy(k => k.RecordDate)
                                .ToList()
                    }).

                OrderBy(e => e.RecordDate)
                .

                ToList();
        }

        #endregion

        #region PhaseEmb

        List<ReceptionDetails> PhasEmb(ReceptionRequestParameters requestParameters)
        {

            var timefiltr = TimeFiltr(requestParameters);
            return requestParameters.Phases.Select(e => new ReceptionDetails()
                {

                Phase = _phaseRepository.Get().FirstOrDefault(h => h.ID == e.ID),
                RecordDate = requestParameters.Period.StartTime,
                RecordDateEnd = requestParameters.Period.EndTime,
               

                ListSD =
                timefiltr
                    .Where(retx => retx.PhaseID != null && retx.PhaseID.Value == e.ID && retx.WorkID_SD == 0)
                    .GroupBy(qq => new { qq.WorkID })
                    .Select(q => new ReceptionDetails()
                        {
                        Phase =
                    new ReceptionPhase()
                    {

                        Description =
                       timefiltr
                       .Select(x=> new StringBuilder("Из")
                       .Append(((timefiltr
                        .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                        .FirstOrDefault(k => k.Phase.Class == PhaseClass.xferIn) != null) ?
                        Regex.Split(timefiltr
                        .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                        .Where(k => k.Phase.Class == PhaseClass.xferIn)
                       .Select(p => p.Phase.Description)
                       .FirstOrDefault(), @"(Из)|(в)"
                        )
                        .Skip(2)
                        .FirstOrDefault() : "null"))
                        .Append("В")
                        .Append(((timefiltr
                        .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                        .FirstOrDefault(k => k.Phase.Class == PhaseClass.xferout) != null) ?
                        Regex.Split(timefiltr
                        .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                        .Where(k => k.Phase.Class == PhaseClass.xferout)
                       .Select(p => p.Phase.Description)
                       .FirstOrDefault(), @"(Из)|(в)"
                        ) .Last() : "null" ))
                       ).FirstOrDefault().ToString()
                        },


                        RecordDate = _receptionRepository.Get()
                        .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                        .OrderBy(qwer=>qwer.RecordDate)
                        .Select(qaz => qaz.RecordDate)
                        .FirstOrDefault(),
                        RecordDateEnd = _receptionRepository.Get()
                        .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                        .OrderByDescending(qwer => qwer.RecordDate)
                        .Select(qaz => qaz.RecordDate)
                        .FirstOrDefault(),
                        ActualAmount = _receptionRepository.Get()
                        .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                        .OrderByDescending(k => k.RecordDate)
                        .Select(qa => qa.ActualAmount)
                        .FirstOrDefault(kk => kk > 0)
                        })
                        .OrderBy(f => f.RecordDate).ToList()
                   

                })
                .ToList();


            }

        #endregion

        #region TankEmb

        List<ReceptionDetails> TankEmb(ReceptionRequestParameters requestParameters)
            {

        
                var tankfiltr = TankFiltr(requestParameters);
            var timefiltr = TimeFiltr(requestParameters);
            return requestParameters.XferIn.Select(e => new ReceptionDetails()
                {

                Phase = new ReceptionPhase()
                    {
                    Name = e
                    },
                RecordDateStart = requestParameters.Period.StartTime,
                RecordDateEnd = requestParameters.Period.EndTime,


                ListSD =
         tankfiltr
              .Where(retx => retx.PhaseID != null 
              //&& retx.PhaseID.Value == e.ID
              //&& retx.WorkID_SD == 0
              )
              .GroupBy(qq => new { qq.WorkID })
              .Select(q => new ReceptionDetails()
                  {
                  Phase =
              new ReceptionPhase()
                  {

                  Description =
                timefiltr
                 .Select(x => new StringBuilder("в ")

                 .Append(((timefiltr
                  .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                  .FirstOrDefault(k => k.Phase.Class == PhaseClass.xferout) != null) ?

                  Regex.Split(timefiltr
                  .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                  .Where(k => k.Phase.Class == PhaseClass.xferout)
                 .Select(p => p.Phase.Description)
                 .FirstOrDefault(), @"(Из)|(в)"
                  ).Last()

                   : "null"))
                  .Append(" по ")
                  .Append(((tankfiltr
                  .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                  .FirstOrDefault(k => k.Phase.Class == PhaseClass.prod) != null) ?
                  (tankfiltr
                  .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                  .Where(k => k.Phase.Class == PhaseClass.prod)
                 .Select(p => p.Phase.Description).FirstOrDefault()

                         ) : "null"))
                        ).FirstOrDefault().ToString()
                  },
                  RecordDate = _receptionRepository.Get()
                         .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                         .OrderBy(qwer => qwer.RecordDate)
                         .Select(qaz => qaz.RecordDate)
                         .FirstOrDefault(),
                  RecordDateEnd = _receptionRepository.Get()
                          .Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                          .OrderByDescending(qwer => qwer.RecordDate)
                          .Select(qaz => qaz.RecordDate)
                          .FirstOrDefault(),
                  ActualAmount = _receptionRepository.Get().Where(ee => ee.WorkID == q.Key.WorkID || ee.WorkID_SD == q.Key.WorkID)
                          .OrderByDescending(k => k.RecordDate)
                          .Select(qa => qa.ActualAmount)
                          .FirstOrDefault(kk => kk > 0)
                  })
                         .OrderBy(f=>f.RecordDate) .ToList()


                })
          .ToList();

            //Phase = new ReceptionPhase()
            //{
            //    Name = e.Name 
            //},
            }
        //=> TankFiltr(requestParameters)

        //.GroupBy(ord => new {ord.IP, ord.WorkID})
        //.Select(q => new ReceptionDetails()
        //{
        //    RecordDateStart = requestParameters.Period.StartTime,
        //    RecordDateEnd = requestParameters.Period.EndTime,
        //    Phase =
        //        new ReceptionPhase()
        //        {
        //            Name = (TimeFiltr(requestParameters)
        //                .Select((x) => new StringBuilder("Из ")
        //                    .Append((TimeFiltr(
        //                                     requestParameters)
        //                                 .Where(e => e.IP == q.Key.IP &
        //                                         (e.WorkID == q.Key.WorkID ||
        //                                          e.WorkID_SD == q.Key.WorkID)).FirstOrDefault(k => k.Phase.Class == PhaseClass.xferIn) != null)
        //                        ? Regex.Split(TimeFiltr(requestParameters)
        //                                .Where(
        //                                    e =>
        //                                        e.IP == q.Key.IP &
        //                                        (e.WorkID == q.Key.WorkID ||
        //                                         e.WorkID_SD == q.Key.WorkID))
        //                                .Where(k => { return k.Phase != null && k.Phase.Class == PhaseClass.xferIn; })
        //                                .Select(p => p.Phase.Description)
        //                                .FirstOrDefault(), @"(Из)|(в)")
        //                            .Skip(2)
        //                            .FirstOrDefault()
        //                        : "null"

        //                    ).ToString()).FirstOrDefault())



        //        },

        //    ListSD = null

        //}).OrderBy(e => e.DateTimeCreate)
        //.ToList();

        #endregion

        List<ReceptionDetails> TimeFiltr(ReceptionRequestParameters requestParameters)
        {
            return _receptionRepository.Get()
                .Include(p => p.Phase)
                .Include(p => p.WorkMessage)
                .Include(p => p.PhaseStatus)
                .Include(p => p.EventInfo)
                .Include(p => p.Operator)
                .Include(p => p.StepNoProd)
                .Include(p => p.UnitStatus)
                .Where(
                    p =>
                        p.RecordDate > requestParameters.Period.StartTime &&
                        p.RecordDate < requestParameters.Period.EndTime)
                .ToList();
        }



        List<ReceptionDetails> TankFiltr(ReceptionRequestParameters requestParameters)
        {
            List<Guid> requestXferInPhasesGuides = new List<Guid>();
            // Выбрали нужные из репозитория.
            foreach (var v in _phaseRepository.Get())
            {
                foreach (var xfer in requestParameters.XferIn)
                {
                    if (v != null && v.Name!= null)
                    {
                        if (v.Name.Contains(xfer) & v.Class == PhaseClass.xferIn)
                        {
                            requestXferInPhasesGuides.Add(v.ID);
                        }
                    }
                }
            }

            List<int> WorkID_SDList = new List<int>();
            foreach (var r in TimeFiltr(requestParameters).Where(e => e.PhaseID != null))
            {
                foreach (var req in requestXferInPhasesGuides)
                {
                    if (r.PhaseID.Value == req)
                    {
                        WorkID_SDList.Add(r.WorkID_SD);
                    }


                }
            }

            List<ReceptionDetails> abc = new List<ReceptionDetails>();
            abc = TimeFiltr(requestParameters).Where(e => WorkID_SDList.Distinct().Contains(e.WorkID)).ToList();
            return abc;
        }



    }
}



