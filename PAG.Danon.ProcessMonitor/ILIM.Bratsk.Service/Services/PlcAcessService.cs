﻿using System;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class PlcAcessService : IPlcAcessService
    {
        public string Ip { get; set; }
        public int Rack { get; set; }
        public int Slot { get; set; }

        JobsProcessor _jobProcessor = null;

        public PlcAcessService(string ip, int rack, int slot)
        {
            Ip = ip;
            Rack = rack;
            Slot = slot;

        }


        public void Init()
        {
            if (_jobProcessor == null)
            {
                _jobProcessor = new JobsProcessor(Ip, Rack, Slot);
            }
        }


        public BatchDirectJobs ProcessJobs(BatchDirectJobs jobs)
        {
            try
            {
                DateTime memTime = DateTime.Now;
                Init();
                _jobProcessor.ProcessJobs(jobs);
                jobs.ProcessingTime = (DateTime.Now - memTime).TotalMilliseconds;

            }
            catch (Exception)
            {

            }
            return jobs;
        }
    }

}