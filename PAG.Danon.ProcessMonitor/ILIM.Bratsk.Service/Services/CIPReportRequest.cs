﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class CIPRequest : ICIPRequest
    {
        
         readonly ICipDetailsRepository _cipDetailsRepository;



        public CIPRequest(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
        {
            _cipDetailsRepository = repositoryFactory.Create<ICipDetailsRepository>(unitOfWork);
           
        }
        public List<CipDetail> Request(CIPRequestParameters requestParameters)
        {       

                
            var ret= _cipDetailsRepository.Get()
               .Include(p => p.Phase)
                .Include(p=>p.Message)
                .Include(p => p.PhaseStatus)
                .Include(p => p.EventInfo)
                .Include(p => p.Operator)
                .Include(p => p.StepNoCIP)
                .Include(p => p.UnitStatus)
                .Include(p=>p.CipContur)
                .Where(
                    p =>
                        p.RecordDate > requestParameters.Period.StartTime &&
                        p.RecordDate < requestParameters.Period.EndTime)
                .ToList();

            List<CipDetail> retyrnList = null;




            if (requestParameters.IsWithActsOnly)
            {
                retyrnList =
                    ret.Where(e => requestParameters.Phases.Select(a => a.ID).Contains(e.PhaseID.Value))
                        .GroupBy(e => new {e.WorkID})
                        .Select(q => new CipDetail()
                        {
                           
                            Phase = q.ToList().FirstOrDefault().Phase,
                            RecordDateStart = requestParameters.Period.StartTime,
                            RecordDateEnd = requestParameters.Period.EndTime,
                            ListSD = ret.Where(e => e.WorkID == q.Key.WorkID)
                            .OrderBy(e=>e.RecordDate)
                            //.OrderBy(e => e.Phase)
                            //.ThenBy(e=>e.RecordDate)
                            .ToList()
                            
                        }).ToList();
            }
            else if (requestParameters.IsWithActsOnly1)
            {
                retyrnList =
                 ret.Where(e => requestParameters.Conturs.Select(a => a.ID).Contains(e.CipConturID.Value))

                .GroupBy(e => new { e.WorkID})
                        .Select(q => new CipDetail()
                        {
                            CipContur = q.ToList().FirstOrDefault().CipContur,
                            Phase = q.ToList().FirstOrDefault().Phase,
                            ListSD = ret.Where(e => e.WorkID == q.Key.WorkID)
                                        .OrderBy(e => e.RecordDate)
                                        .ToList()
                            //.OrderBy(e => e.Phase)
                            //.ThenBy(e=>e.RecordDate)


                        })
                     
                     .ToList();
            }


            return retyrnList;
        }
    }
}
