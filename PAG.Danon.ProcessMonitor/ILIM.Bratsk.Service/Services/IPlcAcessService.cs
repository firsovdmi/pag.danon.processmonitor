﻿using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public interface IPlcAcessService
    {
        BatchDirectJobs ProcessJobs(BatchDirectJobs jobs);
        void Init();
    }
}
