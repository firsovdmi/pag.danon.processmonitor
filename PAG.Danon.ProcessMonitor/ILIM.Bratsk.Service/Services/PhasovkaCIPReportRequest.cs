﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Policy;
using Castle.Core;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;

namespace PAG.Danon.ProcessMonitor.Service.Services
{
    public class PhasovkaCIPRequest : IPhasovkaCIPRequest
    {
        readonly IPhasovkaCipDetailsRepository _receptionRepository;
        private readonly IPhasovkaCIPPhaseRepository _phaseRepository;

        public PhasovkaCIPRequest(IUnitOfWork unitOfWork, IRepositoryFactory repositoryFactory)
        {
            _receptionRepository = repositoryFactory.Create<IPhasovkaCipDetailsRepository>(unitOfWork);
            _phaseRepository = repositoryFactory.Create<IPhasovkaCIPPhaseRepository>(unitOfWork);
        }

        public List<PhasovkaCipDetails> Request(PhasovkaCIPRequestParameters requestParameters)
        {
            var ret = _receptionRepository.Get()
             .Include(p => p.Phase)
              .Include(p => p.Message)
              .Include(p => p.PhaseStatus)
              .Include(p => p.EventInfo)
              .Include(p => p.Operator)
              .Include(p => p.StepNoCIP)
              .Include(p => p.UnitStatus)
              .Include(p => p.CipContur)
              .Include(p => p.ALCIPPrNo)

              .Where(
                  p =>
                      p.RecordDate > requestParameters.Period.StartTime &&
                      p.RecordDate < requestParameters.Period.EndTime)
              .ToList();

            List<PhasovkaCipDetails> retyrnList = null;




           
                retyrnList =
                    ret.Where(e => requestParameters.Phases.Select(a => a.ID).Contains(e.PhaseID.Value))
                        .GroupBy(e => new { e.WorkID })
                        .Select(q => new PhasovkaCipDetails()
                        {

                            Phase = q.ToList().FirstOrDefault().Phase,
                            RecordDateStart = requestParameters.Period.StartTime,
                            RecordDateEnd = requestParameters.Period.EndTime,
                            ListSD = ret.Where(e => e.WorkID == q.Key.WorkID)
                            .OrderBy(e => e.RecordDate)
                            //.OrderBy(e => e.Phase)
                            //.ThenBy(e=>e.RecordDate)
                            .ToList()

                        }).ToList();
            
         


            return retyrnList;

        }
    }
}
