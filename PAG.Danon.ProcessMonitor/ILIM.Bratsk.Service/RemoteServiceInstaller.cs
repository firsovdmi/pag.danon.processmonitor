using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using Castle.Facilities.Startable;
using Castle.Facilities.WcfIntegration;
using Castle.Facilities.WcfIntegration.Behaviors;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA;
using PAG.Danon.ProcessMonitor.Infrastructure.CallBackService.MilkReceiveData;
using PAG.Danon.ProcessMonitor.Infrastructure.DataBaseDirect;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Log;
using PAG.Danon.ProcessMonitor.Service.Services;

namespace PAG.Danon.ProcessMonitor.Service
{
    public class RemoteServiceInstaller : ServiceInstaller
    {
        #region Implementation of IWindsorInstaller

        /// <summary>
        ///     Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer" />.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="store">The configuration store.</param>
        public override void Install(IWindsorContainer container, IConfigurationStore store)
        {
            base.Install(container, store);

            container.AddFacilityIfNotAdded<StartableFacility>();
            container.AddFacilityIfNotAdded<WcfFacility>();
            container.Register(Component.For<MessageLifecycleBehavior>());
            container.Register(Component.For<LifestyleServerMessageAction>());
            container.Register(Component.For<ILazyComponentLoader>()
                .ImplementedBy<LazyOfTComponentLoader>());
            
            var wcfBaseSettings = ((WcfBaseSettingsSection) (System.Configuration.ConfigurationManager.GetSection("WcfBaseSettings")));
            //������� ������ �� �������
            var baseAddresses = new List<string>();
            for (var i = 0; i < wcfBaseSettings.BaseAddresses.Count; i++)
            {
                baseAddresses.Add(wcfBaseSettings.BaseAddresses[i].BaseAddress);
            }
            //����������� ���������
            container.Register(Component.For<IServiceBehavior>().Instance(new ServiceBehaviorAttribute
            {
                InstanceContextMode = InstanceContextMode.PerCall,
                ConcurrencyMode = ConcurrencyMode.Multiple,
                IncludeExceptionDetailInFaults = true
            }));


            container.Register(
    Component.For<IDataBaseDirectService>()
        .ImplementedBy<DataBaseDirectService>()
        .AsWcfService(new DefaultServiceModel()
            .AddBaseAddresses(baseAddresses.Select(pp => new Uri(pp + "/DataBaseDirectService")).ToArray())
            .PublishMetadata(
                pp =>
                {
                    pp.AtAddress("mex");
                    pp.EnableHttpGet();
                })
            .AddEndpoints(
                WcfEndpoint.BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                , WcfEndpoint.BoundTo(new WSDualHttpBinding())
            )
        ).LifestylePerWcfOperation().DependsOn(Dependency.OnValue("connectionString", "PAG.Danon.ProcessMonitor")));

            var xx = Types.FromAssemblyContaining<PhaseService>()
                .InSameNamespaceAs<PhaseService>()
                .WithServiceDefaultInterfaces();
            //����������� ��������
            container.Register(
                Types.FromAssemblyContaining<PhaseService>()
                    .InSameNamespaceAs<PhaseService>()
                    .WithServiceDefaultInterfaces()
                    .Configure(p
                        => p.AsWcfService(new DefaultServiceModel()
                            .AddBaseAddresses(
                                baseAddresses.Select(pp => new Uri(pp + "/" + p.Implementation.Name)).ToArray())
                            .PublishMetadata(
                                pp =>
                                {
                                    pp.AtAddress("mex");
                                    pp.EnableHttpGet();
                                })
                            .AddEndpoints(
                                WcfEndpoint.BoundTo(new NetTcpBinding("netTcp") {PortSharingEnabled = true})
                                , WcfEndpoint.BoundTo(new BasicHttpBinding())
                            )
                            )).LifestylePerWcfOperation());


            container.Register(
                Component.For<ILogPublisherRemote>()
                    .ImplementedBy<LogPublisherRemote>()
                    .AsWcfService(new DefaultServiceModel()
                        .AddBaseAddresses(baseAddresses.Select(pp => new Uri(pp + "/LogPublisherRemote")).ToArray())
                        .PublishMetadata(
                            pp =>
                            {
                                pp.AtAddress("mex");
                                pp.EnableHttpGet();
                            })
                        .AddEndpoints(
                            WcfEndpoint.BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                            , WcfEndpoint.BoundTo(new WSDualHttpBinding())
                        )
                    ).LifestylePerWcfOperation());





            container.Register(
                Component.For<IReceivingTaskPublisherRemote>()
                    .ImplementedBy<ReceivingTaskPublisherRemote>()
                    .AsWcfService(new DefaultServiceModel()
                        .AddBaseAddresses(
                            baseAddresses.Select(pp => new Uri(pp + "/ReceivingTaskPublisherRemote")).ToArray())
                        .PublishMetadata(
                            pp =>
                            {
                                pp.AtAddress("mex");
                                pp.EnableHttpGet();
                            })
                        .AddEndpoints(
                            WcfEndpoint.BoundTo(new NetTcpBinding("netTcp") {PortSharingEnabled = true})
                            , WcfEndpoint.BoundTo(new WSDualHttpBinding())
                        )
                    ).LifestylePerWcfOperation());


            //  container.Register(Component.For<IOperationBehavior>().ImplementedBy<SetMaxObjectsInGraphBehavior>().Attribute("scope").Eq(WcfExtensionScope.Services));
            //container.Register(Component.For<IEndpointBehavior>().ImplementedBy<ChangeObjectGraphBehavior>());
        }

        #endregion
    }
}