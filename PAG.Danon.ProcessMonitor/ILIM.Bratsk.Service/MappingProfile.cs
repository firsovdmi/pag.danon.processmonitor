﻿using AutoMapper;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Service
{
    public class MappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return GetType().Name; }
        }

        protected override void Configure()
        {

            Mapper.CreateMap<Report, ShortEntity>();
        }
    }
}