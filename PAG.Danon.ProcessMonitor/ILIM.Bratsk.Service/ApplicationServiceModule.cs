using System.Configuration;
using AutoMapper;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.ReflectionParser;
using PAG.Danon.ProcessMonitor.Infrastructure.Log;

namespace PAG.Danon.ProcessMonitor.Service
{
    public class ApplicationServiceModule : IModule
    {
        public ApplicationServiceModule(IAAARepositoryFactory aaaRepositoryFactory, IUnitOfWorkAAA uof,
            ILogService logService)
        {
            Mapper.Initialize(cfg => { cfg.AddProfile<MappingProfile>(); });
            var aaaSettingsSection = ((AAASettingsSection) (ConfigurationManager.GetSection("AAASettings")));
            if (aaaSettingsSection.ReinizializeAutorizeObject)
            {
                var repository = aaaRepositoryFactory.Create(uof);
                var parser = new ReflectionParserClass();
                foreach (var authorizeObject in parser.GetAllMembersFromAssembly(GetType().Assembly.FullName))
                {
                    repository.CreateOrUpdateByNameAutorizeObject(authorizeObject);
                }
                uof.Save();
            }
        }
    }
}