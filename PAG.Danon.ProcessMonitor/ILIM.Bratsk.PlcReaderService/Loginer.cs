﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.WBD.Milk.Infrastructure.AAA.AuthenticationClasses;

namespace PAG.WBD.Milk.PlcReaderService
{
   public class Loginer
   {
       private IAAAServiceRemoteFasade _aaaServiceRemoteFasade;
       public Loginer(IAAAServiceRemoteFasade aaaServiceRemoteFasade)
       {
           _aaaServiceRemoteFasade = aaaServiceRemoteFasade;
       }

       public bool Login()
       {
          return _aaaServiceRemoteFasade.Login("System", "1", false).IsSucces;
       }
    }
}
