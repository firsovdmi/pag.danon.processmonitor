﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.PlcReaderService
{
    public sealed class PLCSettings
    {

        private static PLCSettingsSection _config;

        static PLCSettings()
        {
            _config = ((PLCSettingsSection)(global::System.Configuration.ConfigurationManager.GetSection("PLCSettings")));
        }

        private PLCSettings()
        {
        }

        public static PLCSettingsSection Config
        {
            get
            {
                return _config;
            }
        }
    }

    public sealed partial class PLCSettingsSection : System.Configuration.ConfigurationSection
    {

        [System.Configuration.ConfigurationPropertyAttribute("PLC")]
        public PLCElement PLC
        {
            get
            {
                return ((PLCElement)(this["PLC"]));
            }
        }

        [System.Configuration.ConfigurationPropertyAttribute("Buffer")]
        public BufferElement Buffer
        {
            get
            {
                return ((BufferElement)(this["Buffer"]));
            }
        }

        [System.Configuration.ConfigurationPropertyAttribute("ConnectionString")]
        public ConnectionStringElement ConnectionString
        {
            get
            {
                return ((ConnectionStringElement)(this["ConnectionString"]));
            }
        }

        public sealed partial class PLCElement : System.Configuration.ConfigurationElement
        {

            [System.Configuration.ConfigurationPropertyAttribute("Rack", IsRequired = true)]
            public long Rack
            {
                get
                {
                    return ((long)(this["Rack"]));
                }
                set
                {
                    this["Rack"] = value;
                }
            }

            [System.Configuration.ConfigurationPropertyAttribute("Slot", IsRequired = true)]
            public long Slot
            {
                get
                {
                    return ((long)(this["Slot"]));
                }
                set
                {
                    this["Slot"] = value;
                }
            }

            [System.Configuration.ConfigurationPropertyAttribute("IP", IsRequired = true)]
            public string IP
            {
                get
                {
                    return ((string)(this["IP"]));
                }
                set
                {
                    this["IP"] = value;
                }
            }
        }

        public sealed partial class BufferElement : System.Configuration.ConfigurationElement
        {

            [System.Configuration.ConfigurationPropertyAttribute("startAdress", IsRequired = true)]
            public long StartAdress
            {
                get
                {
                    return ((long)(this["startAdress"]));
                }
                set
                {
                    this["startAdress"] = value;
                }
            }

            [System.Configuration.ConfigurationPropertyAttribute("dbN", IsRequired = true)]
            public long DbN
            {
                get
                {
                    return ((long)(this["dbN"]));
                }
                set
                {
                    this["dbN"] = value;
                }
            }

            [System.Configuration.ConfigurationPropertyAttribute("bufferSize", IsRequired = true)]
            public long BufferSize
            {
                get
                {
                    return ((long)(this["bufferSize"]));
                }
                set
                {
                    this["bufferSize"] = value;
                }
            }

            [System.Configuration.ConfigurationPropertyAttribute("scanIntervalInMilliseconds", IsRequired = true)]
            public long ScanIntervalInMilliseconds
            {
                get
                {
                    return ((long)(this["scanIntervalInMilliseconds"]));
                }
                set
                {
                    this["scanIntervalInMilliseconds"] = value;
                }
            }
        }

        public sealed partial class ConnectionStringElement : System.Configuration.ConfigurationElement
        {

            [System.Configuration.ConfigurationPropertyAttribute("Value", IsRequired = true)]
            public string Value
            {
                get
                {
                    return ((string)(this["Value"]));
                }
                set
                {
                    this["Value"] = value;
                }
            }
        }
    }


    public sealed class CommonSettings
    {

        private static CommonSettingsSection _config;

        static CommonSettings()
        {
            _config = ((CommonSettingsSection)(global::System.Configuration.ConfigurationManager.GetSection("CommonSettings")));
        }

        private CommonSettings()
        {
        }

        public static CommonSettingsSection Config
        {
            get
            {
                return _config;
            }
        }
    }

    public sealed partial class CommonSettingsSection : System.Configuration.ConfigurationSection
    {

        [System.Configuration.ConfigurationPropertyAttribute("LocalMachine")]
        public LocalMachineElement LocalMachine
        {
            get
            {
                return ((LocalMachineElement)(this["LocalMachine"]));
            }
        }

        public sealed partial class LocalMachineElement : System.Configuration.ConfigurationElement
        {

            [System.Configuration.ConfigurationPropertyAttribute("Name", IsRequired = true)]
            public string Name
            {
                get
                {
                    return ((string)(this["Name"]));
                }
                set
                {
                    this["Name"] = value;
                }
            }
        }
    }


}
