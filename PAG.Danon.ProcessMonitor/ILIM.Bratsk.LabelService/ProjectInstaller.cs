﻿using System.ComponentModel;

namespace PAG.Danon.ProcessMonitor.LabelService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
