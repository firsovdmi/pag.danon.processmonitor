﻿using System;
using System.Configuration;

namespace PAG.Danon.ProcessMonitor.LabelService
{
    public static class CommonSettings
    {
        public static string HostAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["HostAddress"];
            }
        }

        public static int ScanIntervalInMilliseconds
        {
            get
            {
                return Convert.ToInt32( ConfigurationManager.AppSettings["ScanIntervalInMilliseconds"])*1000;
            }
        }
    }
}
