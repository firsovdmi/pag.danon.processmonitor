﻿using System.Configuration.Install;
using System.Reflection;
using System.ServiceProcess;
using PAG.Danon.ProcessMonitor.PlcScaner;

namespace PAG.Danon.ProcessMonitor.LabelService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                if (args[0] == "/i")
                {
                    ManagedInstallerClass.InstallHelper(new[] {Assembly.GetExecutingAssembly().Location});
                }
                else if (args[0] == "/u")
                {
                    ManagedInstallerClass.InstallHelper(new[] {"/u", Assembly.GetExecutingAssembly().Location});
                }
                else if (args[0] == "/d")
                {
                    var dbgService = new PlcReader();
                    dbgService.OnDebug();
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new PlcReader()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
