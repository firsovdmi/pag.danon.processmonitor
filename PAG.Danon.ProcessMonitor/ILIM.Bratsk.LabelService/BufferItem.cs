﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs.BaseClases;

namespace PAG.Danon.ProcessMonitor.PlcBuffer
{

    class BufferItem
    {
        public BufferItem(Adress adress, int dbn)
        {
            var tmpAdress = new Adress(adress);

            Busy = new PlcProperty<bool>(tmpAdress, dbn); tmpAdress += 8 * 2;
            RecordDate = new PlcProperty<DateTime>(tmpAdress, dbn); tmpAdress += 8 * 8;
            PhaseID                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
            WorkID                     = new  PlcProperty<Int32>           (tmpAdress, dbn);      tmpAdress += 8 * 4;
              WorkID_SD                = new  PlcProperty<Int32>           (tmpAdress, dbn);      tmpAdress += 8 * 4;
            PlcID = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              PhaseStatus              = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              EventInfo                = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              OperatedID               = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Step_No                  = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              UnitStatus               = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
             Bool_0                    = new  PlcProperty<bool>            (tmpAdress, dbn);      tmpAdress += 1;
             Bool_1                    = new  PlcProperty<bool>            (tmpAdress, dbn);      tmpAdress += 1;
             Bool_2                    = new  PlcProperty<bool>            (tmpAdress, dbn);      tmpAdress += 1;
             Bool_3                    = new  PlcProperty<bool>            (tmpAdress, dbn);      tmpAdress += 1;
             Bool_4                    = new  PlcProperty<bool>            (tmpAdress, dbn);      tmpAdress += 1;
             Bool_5                    = new  PlcProperty<bool>            (tmpAdress, dbn);      tmpAdress += 1;
             Bool_6                    = new  PlcProperty<bool>            (tmpAdress, dbn);      tmpAdress += 1;
             Bool_7                    = new  PlcProperty<bool>            (tmpAdress, dbn);      tmpAdress += 1;
             Byte_1                    = new  PlcProperty<byte>            (tmpAdress, dbn);      tmpAdress += 8 ;
              Int_1                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Int_2                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Int_3                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Int_4                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Int_5                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Int_6                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Int_7                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Int_8                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Int_9                    = new  PlcProperty<Int16>           (tmpAdress, dbn);      tmpAdress += 8 * 2;
              Dint_1                   = new  PlcProperty<Int32>           (tmpAdress, dbn);      tmpAdress += 8 * 4;
              Real_1                   = new  PlcProperty<float>           (tmpAdress, dbn);        tmpAdress += 8 * 4;
            Real_2                   = new PlcProperty<float>(tmpAdress, dbn);


            
        }
public PlcProperty<bool>     Busy                         { get; set; }
public PlcProperty<DateTime> RecordDate                  { get; set; }
public PlcProperty<Int16>    PhaseID                       { get; set; }
public PlcProperty<Int32>    WorkID                      { get; set; }
public PlcProperty<Int32>    WorkID_SD                   { get; set; }
public PlcProperty<Int16>   PlcID                          { get; set; }
public PlcProperty<Int16>    PhaseStatus                 { get; set; }
public PlcProperty<Int16>    EventInfo                   { get; set; }
public PlcProperty<Int16>    OperatedID                  { get; set; }
public PlcProperty<Int16>    Step_No                     { get; set; }
public PlcProperty<Int16>    UnitStatus                  { get; set; }
public PlcProperty<bool>    Bool_0                      { get; set; }
public PlcProperty<bool>    Bool_1                      { get; set; }
public PlcProperty<bool>    Bool_2                      { get; set; }
public PlcProperty<bool>    Bool_3                      { get; set; }
public PlcProperty<bool>    Bool_4                      { get; set; }
public PlcProperty<bool>    Bool_5                      { get; set; }
public PlcProperty<bool>    Bool_6                      { get; set; }
public PlcProperty<bool>    Bool_7                      { get; set; }
public PlcProperty<byte>    Byte_1                      { get; set; }
public PlcProperty<Int16>    Int_1                       { get; set; }
public PlcProperty<Int16>    Int_2                       { get; set; }
public PlcProperty<Int16>    Int_3                       { get; set; }
public PlcProperty<Int16>    Int_4                       { get; set; }
public PlcProperty<Int16>    Int_5                       { get; set; }
public PlcProperty<Int16>    Int_6                       { get; set; }
public PlcProperty<Int16>    Int_7                       { get; set; }
public PlcProperty<Int16>    Int_8                       { get; set; }
public PlcProperty<Int16>    Int_9                       { get; set; }
public PlcProperty<Int32>    Dint_1                      { get; set; }
public PlcProperty<float>    Real_1                      { get; set; }
public PlcProperty<float>    Real_2                      { get; set; }




        private IEnumerable<ReadDirectJob> _allReadJobs;

        public Domain.Model.PlcBuffer AsPlcBuffer
        {
            get
            {
                return new Domain.Model.PlcBuffer
                {
                    
                    RecordDate = RecordDate.Value,
                    PhaseID                =  PhaseID              .Value,
                   WorkID                 =  WorkID               .Value,
                   WorkID_SD              =  WorkID_SD            .Value,
                    PlcID = PlcID.Value,
                   PhaseStatus            =  PhaseStatus          .Value,
                   EventInfo              =  EventInfo            .Value,
                   OperatedID             =  OperatedID           .Value,
                   Step_No                =  Step_No              .Value,
                   UnitStatus             =  UnitStatus           .Value,
                  Bool_0                  = Bool_0                .Value,
                  Bool_1                  = Bool_1                .Value,
                  Bool_2                  = Bool_2                .Value,
                  Bool_3                  = Bool_3                .Value,
                  Bool_4                  = Bool_4                .Value,
                  Bool_5                  = Bool_5                .Value,
                  Bool_6                  = Bool_6                .Value,
                  Bool_7                  = Bool_7                .Value,
                  Byte_1                  = Byte_1                .Value,
                   Int_1                  =  Int_1                .Value,
                   Int_2                  =  Int_2                .Value,
                   Int_3                  =  Int_3                .Value,
                   Int_4                  =  Int_4                .Value,
                   Int_5                  =  Int_5                .Value,
                   Int_6                  =  Int_6                .Value,
                   Int_7                  =  Int_7                .Value,
                   Int_8                  =  Int_8                .Value,
                   Int_9                  =  Int_9                .Value,
                   Dint_1                 =  Dint_1               .Value,
                   Real_1                 =  Real_1               .Value,
                   Real_2 = Real_2.Value
                };
            }
        }

        public IEnumerable<ReadDirectJob> AllReadJobs
        {
            get
            {
                if (_allReadJobs == null)
                {
                    var rJobs = new List<ReadDirectJob>
                    {
                         Busy                     .ReadJobs,
                         RecordDate               .ReadJobs,
                        PhaseID                    .ReadJobs,
                        WorkID                     .ReadJobs,
                        WorkID_SD                  .ReadJobs,
                        PlcID                         .ReadJobs,
                        PhaseStatus                .ReadJobs,
                        EventInfo                  .ReadJobs,
                        OperatedID                 .ReadJobs,
                        Step_No                    .ReadJobs,
                        UnitStatus                 .ReadJobs,
                       Bool_0                      .ReadJobs,
                       Bool_1                      .ReadJobs,
                       Bool_2                      .ReadJobs,
                       Bool_3                      .ReadJobs,
                       Bool_4                      .ReadJobs,
                       Bool_5                      .ReadJobs,
                       Bool_6                      .ReadJobs,
                       Bool_7                      .ReadJobs,
                       Byte_1                      .ReadJobs,
                        Int_1                      .ReadJobs,
                        Int_2                      .ReadJobs,
                        Int_3                      .ReadJobs,
                        Int_4                      .ReadJobs,
                        Int_5                      .ReadJobs,
                        Int_6                      .ReadJobs,
                        Int_7                      .ReadJobs,
                        Int_8                      .ReadJobs,
                        Int_9                      .ReadJobs,
                        Dint_1                     .ReadJobs,
                        Real_1                     .ReadJobs,
                        Real_2                     .ReadJobs,
                    };
                    _allReadJobs = rJobs;
                }
                return _allReadJobs;
            }
        }


        public void ReciveData(BatchDirectJobs rJobs)
        {
            if (rJobs != null)
            {
                Busy                        .FindValue(rJobs);
                RecordDate                  .FindValue(rJobs);
                PhaseID                    .FindValue(rJobs);
                WorkID                     .FindValue(rJobs);
                  WorkID_SD                .FindValue(rJobs);
                PlcID.FindValue(rJobs);
                  PhaseStatus              .FindValue(rJobs);
                  EventInfo                .FindValue(rJobs);
                  OperatedID               .FindValue(rJobs);
                  Step_No                  .FindValue(rJobs);
                  UnitStatus               .FindValue(rJobs);
                 Bool_0                    .FindValue(rJobs);
                 Bool_1                    .FindValue(rJobs);
                 Bool_2                    .FindValue(rJobs);
                 Bool_3                    .FindValue(rJobs);
                 Bool_4                    .FindValue(rJobs);
                 Bool_5                    .FindValue(rJobs);
                 Bool_6                    .FindValue(rJobs);
                 Bool_7                    .FindValue(rJobs);
                 Byte_1                    .FindValue(rJobs);
                  Int_1                    .FindValue(rJobs);
                  Int_2                    .FindValue(rJobs);
                  Int_3                    .FindValue(rJobs);
                  Int_4                    .FindValue(rJobs);
                  Int_5                    .FindValue(rJobs);
                  Int_6                    .FindValue(rJobs);
                  Int_7                    .FindValue(rJobs);
                  Int_8                    .FindValue(rJobs);
                  Int_9                    .FindValue(rJobs);
                  Dint_1                   .FindValue(rJobs);
                  Real_1                   .FindValue(rJobs);
                Real_2.FindValue(rJobs);
            }
        }
    }
}
