﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using Castle.Facilities.TypedFactory;
using Castle.Facilities.WcfIntegration;
using Castle.Facilities.WcfIntegration.Behaviors;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Infrastructure.Log;
using PAG.Danon.ProcessMonitor.LabelService;
using PAG.Danon.ProcessMonitor.PlcBuffer;
using Component = Castle.MicroKernel.Registration.Component;


namespace PAG.Danon.ProcessMonitor.PlcScaner
{
    public partial class PlcReader : ServiceBase
    {
        private List<PlcToDBScaner> _bufferScaners;

        private BufferScaner _bufferHandler;
        private WindsorContainer _container;
        public PlcReader()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
            Thread.Sleep(Timeout.Infinite);
        }

        protected override void OnStart(string[] args)
        {
            var success = false;
            var config = RegisterPlcsConfig.GetConfig();
            while (!success)
            {
                try
                {
                    _bufferScaners = new List<PlcToDBScaner>(config.PlcItems.OfType<PlcItem>().Select(p => new PlcBuffer.PlcToDBScaner
                                               (
                                               p.Ip,
                                               p.Rack,
                                               p.Slot,
                                               p.StartAdress,
                                               p.DbN,
                                               p.BufferSize,
                                               p.ScanIntervalInMilliseconds,
                                               p.ConnectionString
                                               )));

                    
                    foreach (var bufferScaner in _bufferScaners)
                    {
                        bufferScaner.StartScan();
                    }
                    success = true;
                }
                catch
                {
                }
            }

            success = false;
            while (!success)
            {
                try
                {
                    _container = new WindsorContainer();
                    _container.AddFacility<TypedFactoryFacility>();
                    _container.AddFacility<WcfFacility>();

                    _container.Register(Component.For<MessageLifecycleBehavior>());
                    _container.Register(Component.For<LifestyleClientMessageAction>());
                    _container.Register(
                        Component.For<IActiveAccountManager>()
                                 .ImplementedBy<ActiveAcountManagerClient>()
                                 .OverridesExistingRegistration());//////////////////////////////////////////////////////////////////////////////

                    _container.Register(
                        Component.For(typeof(IChannelFactoryBuilder<>))
                                 .ImplementedBy(typeof(DefaultChannelFactoryBuilder<>))
                                 .IsDefault());

                    _container.Register(
                        Types.FromAssemblyContaining<IPlcAcessRemoteService>()
                             .InSameNamespaceAs<IPlcAcessRemoteService>()
                             .Configure(p =>
                             {
                                 p.AsWcfClient(new DefaultClientModel()
                                 {
                                     Endpoint = WcfEndpoint
                                                   .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                                                   .At(
                                                       string.Format(
                                                           @"net.tcp://" + CommonSettings.HostAddress +
                                                           ":8082/service/{0}", p.Implementation.Name.Substring(1)))
                                 });
                             }));

                    _container.Register(Component.For<IAAAServiceRemoteFasade>().AsWcfClient(new DefaultClientModel
                    {
                        Endpoint = WcfEndpoint
                                                                                                 .BoundTo(
                                                                                                     new NetTcpBinding(
                                                                                                         "netTcp")
                                                                                                     {
                                                                                                         PortSharingEnabled
                                                                                                                 = true
                                                                                                     })
                                                                                                 .At(
                                                                                                     string.Format(
                                                                                                         @"net.tcp://" +
                                                                                                         CommonSettings
                                                                                                             .HostAddress +
                                                                                                         ":8082/service/AAAServiceRemoteFasade"))
                    }));
                    _container.Register(Component.For<IReadLogRemoteService>().AsWcfClient(new DefaultClientModel
                    {
                        Endpoint = WcfEndpoint
                                                                                               .BoundTo(
                                                                                                   new NetTcpBinding(
                                                                                                       "netTcp")
                                                                                                   {
                                                                                                       PortSharingEnabled
                                                                                                               = true
                                                                                                   })
                                                                                               .At(
                                                                                                   string.Format(
                                                                                                       @"net.tcp://" +
                                                                                                       CommonSettings.HostAddress +
                                                                                                       ":8082/service/ReadLogRemoteService"))
                    }));

                    _container.Register(Component.For<Loginer>().LifestyleTransient());
                    var loginer = _container.Resolve<Loginer>();
                    loginer.Login();
                    var anyPlcItem = config.PlcItems.OfType<PlcItem>().FirstOrDefault();
                    var scanIntervalInMilliseconds = anyPlcItem == null ? 1000 : anyPlcItem.ScanIntervalInMilliseconds;
                    _container.Register(
                        Component.For<BufferScaner>()
                                 .DependsOn(
                                     (Dependency.OnValue("scanIntervalInMilliseconds",
                                                         scanIntervalInMilliseconds))));

                    _bufferHandler = _container.Resolve<BufferScaner>();
                    _bufferHandler.StartScan();
                    success = true;
                }
                catch(Exception ex)
                {
                }
            }
        }

        protected override void OnStop()
        {

            foreach (var bufferScaner in _bufferScaners)
            {
                bufferScaner.StopScan();
            }
            _bufferHandler.StopScan();
            _bufferHandler = null;
        }
    }
    public static class WindsorExtensions
        {
        public static ComponentRegistration<T> OverridesExistingRegistration<T>(
            this ComponentRegistration<T> componentRegistration) where T : class
            {
            return componentRegistration
                .Named(Guid.NewGuid().ToString())
                .IsDefault();
            }
        }
    }
