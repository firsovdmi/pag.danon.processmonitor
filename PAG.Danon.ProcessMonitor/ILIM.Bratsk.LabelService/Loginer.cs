﻿using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;

namespace PAG.Danon.ProcessMonitor.LabelService
{
    public class Loginer
    {
        private IAAAServiceRemoteFasade _aaaServiceRemoteFasade;
        public Loginer(IAAAServiceRemoteFasade aaaServiceRemoteFasade)
        {
            _aaaServiceRemoteFasade = aaaServiceRemoteFasade;
        }

        public bool Login()
        {
            return _aaaServiceRemoteFasade.Login("System", "1", false, "").IsSucces;
        }
    }
}
