﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.Danon.ProcessMonitor.PlcScaner
{
    public class PlcItem : ConfigurationElement
    {
        [ConfigurationProperty("Id", IsRequired = true)]
        public string Id
        {
            get
            {
                return this["Id"] as string;
            }
        }

        [ConfigurationProperty("Ip", IsRequired = true)]
        public string Ip
        {
            get
            {
                return this["Ip"] as string;
            }
        }
        [ConfigurationProperty("Rack", IsRequired = true)]
        public int Rack
        {
            get
            {
                return this["Rack"] is int ? (int)this["Rack"] : 0;
            }
        }
        [ConfigurationProperty("Slot", IsRequired = true)]
        public int Slot
        {
            get
            {
                return this["Slot"] is int ? (int)this["Slot"] : 0;
            }
        }
        [ConfigurationProperty("DbN", IsRequired = true)]
        public int DbN
        {
            get
            {
                return this["DbN"] is int ? (int)this["DbN"] : 0;
            }
        }
        [ConfigurationProperty("StartAdress", IsRequired = true)]
        public int StartAdress
        {
            get
            {
                return this["StartAdress"] is int ? (int)this["StartAdress"] : 0;
            }
        }
        [ConfigurationProperty("BufferSize", IsRequired = true)]
        public int BufferSize
        {
            get
            {
                return this["BufferSize"] is int ? (int)this["BufferSize"] : 0;
            }
        }
        [ConfigurationProperty("ScanIntervalInMilliseconds", IsRequired = true)]
        public int ScanIntervalInMilliseconds
        {
            get
            {
                return this["ScanIntervalInMilliseconds"] is int ? (int)this["ScanIntervalInMilliseconds"] : 0;
            }
        }
        [ConfigurationProperty("ConnectionString", IsRequired = true)]
        public string ConnectionString
        {
            get
            {
                return this["ConnectionString"] as string;
            }
        }
    }

    public class PlcItems
        : ConfigurationElementCollection
    {
        public PlcItem this[int index]
        {
            get
            {
                return base.BaseGet(index) as PlcItem;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public new PlcItem this[string responseString]
        {
            get { return (PlcItem)BaseGet(responseString); }
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new PlcItem();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PlcItem)element).Id;
        }
    }

    public class RegisterPlcsConfig
        : ConfigurationSection
    {

        public static RegisterPlcsConfig GetConfig()
        {
            return (RegisterPlcsConfig)ConfigurationManager.GetSection("XPlcItems") ?? new RegisterPlcsConfig();
        }

        [System.Configuration.ConfigurationProperty("PlcItems")]
        [ConfigurationCollection(typeof(PlcItems), AddItemName = "PlcItem")]
        public PlcItems PlcItems
        {
            get
            {
                object o = this["PlcItems"];
                return o as PlcItems;
            }
        }

    }
}
