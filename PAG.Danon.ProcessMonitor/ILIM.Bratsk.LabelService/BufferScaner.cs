﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.PlcScaner
{
    class BufferScaner : BaseCyclicHandler
    {
        private IPlcBufferHandlerService _plcBuffer;

        public BufferScaner(IPlcBufferHandlerService plcBuffer, int scanIntervalInMilliseconds)
        {
            _plcBuffer = plcBuffer;
            ScanIntervalInMilliseconds = scanIntervalInMilliseconds;
        }

        protected override void Handler()
        {
            //_plcBuffer.HandleBufferRecords();
            _plcBuffer.MyHandlerBufferRecords();
        }
    }
}
