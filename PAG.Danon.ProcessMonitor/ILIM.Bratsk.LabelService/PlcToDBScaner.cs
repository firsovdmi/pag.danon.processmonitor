﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.CommonClasses;
using PAG.Danon.ProcessMonitor.Domain.Model.PlcClasses.Jobs;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using PAG.Danon.ProcessMonitor.Service.Services;

namespace PAG.Danon.ProcessMonitor.PlcBuffer
{
    public class PlcToDBScaner : BaseCyclicHandler
    {
        readonly IPlcAcessService _directConnector;
        private readonly Adress _startAdress;
        private readonly int _dbN;
        private readonly int _bufferSize;
        readonly List<BufferItem> _buffer = new List<BufferItem>();
        private string _connectionString;

        private string _sqlInsertQuery = @"INSERT INTO [dbo].[PlcBuffers]
           (
	[ID]
,[PlcID]
  ,[Status]
  ,[PlcIp]
  ,[PhaseID]
  ,[WorkID]
  ,[WorkID_SD]
  ,[PhaseStatus]
  ,[EventInfo]
  ,[OperatedID]
  ,[Step_No]
  ,[UnitStatus]
  ,[Bool_0]
  ,[Bool_1]
  ,[Bool_2]
  ,[Bool_3]
  ,[Bool_4]
  ,[Bool_5]
  ,[Bool_6]
  ,[Bool_7]
  ,[Byte_1]
  ,[Int_1]
  ,[Int_2]
  ,[Int_3]
  ,[Int_4]
  ,[Int_5]
  ,[Int_6]
  ,[Int_7]
  ,[Int_8]
  ,[Int_9]
  ,[Dint_1]
  ,[Real_1]
  ,[Real_2]
  ,[RecordDate]  
  ,[Name]
  ,[UserCreated]
  ,[UserUpdated]
  ,[IsSystem]
  ,[DateTimeCreate]
  ,[DateTimeUpdate]
  ,[IsDeleted]
  ,Ip
)
    VALUES
    (
         NEWID() 
,@PlcID
         ,0
         ,@PlcIp
         ,@PhaseID 
         ,@WorkID 
         ,@WorkID_SD 
         ,@PhaseStatus 
         ,@EventInfo 
         ,@OperatedID 
         ,@Step_No 
         ,@UnitStatus 
         ,@Bool_0 
         ,@Bool_1 
         ,@Bool_2 
         ,@Bool_3 
         ,@Bool_4 
         ,@Bool_5 
         ,@Bool_6 
         ,@Bool_7 
         ,@Byte_1 
         ,@Int_1 
         ,@Int_2 
         ,@Int_3 
         ,@Int_4 
         ,@Int_5 
         ,@Int_6 
         ,@Int_7 
         ,@Int_8 
         ,@Int_9 
         ,@Dint_1 
         ,@Real_1 
         ,@Real_2 
         ,@RecordDate 
         ,null 
         ,null
         ,null
         ,0
         ,GETDATE() 
         ,GETDATE()
         ,'0' 
,@Ip
         
    )";

        public PlcToDBScaner(string ip, int rack, int slot, int startAdress, int dbN, int bufferSize, int scanIntervalInMilliseconds, string connectionString)
        {
            _directConnector = new PlcAcessService(ip, rack, slot);
            _startAdress = new Adress(startAdress * 8);
            _dbN = dbN;
            _bufferSize = bufferSize;
            ScanIntervalInMilliseconds = scanIntervalInMilliseconds;
            _connectionString = connectionString;
        }

        protected override void Handler()
        {
            var allReadJobs = new BatchDirectJobs(_buffer.SelectMany(p => p.AllReadJobs));
            allReadJobs = _directConnector.ProcessJobs(allReadJobs);
            foreach (var p in _buffer)
            {
                p.ReciveData(allReadJobs);
            }
            var busyItems = _buffer.Where(p => p.Busy.Value);
            var bufferItems = busyItems as IList<BufferItem> ?? busyItems.ToList();
            if (bufferItems.Any())
            {
                WriteDataToDb(bufferItems.Select(p => p.AsPlcBuffer));
                foreach (var p in bufferItems)
                {
                    p.Busy.Value = false;
                }
                var resetBusyJobs = new BatchDirectJobs(bufferItems.Select(p => p.Busy.WriteJobs));
                _directConnector.ProcessJobs(resetBusyJobs);
            }
        }

        protected override void Initialize()
        {
            var adress = new Adress(_startAdress);
            for (int i = 0; i < _bufferSize; i++)
            {
                _buffer.Add(new BufferItem(adress, _dbN));
                adress += 64 * 8;
            }
        }

        void WriteDataToDb(IEnumerable<Domain.Model.PlcBuffer> items)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var command = new SqlCommand(_sqlInsertQuery, connection);
               

                connection.Open();
                foreach (var p in items)
                {
                    command.Parameters.AddWithValue("@PlcIp", p.PlcIp);
                    command.Parameters.AddWithValue("@PlcID", p.PlcID);
                    command.Parameters.AddWithValue("@PhaseID ", p.PhaseID);
                    command.Parameters.AddWithValue("@WorkID ", p.WorkID);
                    command.Parameters.AddWithValue("@WorkID_SD ", p.WorkID_SD);
                    command.Parameters.AddWithValue("@PhaseStatus ", p.PhaseStatus);
                    command.Parameters.AddWithValue("@EventInfo ", p.EventInfo);
                    command.Parameters.AddWithValue("@OperatedID ", p.OperatedID);
                    command.Parameters.AddWithValue("@Step_No ", p.Step_No);
                    command.Parameters.AddWithValue("@UnitStatus ", p.UnitStatus);
                    command.Parameters.AddWithValue("@Bool_0 ", p.Bool_0);
                    command.Parameters.AddWithValue("@Bool_1 ", p.Bool_1);
                    command.Parameters.AddWithValue("@Bool_2 ", p.Bool_2);
                    command.Parameters.AddWithValue("@Bool_3 ", p.Bool_3);
                    command.Parameters.AddWithValue("@Bool_4 ", p.Bool_4);
                    command.Parameters.AddWithValue("@Bool_5 ", p.Bool_5);
                    command.Parameters.AddWithValue("@Bool_6 ", p.Bool_6);
                    command.Parameters.AddWithValue("@Bool_7 ", p.Bool_7);
                    command.Parameters.AddWithValue("@Byte_1 ", p.Byte_1);
                    command.Parameters.AddWithValue("@Int_1 ", p.Int_1);
                    command.Parameters.AddWithValue("@Int_2 ", p.Int_2);
                    command.Parameters.AddWithValue("@Int_3 ", p.Int_3);
                    command.Parameters.AddWithValue("@Int_4 ", p.Int_4);
                    command.Parameters.AddWithValue("@Int_5 ", p.Int_5);
                    command.Parameters.AddWithValue("@Int_6 ", p.Int_6);
                    command.Parameters.AddWithValue("@Int_7 ", p.Int_7);
                    command.Parameters.AddWithValue("@Int_8 ", p.Int_8);
                    command.Parameters.AddWithValue("@Int_9 ", p.Int_9);
                    command.Parameters.AddWithValue("@Dint_1 ", p.Dint_1);
                    command.Parameters.AddWithValue("@Real_1 ", p.Real_1);
                    command.Parameters.AddWithValue("@Real_2 ", p.Real_2);
                    command.Parameters.AddWithValue("@Ip ",int.Parse( ((PlcAcessService)_directConnector).Ip.Split('.').Last()));
                    var date = p.RecordDate;
                    var minDate = (DateTime) SqlDateTime.MinValue;
                    if (date < minDate)
                    { command.Parameters.AddWithValue("@RecordDate ", SqlDateTime.MinValue); }
                    else
                    { command.Parameters.AddWithValue("@RecordDate ", date); }

                    command.ExecuteNonQuery();
                    command.Parameters.Clear();
                }
                connection.Close();
            }
        }
    }
}
