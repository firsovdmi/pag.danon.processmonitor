﻿using System.Data.Entity;
using ILIM.Bratsk.Data;

namespace ILIM.Bratsk.IntegrationTest
{
    public class MyDatabaseInitializer : DropCreateDatabaseAlways<FactoryContext>
    {
        protected override void Seed(FactoryContext context)
        {
            context.SaveChanges();
        }
    }
}