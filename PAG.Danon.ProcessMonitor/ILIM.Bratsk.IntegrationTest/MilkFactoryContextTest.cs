﻿using System.Transactions;
using NUnit.Framework;
using ILIM.Bratsk.Data;
using ILIM.Bratsk.Infrastructure.AAA.AuthenticationClasses;

namespace ILIM.Bratsk.IntegrationTest
{
    [TestFixture]
    public class FactoryContextTest
    {
        protected TransactionScope _transactionScope;

        [SetUp]
        public virtual void Setup()
        {
            _transactionScope = new TransactionScope(TransactionScopeOption.RequiresNew);
        }

        [TearDown]
        public virtual void TearDown()
        {
            _transactionScope.Dispose();
        }

        [Test]
        public void Test()
        {
        }
    }

    [TestFixture]
    public class ServiceDynamicProxyTest
    {
        [Test]
        public void Test()
        {
        }

        [Test]
        public void TestContainer()
        {
        }
    }
}