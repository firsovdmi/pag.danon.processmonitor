﻿using System.Windows.Controls;

namespace PAG.Danon.ProcessMonitor.Visualization.Views
{
    /// <summary>
    /// Interaction logic for YesNoDialogView.xaml
    /// </summary>
    public partial class YesNoDialogView : UserControl
    {
        public YesNoDialogView()
        {
            InitializeComponent();
        }
    }
}
