﻿using System.Configuration;

namespace PAG.Danon.ProcessMonitor.Visualization
{
    internal static class Settings
    {
        /// <summary>
        ///     Строка подключения
        /// </summary>
        public static string ConnectionString
        {
            get { return ConfigurationSettings.AppSettings["ConnectionString"]; }
        }
    }
}