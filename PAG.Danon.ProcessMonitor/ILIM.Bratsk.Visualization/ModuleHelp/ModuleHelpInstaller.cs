﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleHelp.MenuAction;
using PAG.Danon.ProcessMonitor.Visualization.ModuleHelp.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleHelp
{
    public class ModuleHelpInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IModule>().ImplementedBy<ModuleHelp>());
            container.Register(
                Types.FromThisAssembly()
                    .BasedOn(typeof(HelpMenuAction<>))
                    .If(p => p != typeof(HelpMenuAction<>))
                    .WithServices(typeof(HelpMenuAction)));

            container.Register(
        Component.For<AboutViewModel>()
            .ImplementedBy<AboutViewModel>()
            .LifestyleCustom<RealisableSingletoneLifestyleManager>());

        }
    }
}