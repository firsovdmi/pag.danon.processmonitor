﻿using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleHelp.MenuAction;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleHelp
{
    public class ModuleHelp: IModule
    {
        public ModuleHelp(IMenuManager menuManager,
            IEnumerable<HelpMenuAction> actionItems
            )
        {
            menuManager.ShowItem(new ActionItem("Помощь", null), 4);
            foreach (var actionItem in actionItems)
            {
                menuManager.WithParent("Помощь").ShowItem(actionItem);
            }
        }
    }
}