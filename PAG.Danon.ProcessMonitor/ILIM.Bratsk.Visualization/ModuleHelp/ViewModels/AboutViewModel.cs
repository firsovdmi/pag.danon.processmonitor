﻿using System.Reflection;
using Caliburn.Micro;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleHelp.ViewModels
{
    public class AboutViewModel : Screen
    {
        public AboutViewModel()
        {

        }

        public string Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version.ToString(); }
        }
    }
}