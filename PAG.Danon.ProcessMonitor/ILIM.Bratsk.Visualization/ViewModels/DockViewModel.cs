using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Visualization.ViewModels
{
    public class DockViewModel : Screen
    {
        private readonly IRadDockingManager _radDockingManagers;

        public DockViewModel(IRadDockingManager radDockingManagers)
        {
            _radDockingManagers = radDockingManagers;
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            _radDockingManagers.Init(this);
        }

        #region Overrides of Screen

        /// <summary>
        ///     Called when deactivating.
        /// </summary>
        /// <param name="close">Inidicates whether this instance will be closed.</param>
        protected override void OnDeactivate(bool close)
        {
            _radDockingManagers.CloseAllWindows();
            base.OnDeactivate(close);
        }

        #endregion
    }
}