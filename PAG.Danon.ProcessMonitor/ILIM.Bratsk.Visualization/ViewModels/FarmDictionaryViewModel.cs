﻿using System.Collections.ObjectModel;
using System.Linq;
using Caliburn.Micro;
using PAG.WBD.Milk.Domain.RemoteFacade;
using PAG.WBD.Milk.Visualization.Model;

namespace PAG.WBD.Milk.Visualization.ViewModels
{
    public class FarmDictionaryViewModel : DictionaryViewModelBase
    {
        private readonly IFarmService _farmService;

        public FarmDictionaryViewModel(IFarmService farmService)
        {
            _farmService = farmService;
            Items = new ObservableCollection<FarmModel>(_farmService.Get().Select(p => new FarmModel(p)));
            DisplayName = "Справочник ферм";
        }

        public FarmModel SelectedItem { get; set; }
        public ObservableCollection<FarmModel> Items { get; set; }


        #region SaveItemCommand
        protected override bool CanExecuteSaveItemCommand(object obj)
        {
            return SelectedItem != null && (SelectedItem.IsEdited || SelectedItem.IsDeleted || SelectedItem.IsAdded);
        }

        protected override void ExecuteSaveItemCommand(object obj)
        {
            if (SelectedItem.IsDeleted)
            {
                _farmService.Delete(SelectedItem.Content);
                Items.Remove(SelectedItem);
                return;
            }
            if (SelectedItem.IsAdded)
                _farmService.Create(SelectedItem.Content);
            else if (SelectedItem.IsEdited)
                _farmService.Update(SelectedItem.Content);
            SelectedItem.IsAdded = false;
            SelectedItem.IsDeleted = false;
            SelectedItem.IsEdited = false;
        }
        #endregion

        #region SaveItemsCommand
        protected override bool CanExecuteSaveItemsCommand(object obj)
        {
            return Items.Any(p => p.IsEdited || p.IsDeleted || p.IsAdded);
        }

        protected override void ExecuteSaveItemsCommand(object obj)
        {
            var itemsToDelete = Items.Where(p => p.IsDeleted).ToList();
            _farmService.Delete(itemsToDelete.Select(p => p.Content).ToList());
            foreach (var farm in itemsToDelete)
            {
                Items.Remove(farm);
            }

            var itemsToAdd = Items.Where(p => p.IsAdded).ToList();
            _farmService.Create(itemsToAdd.Select(p => p.Content).ToList());
            foreach (var farm in itemsToAdd)
            {
                farm.IsAdded = false; farm.IsEdited = false;
            }

            var itemsToUpdate = Items.Where(p => p.IsEdited).ToList();
            _farmService.Update(itemsToUpdate.Select(p => p.Content).ToList());
            foreach (var farm in itemsToUpdate)
            {
                farm.IsEdited = false;
            }
        }
        #endregion

        #region DeleteItemCommand
        protected override bool CanExecuteDeleteItemCommand(object obj)
        {
            return SelectedItem != null;
        }

        protected override void ExecuteDeleteItemCommand(object obj)
        {
            SelectedItem.IsDeleted = true;
        }
        #endregion

        #region EndEditHandlingCommand
        protected override bool CanExecuteEndEditHandlingCommand(object obj)
        {
            return true;
        }

        protected override void ExecuteEndEditHandlingCommand(object obj)
        {
            var farmModel = obj as FarmModel;
            if (farmModel != null)
            {
                if (farmModel.IsAdded != true)
                    farmModel.IsEdited = true;
            }
        }

        #endregion

    }
}
