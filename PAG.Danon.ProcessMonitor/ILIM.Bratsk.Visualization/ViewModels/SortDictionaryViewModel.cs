﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Caliburn.Micro;
using PAG.WBD.Milk.Data.Model.Dictionaries;
using PAG.WBD.Milk.Domain.RemoteFacade;
using PAG.WBD.Milk.Visualization.Model;
using PAG.WBD.Milk.Visualization.ViewModelClasses.Dictionaries;

namespace PAG.WBD.Milk.Visualization.ViewModels
{
    public class SortDictionaryViewModel : DictionaryViewModelBase
    {
        private readonly ISortService _sortService;

        public SortDictionaryViewModel(ISortService sortService)
        {
            _sortService = sortService;
            Items = new ObservableCollection<SortModel>(_sortService.Get().Select(p => new SortModel(p)));
            DisplayName = "Справочник сортов";
        }

        public SortModel SelectedItem { get; set; }
        public ObservableCollection<SortModel> Items { get; set; }


        #region SaveItemCommand
        protected override bool CanExecuteSaveItemCommand(object obj)
        {
            return SelectedItem != null && (SelectedItem.IsEdited || SelectedItem.IsDeleted || SelectedItem.IsAdded);
        }

        protected override void ExecuteSaveItemCommand(object obj)
        {
            if (SelectedItem.IsDeleted)
            {
                _sortService.Delete(SelectedItem.Content);
                Items.Remove(SelectedItem);
                return;
            }
            if (SelectedItem.IsAdded)
                _sortService.Create(SelectedItem.Content);
            else if (SelectedItem.IsEdited)
                _sortService.Update(SelectedItem.Content);
            SelectedItem.IsAdded = false;
            SelectedItem.IsDeleted = false;
            SelectedItem.IsEdited = false;
        }
        #endregion

        #region SaveItemsCommand
        protected override bool CanExecuteSaveItemsCommand(object obj)
        {
            return Items.Any(p => p.IsEdited || p.IsDeleted || p.IsAdded);
        }

        protected override void ExecuteSaveItemsCommand(object obj)
        {
            var itemsToDelete = Items.Where(p => p.IsDeleted).ToList();
            _sortService.Delete(itemsToDelete.Select(p => p.Content).ToList());
            foreach (var farm in itemsToDelete)
            {
                Items.Remove(farm);
            }

            var itemsToAdd = Items.Where(p => p.IsAdded).ToList();
            _sortService.Create(itemsToAdd.Select(p => p.Content).ToList());
            foreach (var farm in itemsToAdd)
            {
                farm.IsAdded = false; farm.IsEdited = false;
            }

            var itemsToUpdate = Items.Where(p => p.IsEdited).ToList();
            _sortService.Update(itemsToUpdate.Select(p => p.Content).ToList());
            foreach (var farm in itemsToUpdate)
            {
                farm.IsEdited = false;
            }
        }
        #endregion

        #region DeleteItemCommand
        protected override bool CanExecuteDeleteItemCommand(object obj)
        {
            return SelectedItem != null;
        }

        protected override void ExecuteDeleteItemCommand(object obj)
        {
            SelectedItem.IsDeleted = true;
        }
        #endregion

        #region EndEditHandlingCommand
        protected override bool CanExecuteEndEditHandlingCommand(object obj)
        {
            return true;
        }

        protected override void ExecuteEndEditHandlingCommand(object obj)
        {
            var sortModel = obj as SortModel;
            if (sortModel != null)
            {
                if (sortModel.IsAdded != true)
                    sortModel.IsEdited = true;
            }
        }

        #endregion

    }
}
