using System.Linq;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model.Alarms;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Visualization.ViewModels
{
    public class MainViewModel : Screen, IHandle<AccountDataChanged>
    {
        private readonly IAlarmRemoteService _alarmRemoteService;
        private IActiveAccountManager _activeAccountManager;
        public MainViewModel(IAlarmRemoteService alarmRemoteServic, IActiveAccountManager activeAccountManager)
        {
            _alarmRemoteService = alarmRemoteServic;
            _activeAccountManager = activeAccountManager;
        }

        public AlarmItem Message
        {
            get
            {
                var alarmItem = _alarmRemoteService.GetAlarmItems().OrderBy(p => p.AlarmLevel).FirstOrDefault();
                return alarmItem;
            }
        } 
        public ToolBarViewModel ToolBarRegion { get; set; }
        public DockViewModel DockRegion { get; set; }
        public MenuViewModel MenuRegion { get; set; }

        public bool IsCanEditSystemSettings { get { return _activeAccountManager.WhiteListObjects.Any(p => p == "SystemSettings"); } }
        public string LoginName{get { return _activeAccountManager.ActiveAccount.Account.Login; }}
        public void Handle(AccountDataChanged message)
        {
            NotifyOfPropertyChange(() => IsCanEditSystemSettings);
            NotifyOfPropertyChange(() => LoginName);
        }
    }
}