using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Windows;
using System.Windows.Threading;
using AutoMapper;
using Caliburn.Micro;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.Facilities.WcfIntegration;
using Castle.Facilities.WcfIntegration.Behaviors;
using Castle.MicroKernel.ModelBuilder.Inspectors;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Infrastructure.CallBackService.MilkReceiveData;
using PAG.Danon.ProcessMonitor.Infrastructure.DataBaseDirect;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Log;
using PAG.Danon.ProcessMonitor.Visualization.ErrorMessage;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleAdministration;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries;
using PAG.Danon.ProcessMonitor.Visualization.ModuleHelp;
using PAG.Danon.ProcessMonitor.Visualization.ModuleManager;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport;
using PAG.Danon.ProcessMonitor.Visualization.ViewModels;


namespace PAG.Danon.ProcessMonitor.Visualization.Bootstrap
{
    public class AppBootstrapper : BootstrapperBase
    {
        private WindsorContainer _container;

        public AppBootstrapper()
        {
            // Thread.Sleep(20000);
            Initialize();
        }

        protected override void Configure()
        {
            ConventionManager.ApplyValidation =
                (binding, viewModelType, property) => { binding.ValidatesOnExceptions = true; };

            //LogManager.GetLog = type => new DebugLogger(type);
            _container = new WindsorContainer();
            var propInjector = _container.Kernel.ComponentModelBuilder
.Contributors
.OfType<PropertiesDependenciesModelInspector>()
.Single();
            //_container.Kernel.ComponentModelBuilder.RemoveContributor(propInjector);


            _container.AddFacilityIfNotAdded<TypedFactoryFacility>();
            _container.AddFacilityIfNotAdded<StartableFacility>();
            _container.AddFacilityIfNotAdded<EventRegistrationFacility>();
            _container.AddFacilityIfNotAdded<WcfFacility>();
            _container.Register(Component.For<MessageLifecycleBehavior>());
            _container.Register(Component.For<LifestyleClientMessageAction>());
            //_container.Register(Component.For<IEndpointBehavior>().ImplementedBy<ServiceMessageInspectorBehavior>());

            

            _container.Kernel.Resolver.AddSubResolver(new CollectionResolver(_container.Kernel));
            _container.Register(Component.For<IMappingEngine>().UsingFactoryMethod(() => Mapper.Engine));

            //��� �� wcf, ����� �� ������� �� ����������� ����������� ������
            _container.Register(
                Component.For(typeof(IChannelFactoryBuilder<>))
                    .ImplementedBy(typeof(DefaultChannelFactoryBuilder<>))
                    .IsDefault());

            _container.Register(Component.For<IScreenFactory>().AsFactory().LifestyleTransient());
            
            _container.Register(Component.For<DockViewModel>());
            _container.Register(Component.For<LoginViewModel>().LifestyleTransient());
            _container.Register(Component.For<MainViewModel>());
            _container.Register(Component.For<AAAManager>());
            
            _container.Register(Component.For<IWindowManager>().ImplementedBy<TelerikWindowManager>());
            
            _container.Register(Component.For<IEventAggregator>().ImplementedBy<EventAggregator>());
_container.Register(Component.For<IShell>().ImplementedBy<ShellViewModel>().LifestyleTransient());
            _container.Register(Component.For<IRadDockingManager>().ImplementedBy<RadDockingManager>());
            _container.Register(Component.For<IMenuManager, MenuViewModel>().ImplementedBy<MenuViewModel>());
            _container.Register(Component.For<IToolBarManager, ToolBarViewModel>().ImplementedBy<ToolBarViewModel>());
            _container.Register(
                Component.For<IActiveAccountManager>()
                    .ImplementedBy<ActiveAcountManagerClient>()
                    .OverridesExistingRegistration());
            _container.Register(Component.For<ILoginViewModelFactory>().AsFactory().LifestyleTransient());
            _container.Register(Component.For<IEndpointBehavior>().ImplementedBy<SetMaxObjectsInGraphBehavior>());

            //������������ �������� ��������
            //------------
            //_container.Install(new LocalServiceInstaller());
            //------------
            

            _container.Install(new DictionaryModuleInstaller());
            _container.Install(new AdministrationModuleInstaller());
            _container.Install(new ManagerModuleInstaller());
            _container.Install(new ReportModuleInstaller());
            _container.Install(new ModuleHelpInstaller());


            TelerikConventions.Install();
            _container.Register(
                Component.For<DataContractSerializer>().DependsOn(new { MaxItemsInObjectGraph = 2147483647 }));
            //������������� �������� ��������
            //------------192.168.137.154
            _container.Register(
                Types.FromAssemblyContaining<IPhaseService>()
                    .InSameNamespaceAs<IPhaseService>()
                    .Configure(p =>
                    {
                        p.AsWcfClient(new DefaultClientModel
                        {
                            Endpoint = WcfEndpoint
                                .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                                .At(
                                    string.Format(
                                        @"net.tcp://" + CommonSettings.Config.LocalMachine.Name + ":8082/service/{0}",
                                        p.Implementation.Name.Substring(1)))
                        });
                    })
                );
            _container.Register(Component.For<IAAAServiceRemoteFasade>().AsWcfClient(new DefaultClientModel
            {
                Endpoint = WcfEndpoint
                    .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                    .At(
                        string.Format(@"net.tcp://" + CommonSettings.Config.LocalMachine.Name +
                                      ":8082/service/AAAServiceRemoteFasade"))
            }));
            _container.Register(Component.For<IReadLogRemoteService>().AsWcfClient(new DefaultClientModel
            {
                Endpoint = WcfEndpoint
                    .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                    .At(
                        string.Format(@"net.tcp://" + CommonSettings.Config.LocalMachine.Name +
                                      ":8082/service/ReadLogRemoteService"))
            }));
            _container.Register(Component.For<IDataBaseDirectService>().AsWcfClient(new DefaultClientModel
            {
                Endpoint = WcfEndpoint
                    .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                    .At(
                        string.Format(@"net.tcp://" + CommonSettings.Config.LocalMachine.Name +
                                      ":8082/service/DataBaseDirectService"))
            }));
            //------------

            //����������� ��� ����-��� �������
            _container.Register(Component.For<ILogSubscriber, LogSubscriber>()
                .ImplementedBy<LogSubscriber>()
                );

            //����������� ��������� ������ ����-��� �������
            _container.Register(
                Component.For<IReceivingTaskSubscriber, ReceivingTaskSubscriber>()
                    .ImplementedBy<ReceivingTaskSubscriber>());


            var serviceContractAttribute =
                (ServiceContractAttribute)
                    (Attribute.GetCustomAttribute(typeof(ILogPublisherRemote), typeof(ServiceContractAttribute)));

            _container.Register(Component.For<ILogPublisherRemote>().AsWcfClient(new DuplexClientModel
            {
                Endpoint = WcfEndpoint
                    .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                    .At(
                        string.Format(@"net.tcp://" + CommonSettings.Config.LocalMachine.Name +
                                      ":8082/service/LogPublisherRemote"))
            }.Callback(_container.Resolve(serviceContractAttribute.CallbackContract))));


            var serviceContractAttributeilkReceivingTaskPublisherRemote =
                (ServiceContractAttribute)
                    (Attribute.GetCustomAttribute(typeof(IReceivingTaskPublisherRemote),
                        typeof(ServiceContractAttribute)));

            _container.Register(Component.For<IReceivingTaskPublisherRemote>().AsWcfClient(new DuplexClientModel
            {
                Endpoint = WcfEndpoint
                    .BoundTo(new NetTcpBinding("netTcp") { PortSharingEnabled = true })
                    .At(
                        string.Format(@"net.tcp://" + CommonSettings.Config.LocalMachine.Name +
                                      ":8082/service/ReceivingTaskPublisherRemote"))
            }.Callback(_container.Resolve(serviceContractAttributeilkReceivingTaskPublisherRemote.CallbackContract))));

            //����� �������������� View ��� Viemodel, ���������� ��������� ViewAttribute
            var oldLoc = ViewLocator.LocateTypeForModelType;
            ViewLocator.LocateTypeForModelType = (modelType, displayLocation, context) =>
            {
                var customAttributes = modelType.GetCustomAttributes(typeof(ViewAttribute), true);
                var attribute =
                    customAttributes.OfType<ViewAttribute>().Where(x => x.Context == context).FirstOrDefault();
                if (attribute == null && context != null)
                {
                    attribute =
                        customAttributes.OfType<ViewAttribute>()
                            .Where(x => x.Context.ToString() == context.ToString())
                            .FirstOrDefault();
                }
                return attribute != null ? attribute.ViewType : oldLoc(modelType, displayLocation, context);
            };

            


        }

        protected override object GetInstance(Type serviceType, string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return _container.Resolve(serviceType);
            }
            return _container.Resolve(key, serviceType);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.ResolveAll(serviceType).Cast<object>();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            //    Thread.Sleep(10000);

            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            DisplayRootViewFor<IShell>();
            _container.ResolveAll<IModule>();
        }

        private void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            if (e.Exception is EndpointNotFoundException)
            {
                e.Handled = true;
                return;
            }
            var ex = e.Exception;
            var authorizationException = FindInnerException<FaultException>(e.Exception);
            if (authorizationException != null &&
                authorizationException.Code.Name == FaultsCodes.AutorizationFault.ToString())
            {
                var windowManager = _container.Resolve<IWindowManager>();
                var loginViewModelFactory = _container.Resolve<ILoginViewModelFactory>();

                var loginViewModel = loginViewModelFactory.Create(LoginType.Relogin);
                windowManager.ShowDialog(loginViewModel, null,
                    new Dictionary<string, object> { { "ResizeMode", ResizeMode.NoResize }, { "CanClose", false } });
                loginViewModelFactory.Release(loginViewModel);
                e.Handled = true;
                return;
            }


            var errorCaption = string.Format("{0}", ex.Source);
            // var errorMessage = string.Format("{0} (inner: {1})", ex.Message, ex.InnerException == null ? "�����������" :  ex.InnerException.Message);
            var errorMessage = GetInnerException(ex);
            var errorStack = GetInnerStack(ex);


            if (ErrorWindow.ShowError(errorCaption, errorMessage, errorStack))
            {
                if (Application.Current != null)
                {
                    Application.Current.Shutdown();
                }
            }
            e.Handled = true;
        }

        private string GetInnerException(Exception ex)
        {
            if (ex.InnerException == null) return ex.Message;
            return ex.Message + "\n\r" + GetInnerException(ex.InnerException);
        }

        private string GetInnerStack(Exception ex)
        {
            if (ex.InnerException == null) return ex.StackTrace;
            return ex.StackTrace + "\n\r !!!!!INNER!!! \n\r" + GetInnerStack(ex.InnerException);
        }

        private T FindInnerException<T>(Exception e) where T : Exception
        {
            if (e == null)
                return null;
            if (e is T)
                return (T)e;
            return FindInnerException<T>(e.InnerException);
        }
    }
    public class SetMaxObjectsInGraphBehavior : IEndpointBehavior
        {
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
            {
            foreach (var operation in endpoint.Contract.Operations)
                {
                var behavior = operation.Behaviors.Find<DataContractSerializerOperationBehavior>();
                behavior.MaxItemsInObjectGraph = 2147483647;
                }
            }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
            {
            }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
            {
            }

        public void Validate(ServiceEndpoint endpoint)
            {
            }
        }

    public static class WindsorExtensions
        {
        public static ComponentRegistration<T> OverridesExistingRegistration<T>(
            this ComponentRegistration<T> componentRegistration) where T : class
            {
            return componentRegistration
                .Named(Guid.NewGuid().ToString())
                .IsDefault();
            }
        }
    }