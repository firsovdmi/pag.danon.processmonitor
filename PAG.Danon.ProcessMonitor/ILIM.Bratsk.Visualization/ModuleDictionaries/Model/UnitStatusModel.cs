using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class UnitStatusModel : EntityWithEditStatusModel<UnitStatus>
    {
        public UnitStatusModel()
        {
            Content = new UnitStatus { ID = Guid.NewGuid()};
        }

        public UnitStatusModel(UnitStatus model)
            : base(model)
        {
        }
    }
}