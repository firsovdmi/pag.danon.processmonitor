using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class StepNoCIPModel : EntityWithEditStatusModel<StepNoCip>
    {
        public StepNoCIPModel()
        {
            Content = new StepNoCip { ID = Guid.NewGuid()};
        }

        public StepNoCIPModel(StepNoCip model)
            : base(model)
        {
        }
    }
}