using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class CheesePhaseModel : EntityWithEditStatusModel<CheesePhase>
    {
        public CheesePhaseModel()
        {
            Content = new CheesePhase { ID = Guid.NewGuid()};
        }

        public CheesePhaseModel(CheesePhase model)
            : base(model)
        {
        }
    }
}