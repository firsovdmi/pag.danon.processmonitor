using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class PhaseIDModel : EntityWithEditStatusModel<WorkMessage>
    {
        public PhaseIDModel()
        {
            Content = new WorkMessage { ID = Guid.NewGuid()};
        }

        public PhaseIDModel(WorkMessage model)
            : base(model)
        {
        }
    }
}