using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class PhaseStatusModel : EntityWithEditStatusModel<PhaseStatus>
    {
        public PhaseStatusModel()
        {
            Content = new PhaseStatus { ID = Guid.NewGuid()};
        }

        public PhaseStatusModel(PhaseStatus model)
            : base(model)
        {
        }
    }
}