using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class OperatorModel : EntityWithEditStatusModel<Operator>
    {
        public OperatorModel()
        {
            Content = new Operator { ID = Guid.NewGuid()};
        }

        public OperatorModel(Operator model)
            : base(model)
        {
        }
    }
}