using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class StepNoProdModel : EntityWithEditStatusModel<StepNoProd>
    {
        public StepNoProdModel()
        {
            Content = new StepNoProd { ID = Guid.NewGuid()};
        }

        public StepNoProdModel(StepNoProd model)
            : base(model)
        {
        }
    }
}