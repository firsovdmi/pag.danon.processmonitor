using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class AppCIPPhaseModel : EntityWithEditStatusModel<AppCIPPhase>
    {
        public AppCIPPhaseModel()
        {
            Content = new AppCIPPhase { ID = Guid.NewGuid()};
        }

        public AppCIPPhaseModel(AppCIPPhase model)
            : base(model)
        {
        }
    }
}