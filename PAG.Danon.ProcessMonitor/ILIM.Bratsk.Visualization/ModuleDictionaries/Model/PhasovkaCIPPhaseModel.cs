using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class PhasovkaCIPPhaseModel : EntityWithEditStatusModel<PhasovkaCIPPhase>
    {
        public PhasovkaCIPPhaseModel()
        {
            Content = new PhasovkaCIPPhase { ID = Guid.NewGuid()};
        }

        public PhasovkaCIPPhaseModel(PhasovkaCIPPhase model)
            : base(model)
        {
        }
    }
}