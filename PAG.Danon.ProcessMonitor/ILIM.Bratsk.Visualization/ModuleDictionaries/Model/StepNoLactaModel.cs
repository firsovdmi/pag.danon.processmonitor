using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class StepNoLactaModel : EntityWithEditStatusModel<StepNoLacta>
    {
        public StepNoLactaModel()
        {
            Content = new StepNoLacta { ID = Guid.NewGuid()};
        }

        public StepNoLactaModel(StepNoLacta model)
            : base(model)
        {
        }
    }
}