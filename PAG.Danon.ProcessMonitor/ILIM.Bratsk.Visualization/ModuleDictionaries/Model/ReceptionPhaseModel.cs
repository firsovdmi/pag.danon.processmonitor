using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class ReceptionPhaseModel : EntityWithEditStatusModel<ReceptionPhase>
    {
        public ReceptionPhaseModel()
        {
            Content = new ReceptionPhase { ID = Guid.NewGuid()};
        }

        public ReceptionPhaseModel(ReceptionPhase model)
            : base(model)
        {
        }
    }
}