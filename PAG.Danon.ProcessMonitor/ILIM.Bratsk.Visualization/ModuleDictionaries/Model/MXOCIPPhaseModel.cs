using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class MXOCIPPhaseModel : EntityWithEditStatusModel<MXOCIPPhase>
    {
        public MXOCIPPhaseModel()
        {
            Content = new MXOCIPPhase { ID = Guid.NewGuid()};
        }

        public MXOCIPPhaseModel(MXOCIPPhase model)
            : base(model)
        {
        }
    }
}