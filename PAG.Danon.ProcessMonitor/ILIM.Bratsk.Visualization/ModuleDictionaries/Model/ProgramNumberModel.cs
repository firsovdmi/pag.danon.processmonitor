using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class ProgramNumberModel : EntityWithEditStatusModel<ProgrammNumber>
    {
        public ProgramNumberModel()
        {
            Content = new ProgrammNumber() { ID = Guid.NewGuid()};
        }

        public ProgramNumberModel(ProgrammNumber model)
            : base(model)
        {
        }
    }
}