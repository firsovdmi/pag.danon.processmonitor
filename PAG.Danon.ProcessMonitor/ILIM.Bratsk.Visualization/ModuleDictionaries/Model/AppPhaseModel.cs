using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class AppPhaseModel : EntityWithEditStatusModel<AppPhase>
    {
        public AppPhaseModel()
        {
            Content = new AppPhase { ID = Guid.NewGuid()};
        }

        public AppPhaseModel(AppPhase model)
            : base(model)
        {
        }
    }
}