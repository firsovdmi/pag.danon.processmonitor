using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class StepNoOVATModel : EntityWithEditStatusModel<StepNoOVAT>
    {
        public StepNoOVATModel()
        {
            Content = new StepNoOVAT { ID = Guid.NewGuid()};
        }

        public StepNoOVATModel(StepNoOVAT model)
            : base(model)
        {
        }
    }
}