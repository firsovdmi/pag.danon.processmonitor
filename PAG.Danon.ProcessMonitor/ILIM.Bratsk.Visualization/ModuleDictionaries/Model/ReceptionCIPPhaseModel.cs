using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class ReceptionCIPPhaseModel : EntityWithEditStatusModel<ReceptionCIPPhase>
    {
        public ReceptionCIPPhaseModel()
        {
            Content = new ReceptionCIPPhase { ID = Guid.NewGuid()};
        }

        public ReceptionCIPPhaseModel(ReceptionCIPPhase model)
            : base(model)
        {
        }
    }
}