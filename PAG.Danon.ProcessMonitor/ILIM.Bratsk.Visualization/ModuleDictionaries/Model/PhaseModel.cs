using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class PhaseModel : EntityWithEditStatusModel<Phase>
    {
        public PhaseModel()
        {
            Content = new Phase { ID = Guid.NewGuid()};
        }

        public PhaseModel(Phase model)
            : base(model)
        {
        }
    }
}