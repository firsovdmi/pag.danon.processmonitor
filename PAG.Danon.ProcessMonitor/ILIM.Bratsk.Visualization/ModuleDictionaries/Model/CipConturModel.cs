using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class CipConturModel : EntityWithEditStatusModel<CipContur>
    {
        public CipConturModel()
        {
            Content = new CipContur() { ID = Guid.NewGuid()};
        }

        public CipConturModel(CipContur model)
            : base(model)
        {
        }
    }
}