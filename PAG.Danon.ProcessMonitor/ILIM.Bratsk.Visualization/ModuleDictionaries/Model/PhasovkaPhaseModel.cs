using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class PhasovkaPhaseModel : EntityWithEditStatusModel<PhasovkaPhase>
    {
        public PhasovkaPhaseModel()
        {
            Content = new PhasovkaPhase { ID = Guid.NewGuid()};
        }

        public PhasovkaPhaseModel(PhasovkaPhase model)
            : base(model)
        {
        }
    }
}