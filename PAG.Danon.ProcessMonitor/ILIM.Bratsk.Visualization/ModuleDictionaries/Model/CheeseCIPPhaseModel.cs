using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class CheeseCIPPhaseModel : EntityWithEditStatusModel<CheeseCIPPhase>
    {
        public CheeseCIPPhaseModel()
        {
            Content = new CheeseCIPPhase { ID = Guid.NewGuid()};
        }

        public CheeseCIPPhaseModel(CheeseCIPPhase model)
            : base(model)
        {
        }
    }
}