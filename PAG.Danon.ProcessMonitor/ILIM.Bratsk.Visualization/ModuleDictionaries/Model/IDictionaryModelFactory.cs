using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public interface IDictionaryModelFactory
    {
        EntityWithEditStatusModel<T> Create<T>(T model) where T : EntityBase, new();
        void Release(object model);
    }
}