using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class ApplicationSettingsModel : EntityWithEditStatusModel<ApplicationSettings>
    {
        public ApplicationSettingsModel()
        {
            Content = new ApplicationSettings {ID = Guid.NewGuid()};
        }

        public ApplicationSettingsModel(ApplicationSettings model)
            : base(model)
        {
        }
    }
}