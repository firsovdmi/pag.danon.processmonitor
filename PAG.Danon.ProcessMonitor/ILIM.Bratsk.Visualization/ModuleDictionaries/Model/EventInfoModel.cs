using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class EventInfoModel : EntityWithEditStatusModel<EventInfo>
    {
        public EventInfoModel()
        {
            Content = new EventInfo { ID = Guid.NewGuid()};
        }

        public EventInfoModel(EventInfo model)
            : base(model)
        {
        }
    }
}