using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model
{
    public class MXOPhaseModel : EntityWithEditStatusModel<MXOPhase>
    {
        public MXOPhaseModel()
        {
            Content = new MXOPhase { ID = Guid.NewGuid()};
        }

        public MXOPhaseModel(MXOPhase model)
            : base(model)
        {
        }
    }
}