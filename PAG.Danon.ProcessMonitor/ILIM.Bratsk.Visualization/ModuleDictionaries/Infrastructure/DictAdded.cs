using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Infrastructure
{
    public class DictAdded<T> : DictChanges<T> where T : IEntity
    {
        public DictAdded(T content, object sender)
            : base(content, sender)
        {
        }
    }
}