using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Infrastructure
{
    public class DictDeleted<T> : DictChanges<T> where T : IEntity
    {
        public DictDeleted(T content, object sender)
            : base(content, sender)
        {
        }
    }
}