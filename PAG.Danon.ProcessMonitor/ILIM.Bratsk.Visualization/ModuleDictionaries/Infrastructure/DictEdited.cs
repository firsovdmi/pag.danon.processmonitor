using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Infrastructure
{
    public class DictEdited<T> : DictChanges<T> where T : IEntity
    {
        public DictEdited(T content, object sender)
            : base(content, sender)
        {
        }
    }
}