using System.Linq;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Infrastructure
{
    public abstract class DictChanges<T> : Message<T> where T : IEntity
    {
        public DictChanges(T content, object sender)
        {
            Content = content;
            Sender = sender;
        }

        public T Content { get; set; }
        public object Sender { get; set; }

        /// <summary>
        ///     ���������� ������ ��������� �����������
        /// </summary>
        /// <typeparam name="T">��� �����������</typeparam>
        /// <param name="dict">������ ���������� �����������</param>
        /// <param name="dictionaryService">������ ������� � �����������</param>
        /// <param name="message">��������� �� ������� �� ����������</param>
        public void RefreashDictByMessage(BindableCollection<ShortEntity> dict, IDictionaryService<T> dictionaryService,
            DictChanges<T> message)
        {
            var oldDict = dict.Where(p => p != null).FirstOrDefault(p => p.ID == message.Content.ID);

            if (message is DictDeleted<T> && oldDict != null)
                dict.Remove(oldDict);
            if (message is DictAdded<T> || message is DictEdited<T>)
            {
                var newDict = dictionaryService.GetShort(message.Content.ID);
                if (message is DictAdded<T> && message != null)
                    dict.Add(newDict);
                if (message is DictEdited<T> && oldDict != null)
                    oldDict.Name = newDict.Name;
                dict.OrderBy(p => p.Name);
            }
        }
    }
}