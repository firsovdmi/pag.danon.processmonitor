using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.MenuAction
{
    public class ShowCIPAppPhaseDictionaryMenuAction : DictionaryMenuAction<DictionaryViewModelBase<AppCIPPhase>>
    {
        public ShowCIPAppPhaseDictionaryMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager)
            : base(radDockingManager, screenFactory, "���������� ��� ����� �����������", eventAggregator, activeAccountManager)
            {
            IsEnable = activeAccountManager.ActiveAccount.Account.Login.Equals("Admin1");

            }
        }
}