﻿using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.ToolBarAction;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.ViewModels
{
    public sealed class CheeseCIPPhaseDictionaryViewModel : DictionaryViewModelBase<CheeseCIPPhase>
    {

        public CheeseCIPPhaseDictionaryViewModel(IToolBarManager toolBarManager, DictionaryActionItems dictionaryActionItems,
            ICheeseCIPPhaseService dictionaryService, IEventAggregator eventAggregator,
            IDictionaryModelFactory dictionaryModelFactory,
            IWindowManager windowManager, IAccountDataService accountDataService, 
            IActiveAccountManager activeAccountManager)
            : base(
                toolBarManager, dictionaryActionItems, dictionaryService, eventAggregator, dictionaryModelFactory,
                windowManager, accountDataService, activeAccountManager)
        {
            DisplayName = "Справочник фаз мойки творожного";
            Initialize();
        }


        private void Initialize()
        {
           
        }

        #region Implementation of IHandle<AddNewDictionaryElement<Driver>>

        /// <summary>
        ///     Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(DictChanges<CheeseCIPPhase> message)
        {

        }

        #endregion
    }
}