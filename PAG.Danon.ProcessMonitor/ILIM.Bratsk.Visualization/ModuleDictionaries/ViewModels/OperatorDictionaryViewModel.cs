﻿using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.ToolBarAction;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.ViewModels
{
    public sealed class OperatorDictionaryViewModel : DictionaryViewModelBase<Operator>
    {

        public OperatorDictionaryViewModel(IToolBarManager toolBarManager, DictionaryActionItems dictionaryActionItems,
            IOperatorService dictionaryService, IEventAggregator eventAggregator,
            IDictionaryModelFactory dictionaryModelFactory,
            IWindowManager windowManager, IAccountDataService accountDataService, 
            IActiveAccountManager activeAccountManager)
            : base(
                toolBarManager, dictionaryActionItems, dictionaryService, eventAggregator, dictionaryModelFactory,
                windowManager, accountDataService, activeAccountManager)
        {
            DisplayName = "Справочник операторов";
            Initialize();
        }


        private void Initialize()
        {
           
        }

        #region Implementation of IHandle<AddNewDictionaryElement<Driver>>

        /// <summary>
        ///     Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(DictChanges<Phase> message)
        {

        }

        #endregion
    }
}