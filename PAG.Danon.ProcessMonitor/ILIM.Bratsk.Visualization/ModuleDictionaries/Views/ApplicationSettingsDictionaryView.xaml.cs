﻿using System;
using System.Windows;
using System.Windows.Controls;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Views
{
    /// <summary>
    /// Interaction logic for ApplicationSettingsDictionaryView.xaml
    /// </summary>
    public partial class ApplicationSettingsDictionaryView : UserControl
    {
        public ApplicationSettingsDictionaryView()
        {
            InitializeComponent();
        }

        private void RaiseEdit(object sender, object e)
        {
            try
            {
                ((ApplicationSettingsModel)((FrameworkElement)sender).DataContext).IsEdited = true;
            }
            catch (Exception ex)
            {
            }
        }
    }
}
