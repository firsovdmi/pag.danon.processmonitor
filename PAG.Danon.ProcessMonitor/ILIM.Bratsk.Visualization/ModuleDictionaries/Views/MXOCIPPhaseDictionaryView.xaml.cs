﻿using System;
using System.Windows;
using System.Windows.Controls;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Views
{
    /// <summary>
    /// Логика взаимодействия для PhaseDictionaryView.xaml
    /// </summary>
    public partial class MXOCIPPhaseDictionaryView : UserControl
    {
        public MXOCIPPhaseDictionaryView()
        {
            InitializeComponent();
        }
                private void RaiseEdit(object sender, object e)
        {
            try
            {
                ((MXOCIPPhaseModel) ((FrameworkElement) sender).DataContext).IsEdited = true;
            }
            catch (Exception ex)
            {
            }
        }
    }
}
