﻿using System;
using System.Windows;
using System.Windows.Controls;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.Views
{
    /// <summary>
    /// Логика взаимодействия для PhaseDictionaryView.xaml
    /// </summary>
    public partial class ReceptionCIPPhaseDictionaryView : UserControl
    {
        public ReceptionCIPPhaseDictionaryView()
        {
            InitializeComponent();
        }
                private void RaiseEdit(object sender, object e)
        {
            try
            {
                ((ReceptionCIPPhaseModel) ((FrameworkElement) sender).DataContext).IsEdited = true;
            }
            catch (Exception ex)
            {
            }
        }
    }
}
