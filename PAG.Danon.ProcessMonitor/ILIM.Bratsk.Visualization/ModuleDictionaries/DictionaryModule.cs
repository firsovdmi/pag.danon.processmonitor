using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.MenuAction;
using PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries.ToolBarAction;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleDictionaries
{
    public class DictionaryModule : IModule
    {
        public DictionaryModule(IMenuManager menuManager,
            IToolBarManager toolBarManager,
            IEnumerable<DictionaryMenuAction> actionItems,
            DictionaryActionItems dictionaryActionItems
            )
        {
            menuManager.ShowItem(new ActionItem("Справочники", null),2);
            foreach (var actionItem in actionItems)
                menuManager.WithParent("Справочники")
                    .ShowItem(actionItem);

            toolBarManager.ShowItem(dictionaryActionItems);
            dictionaryActionItems.Deactivate(false);
        }
    }
}