﻿using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;

namespace PAG.Danon.ProcessMonitor.Visualization.Extentions
{
    public interface IAccountDataGiver
    {
        IAccountDataService AccountDataService { get; }
    }
}