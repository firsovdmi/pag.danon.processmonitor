﻿namespace PAG.Danon.ProcessMonitor.Visualization.Infrastructure
{
    public class EmptType
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
