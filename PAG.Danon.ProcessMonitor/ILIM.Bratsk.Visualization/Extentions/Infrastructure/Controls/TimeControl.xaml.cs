﻿using System.Windows;
using System.Windows.Controls;

namespace PAG.Danon.ProcessMonitor.Visualization.Infrastructure.Controls
{
    /// <summary>
    /// Interaction logic for TimeControl.xaml
    /// </summary>
    public partial class TimeControl : UserControl
    {
        public TimeControl()
        {
            InitializeComponent();
        }

        public Orientation PanelOrientation
        {
            get { return (Orientation)GetValue(PanelOrientationProperty); }
            set
            {
                SetValue(PanelOrientationProperty, value);
                MainStackPanel.Orientation = value;
            }
        }

        // Using a DependencyProperty as the backing store for Property1.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PanelOrientationProperty =
             DependencyProperty.Register("PanelOrientation", typeof(Orientation),  typeof(TimeControl), new PropertyMetadata(Orientation.Vertical));


    }
}
