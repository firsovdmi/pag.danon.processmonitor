using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;

namespace PAG.Danon.ProcessMonitor.Visualization.Infrastructure
{
    public class AccountDataChanged
    {
        public AuthenticationResult AuthenticationResult { get; set; }
    }
}