using PAG.Danon.ProcessMonitor.Visualization.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.Infrastructure
{
    public interface ILoginViewModelFactory
    {
        LoginViewModel Create(LoginType loginType);
        void Release(LoginViewModel obj);
    }
}