﻿using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleManager.MenuAction;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleManager
{
    public class ManagerModule : IModule
    {
        public ManagerModule(IMenuManager menuManager,
            IToolBarManager toolBarManager,
            IEnumerable<ManagerMenuAction> actionItems,
            ManagerMenuAction administrationActionItems
            )
        {
            menuManager.ShowItem(new ActionItem("Управление", null), 0);
            foreach (var actionItem in actionItems)
                menuManager.WithParent("Управление")
                    .ShowItem(actionItem);
        }
    }
}