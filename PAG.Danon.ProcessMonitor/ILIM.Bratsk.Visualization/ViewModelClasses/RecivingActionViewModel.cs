﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Data.Model;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Visualization.ViewModelClasses
{
    public class RecivingActionViewModel : IRecivingAction
    {
        public int Id { get; set; }
        public ITank Tank { get; set; }
        public float Volume { get; set; }
        public int Section { get; set; }
        public IRecivingPost RecivePost { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Downtime { get; set; }
    }
}
