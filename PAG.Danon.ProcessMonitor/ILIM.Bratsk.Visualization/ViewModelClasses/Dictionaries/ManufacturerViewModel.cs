﻿using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Visualization.ViewModelClasses.Dictionaries
{
    public class ManufacturerViewModel : BaseDictionaryViewModel, IManufacturer
    {
    }
}
