﻿using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Visualization.ViewModelClasses.Dictionaries
{
    public class BaseDictionaryViewModel : IBaseDictionary
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }
}
