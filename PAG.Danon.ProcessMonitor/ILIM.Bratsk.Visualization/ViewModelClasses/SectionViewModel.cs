﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Data.Model;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Visualization.ViewModelClasses
{
    public class SectionViewModel : ISection, INotifyPropertyChanged
    {
        public SectionViewModel()
        {
            
        }

        public SectionViewModel(ISection source)
        {
            Id = source.Id;
            AcidTtn = source.AcidTtn;
            DensityTtn = source.DensityTtn;
            FatTtn = source.FatTtn;
            ProteinTtn = source.ProteinTtn;
            ThermalResistance = source.ThermalResistance;
            Farm = source.Farm;
            CarNumber = source.CarNumber;
            TtnVolume = source.TtnVolume;
            Sort = source.Sort;
            AcidLab = source.AcidLab;
            DensityLab = source.DensityLab;
            FatLab = source.FatLab;
            ProteinLab = source.ProteinLab;
            RecommendationRecivePost = source.RecommendationRecivePost;
            SectionN = source.SectionN;
            if (source.SubSections != null)
            {
                SubSections =
                    new ObservableCollection<SectionViewModel>(source.SubSections.Select(p => new SectionViewModel(p)));
            }
        }

        public int Id { get; set; }
        public float AcidTtn { get; set; }
        public float DensityTtn { get; set; }
        public float FatTtn { get; set; }
        public float ProteinTtn { get; set; }
        public IThermalResistance ThermalResistance { get; set; }
        public IFarm Farm { get; set; }
        public string CarNumber { get; set; }
        public float TtnVolume { get; set; }
        public ISort Sort { get; set; }
        public float AcidLab { get; set; }
        public float DensityLab { get; set; }
        public float FatLab { get; set; }
        public float ProteinLab { get; set; }
        public IRecivingPost RecommendationRecivePost { get; set; }
        public bool IsGroup
        {
            get { return SubSections != null && SubSections.Any(); }
        }
        private ISectionsName _sectionN;
        public ISectionsName SectionN
        {
            get { return _sectionN; }
            set
            {
                _sectionN = value;
                RaisePropertyChanged("DisplayName");
            }
        }

        private IEnumerable<ISection> _subSections;

        public IEnumerable<ISection> SubSections
        {
            get
            { return _subSections; }
            set
            {
                _subSections = value;
                RaisePropertyChanged("DisplayName");
            }
        }
        public bool Selected { get; set; }
        public bool Extanded { get; set; }
        public string DisplayName
        {
            get
            {
                string ret=null;
                if (SubSections == null || !SubSections.Any())
                {
                    if (SectionN!=null) ret ="Секция: "+ SectionN.Name;
                }
                else
                {
                    ret = "Секции: " + string.Join(", ", SubSections.Select(p => p.SectionN.Name).ToArray());
                }
                if (ret == null) return "Секция не определена";
                return ret;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
