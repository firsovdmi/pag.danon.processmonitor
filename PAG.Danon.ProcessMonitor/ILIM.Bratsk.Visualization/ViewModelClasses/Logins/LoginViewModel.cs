﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Data.Login;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Visualization.ViewModelClasses.Logins
{
    public class LoginViewModel
    {
        private LoginModel _model;

        public LoginViewModel()
        {
            _model = new LoginModel();
            _model.InitLogins();
            Staffs=new ObservableCollection<IStaff>(_model.Staffs);
            AcessLevels=new ObservableCollection<IAcessLevel>(_model.AcessLevels);
            LoginItems=new ObservableCollection<ILoginItem>(_model.Logins);
            SelectedItem = LoginItems.FirstOrDefault();
        }
        public ObservableCollection<ILoginItem> LoginItems { get; set; }
        public ILoginItem SelectedItem { get; set; }
        public ObservableCollection<IStaff> Staffs { get; set; }
        public ObservableCollection<IAcessLevel> AcessLevels { get; set; }

        #region AddAcessLevelCommand

        private RelayCommand _addAcessLevelCommand;

        public RelayCommand AddAcessLevelCommand
        {
            get
            {
                if (_addAcessLevelCommand == null)
                {
                    _addAcessLevelCommand = new RelayCommand(ExecuteAddAcessLevelCommand, CanExecuteAddAcessLevelCommand);
                }
                return _addAcessLevelCommand;
            }
        }

        private bool CanExecuteAddAcessLevelCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddAcessLevelCommand(object obj)
        {

        }

        #endregion

        #region DeleteAcessLevelCommand

        private RelayCommand _deleteThermalResistanceCommand;

        public RelayCommand DeleteThermalResistanceCommand
        {
            get
            {
                if (_deleteThermalResistanceCommand == null)
                {
                    _deleteThermalResistanceCommand = new RelayCommand(ExecuteDeleteThermalResistanceCommand, CanExecuteDeleteThermalResistanceCommand);
                }
                return _deleteThermalResistanceCommand;
            }
        }

        private bool CanExecuteDeleteThermalResistanceCommand(object obj)
        {
            return true;
        }

        private void ExecuteDeleteThermalResistanceCommand(object obj)
        {

        }

        #endregion

        #region SaveAcessLevelCommand

        private RelayCommand _saveThermalResistanceCommand;

        public RelayCommand SaveThermalResistanceCommand
        {
            get
            {
                if (_saveThermalResistanceCommand == null)
                {
                    _saveThermalResistanceCommand = new RelayCommand(ExecuteSaveThermalResistanceCommand, CanExecuteSaveThermalResistanceCommand);
                }
                return _saveThermalResistanceCommand;
            }
        }

        private bool CanExecuteSaveThermalResistanceCommand(object obj)
        {
            return true;
        }

        private void ExecuteSaveThermalResistanceCommand(object obj)
        {

        }

        #endregion
    }
}
