﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using PAG.WBD.Milk.Data.Model;
using PAG.WBD.Milk.Data.Model.Dictionaries;
using PAG.WBD.Milk.Visualization.Login;
using PAG.WBD.Milk.Visualization.ViewModelClasses.Logins;

namespace PAG.WBD.Milk.Visualization.ViewModelClasses
{
    public class ViewModel : INotifyPropertyChanged
    {
        private Data.Model.Model _model = new Data.Model.Model();
        private Timer _timer;
        public string CurrentTime { get; set; }

        public ViewModel()
        {
            _timer = new Timer( Callback, null, 1000, 1000);

            RefreshTtns();
        }

        private void Callback(Object state)
        {
            CurrentTime = DateTime.Now.ToString("hh:mm:ss");
            RaisePropertyChanged("CurrentTime");
           // _timer.Change(1000, Timeout.Infinite);
        }

        public ISection _selectedSection;
        public ISection SelectedSection
        {
            get { return _selectedSection; }
            set
            {
                _selectedSection = value;
                RaisePropertyChanged("SelectedSection");
            }
        }

        ObservableCollection<SectionViewModel> _sections = new ObservableCollection<SectionViewModel>();
        public ObservableCollection<SectionViewModel> Sections { get { return _sections; } set { _sections = value; RaisePropertyChanged("Sections"); } }

        private ObservableCollection<ITtnHead> _ttns;

        public ObservableCollection<ITtnHead> Ttns
        {
            get
            {
                if (_ttns == null) _ttns = new ObservableCollection<ITtnHead>(_model);
                return _ttns;
            }
        }

        private ObservableCollection<IFioDriver> _fioDrivers;
        public ObservableCollection<IFioDriver> FioDrivers
        {
            get
            {
                if (_fioDrivers == null) _fioDrivers = new ObservableCollection<IFioDriver>(_model.FioDrivers);
                return _fioDrivers;
            }
        }

        private ObservableCollection<IManufacturer> _manufacturers;
        public ObservableCollection<IManufacturer> Manufacturers
        {
            get
            {
                return _manufacturers ??
                       (_manufacturers = new ObservableCollection<IManufacturer>(_model.Manufacturers));
            }
        }

        private ObservableCollection<ISectionsName> _sectionNames;
        public ObservableCollection<ISectionsName> SectionNames
        {
            get { return _sectionNames ?? (_sectionNames = new ObservableCollection<ISectionsName>(_model.SectionsNames)); }
        }

        private ObservableCollection<IRecivingPost> _recivePosts;
        public ObservableCollection<IRecivingPost> RecivePosts
        {
            get { return _recivePosts ?? (_recivePosts = new ObservableCollection<IRecivingPost>(_model.RecivingPosts)); }
        }

        private ObservableCollection<IStaff> _staffs;
        public ObservableCollection<IStaff> Staffs
        {
            get { return _staffs ?? (_staffs = new ObservableCollection<IStaff>(_model.Staffs)); }
        }

        private ObservableCollection<IThermalResistance> _thermalResistances;
        public ObservableCollection<IThermalResistance> ThermalResistances
        {
            get { return _thermalResistances ?? (_thermalResistances = new ObservableCollection<IThermalResistance>(_model.ThermalResistance)); }
        }

        private ObservableCollection<IFarm> _farms;
        public ObservableCollection<IFarm> Farms
        {
            get { return _farms ?? (_farms = new ObservableCollection<IFarm>(_model.Farms)); }
        }

        private ObservableCollection<ISort> _sortes;
        public ObservableCollection<ISort> Sortes
        {
            get { return _sortes ?? (_sortes = new ObservableCollection<ISort>(_model.Sortes)); }
        }

        public IStaff Master { get; set; }
        public IStaff Operator { get; set; }

        private ObservableCollection<IContract> _contracts;

        public ObservableCollection<IContract> Contracts
        {
            get { return _contracts ?? (_contracts = new ObservableCollection<IContract>(_model.Contracts)); }
        }

        private ITtnHead _selectedItem;
        public ITtnHead SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                AddSectionsToTtn();
                _selectedItem = value;
                InitSectionsFromTtn();
                RaisePropertyChanged("SelectedItem");
            }
        }

        public void RefreshTtns()
        {
            _model.InitItems();
            SelectedItem = Ttns.FirstOrDefault();
        }

        private void AddSectionsToTtn()
        {
            if (_selectedItem == null) return;
            _selectedItem.Section = Sections;
        }

        private void InitSectionsFromTtn()
        {
            if (_selectedItem == null || _selectedItem.Section==null) return;
            Sections =new ObservableCollection<SectionViewModel>( _selectedItem.Section.Select(p=>new SectionViewModel(p)));
        }

        #region Commandos

        #region AddNewTtnCommand

        private RelayCommand _addNewTtnCommand;

        public RelayCommand AddNewTtnCommand
        {
            get
            {
                return _addNewTtnCommand ??
                       (_addNewTtnCommand = new RelayCommand(ExecuteAddNewTtnCommand, CanExecuteAddNewTtnCommand));
            }
        }

        private bool CanExecuteAddNewTtnCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddNewTtnCommand(object obj)
        {
            SelectedItem = new TtnHeadViewModel();
            Ttns.Add(SelectedItem);
        }

        #endregion

        #region AddSectionCommand

        private RelayCommand _addSectionCommand;

        public RelayCommand AddSectionCommand
        {
            get
            {
                if (_addSectionCommand == null)
                {
                    _addSectionCommand = new RelayCommand(ExecuteAddSectionCommand, CanExecuteAddSectionCommand);
                }
                return _addSectionCommand;
            }
        }

        private bool CanExecuteAddSectionCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddSectionCommand(object obj)
        {
            //var usedName =
            //    Sections.Where(p => p.SubSections != null)
            //        .SelectMany(p => p.SubSections)
            //        .Select(p => p.SectionN);
            //usedName = usedName.Concat(     Sections.Where(p => p.SubSections == null).Select(p => p.SectionN)          ).ToList();
            //var freeSections = usedName.Except(Sections).ToList();
            Sections.Add(new SectionViewModel());
        }

        #endregion

        #region DeleteSectionCommand

        private RelayCommand _deleteSectionCommand;

        public RelayCommand DeleteSectionCommand
        {
            get
            {
                if (_deleteSectionCommand == null)
                {
                    _deleteSectionCommand = new RelayCommand(ExecuteDeleteSectionCommand, CanExecuteDeleteSectionCommand);
                }
                return _deleteSectionCommand;
            }
        }

        private bool CanExecuteDeleteSectionCommand(object obj)
        {
            return true;
        }

        private void ExecuteDeleteSectionCommand(object obj)
        {
            foreach (var section in Sections.ToList())
            {
                if (section.Selected)
                {
                    Sections.Remove(section);
                }
            }
            foreach (var section in Sections.Where(p => p.SubSections != null).SelectMany(p => p.SubSections).Select(p => (SectionViewModel)p).ToList())
            {
                if (section.Selected)
                {
                    Sections.Remove(section);
                }
            }
        }

        #endregion

        #region GroupingSectionCommand

        private RelayCommand _groupingSectionCommand;

        public RelayCommand GroupingSectionCommand
        {
            get
            {
                if (_groupingSectionCommand == null)
                {
                    _groupingSectionCommand = new RelayCommand(ExecuteGroupingSectionCommand, CanExecuteGroupingSectionCommand);
                }
                return _groupingSectionCommand;
            }
        }

        private bool CanExecuteGroupingSectionCommand(object obj)
        {
            return true;
        }

        private void ExecuteGroupingSectionCommand(object obj)
        {
            if (Sections.Count(p => p.Selected) < 2) return;
            var subList = new ObservableCollection<SectionViewModel>();
            foreach (var section in Sections.Where(p => !p.IsGroup).ToList())
            {
                if (section.Selected)
                {
                    subList.Add(section);
                    Sections.Remove(section);
                }
            }
            Sections.Add(new SectionViewModel { SubSections = subList });
        }

        #endregion

        #region UnGroupingSectionCommand

        private RelayCommand _unGroupingSectionCommand;

        public RelayCommand UnGroupingSectionCommand
        {
            get
            {
                if (_unGroupingSectionCommand == null)
                {
                    _unGroupingSectionCommand = new RelayCommand(ExecuteUnGroupingSectionCommand, CanExecuteUnGroupingSectionCommand);
                }
                return _unGroupingSectionCommand;
            }
        }

        private bool CanExecuteUnGroupingSectionCommand(object obj)
        {
            return true;
        }

        private void ExecuteUnGroupingSectionCommand(object obj)
        {
            
            var subList = new ObservableCollection<SectionViewModel>();
            foreach (var section in Sections.Where(p => p.SubSections != null).SelectMany(p => p.SubSections).Select(p => (SectionViewModel)p).ToList())
            {
                Sections.Add(section);
            }
            foreach (var section in Sections.Where(p => p.IsGroup).ToList())
            {
                Sections.Remove(section);
            }
        }

        #endregion

        #region SaveSelectedItemCommand

        private RelayCommand _saveSelectedItemCommand;

        public RelayCommand SaveSelectedItemCommand
        {
            get
            {
                if (_saveSelectedItemCommand == null)
                {
                    _saveSelectedItemCommand = new RelayCommand(ExecuteSaveSelectedItemCommand, CanExecuteSaveSelectedItemCommand);
                }
                return _saveSelectedItemCommand;
            }
        }

        private bool CanExecuteSaveSelectedItemCommand(object obj)
        {
            return true;
        }

        private void ExecuteSaveSelectedItemCommand(object obj)
        {
            AddSectionsToTtn();
            _model.SaveTtn(SelectedItem);
        }

        #endregion

        #region AddFioDriverCommand

        private RelayCommand _addFioDriverCommand;

        public RelayCommand AddFioDriverCommand
        {
            get
            {
                if (_addFioDriverCommand == null)
                {
                    _addFioDriverCommand = new RelayCommand(ExecuteAddFioDriverCommand, CanExecuteAddFioDriverCommand);
                }
                return _addFioDriverCommand;
            }
        }

        private bool CanExecuteAddFioDriverCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddFioDriverCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newFioDriver = new FioDriver { Name = inputDialog.Value };
            FioDrivers.Add(newFioDriver);
            _model.AddFioDrivers(newFioDriver);
        }

        #endregion

        #region AddStaffCommand

        private RelayCommand _addStaffCommand;

        public RelayCommand AddStaffCommand
        {
            get
            {
                if (_addStaffCommand == null)
                {
                    _addStaffCommand = new RelayCommand(ExecuteAddStaffCommand, CanExecuteAddStaffCommand);
                }
                return _addStaffCommand;
            }
        }

        private bool CanExecuteAddStaffCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddStaffCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newStaff = new Staff { Name = inputDialog.Value };
            Staffs.Add(newStaff);
            _model.AddStaffs(newStaff);
        }

        #endregion

        #region AddManufacturerCommand

        private RelayCommand _addManufacturerCommand;

        public RelayCommand AddManufacturerCommand
        {
            get
            {
                if (_addManufacturerCommand == null)
                {
                    _addManufacturerCommand = new RelayCommand(ExecuteAddManufacturerCommand, CanExecuteAddManufacturerCommand);
                }
                return _addManufacturerCommand;
            }
        }

        private bool CanExecuteAddManufacturerCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddManufacturerCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newManufacturer = new Manufacturer { Name = inputDialog.Value };
            Manufacturers.Add(newManufacturer);
            _model.AddManufacturers(newManufacturer);
        }

        #endregion

        #region AddContractCommand

        private RelayCommand _addContractCommand;

        public RelayCommand AddContractCommand
        {
            get
            {
                if (_addContractCommand == null)
                {
                    _addContractCommand = new RelayCommand(ExecuteAddContractCommand, CanExecuteAddContractCommand);
                }
                return _addContractCommand;
            }
        }

        private bool CanExecuteAddContractCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddContractCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newContract = new Contract { Name = inputDialog.Value };
            Contracts.Add(newContract);
            _model.AddContracts(newContract);
        }

        #endregion

        #region AddSectionNameCommand

        private RelayCommand _addSectionNameCommand;

        public RelayCommand AddSectionNameCommand
        {
            get
            {
                if (_addSectionNameCommand == null)
                {
                    _addSectionNameCommand = new RelayCommand(ExecuteAddSectionNameCommand, CanExecuteAddSectionNameCommand);
                }
                return _addSectionNameCommand;
            }
        }

        private bool CanExecuteAddSectionNameCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddSectionNameCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newSectionName = new SectionsName { Name = inputDialog.Value };
            SectionNames.Add(newSectionName);
            _model.AddSectionsName(newSectionName);
        }

        #endregion

        #region AddRecivePostsCommand

        private RelayCommand _addRecivePostsCommand;

        public RelayCommand AddRecivePostsCommand
        {
            get
            {
                if (_addRecivePostsCommand == null)
                {
                    _addRecivePostsCommand = new RelayCommand(ExecuteAddRecivePostsCommand, CanExecuteAddRecivePostsCommand);
                }
                return _addRecivePostsCommand;
            }
        }

        private bool CanExecuteAddRecivePostsCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddRecivePostsCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newRecivePosts = new RecivingPost() { Name = inputDialog.Value };
            RecivePosts.Add(newRecivePosts);
            _model.AddRecivePost(newRecivePosts);
        }

        #endregion

        #region AddThermalResistanceCommand

        private RelayCommand _addThermalResistanceCommand;

        public RelayCommand AddThermalResistanceCommand
        {
            get
            {
                if (_addThermalResistanceCommand == null)
                {
                    _addThermalResistanceCommand = new RelayCommand(ExecuteAddThermalResistanceCommand, CanExecuteAddThermalResistanceCommand);
                }
                return _addThermalResistanceCommand;
            }
        }

        private bool CanExecuteAddThermalResistanceCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddThermalResistanceCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newThermalResistance = new ThermalResistance() { Name = inputDialog.Value };
            ThermalResistances.Add(newThermalResistance);
            _model.AddThermalResistance(newThermalResistance);
        }

        #endregion

        #region DictionaryFarmEditCommand

        private RelayCommand _dictionaryFarmEditCommand;

        public RelayCommand DictionaryFarmEditCommand
        {
            get
            {
                if (_dictionaryFarmEditCommand == null)
                {
                    _dictionaryFarmEditCommand = new RelayCommand(ExecuteDictionaryFarmEditCommand, CanExecuteDictionaryFarmEditCommand);
                }
                return _dictionaryFarmEditCommand;
            }
        }

        private bool CanExecuteDictionaryFarmEditCommand(object obj)
        {
            return true;
        }

        private void ExecuteDictionaryFarmEditCommand(object obj)
        {
            //var vmd = App.Container.Resolve<FarmDictionaryViewModel>();
            //var vmd = new DictionaryItemViewModel(_model.Farms);
            //vmd.SaveMethod += _model.UpdateDictionaryFarms;
            //new DictionaryEdit{DataContext=vmd }.ShowDialog();
        }

        #endregion

        #region AddFarmCommand

        private RelayCommand _addFarmCommand;

        public RelayCommand AddFarmCommand
        {
            get
            {
                if (_addFarmCommand == null)
                {
                    _addFarmCommand = new RelayCommand(ExecuteAddFarmCommand, CanExecuteAddFarmCommand);
                }
                return _addFarmCommand;
            }
        }

        private bool CanExecuteAddFarmCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddFarmCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newFarm = new Farm() { Name = inputDialog.Value };
            Farms.Add(newFarm);
            _model.AddFarm(newFarm);
        }

        #endregion

        #region AddSortCommand

        private RelayCommand _addSortCommand;

        public RelayCommand AddSortCommand
        {
            get
            {
                if (_addSortCommand == null)
                {
                    _addSortCommand = new RelayCommand(ExecuteAddSortCommand, CanExecuteAddSortCommand);
                }
                return _addSortCommand;
            }
        }

        private bool CanExecuteAddSortCommand(object obj)
        {
            return true;
        }

        private void ExecuteAddSortCommand(object obj)
        {
            var inputDialog = new TextInputDialog() { Lable = "ФИО:", WindowTitle = "Добавление фамилии" };
            inputDialog.ShowDialog();
            if (inputDialog.IsCancel) return;
            var newSort = new Sort() { Name = inputDialog.Value };
            Sortes.Add(newSort);
            _model.AddSort(newSort);
        }

        #endregion

        #region ConfigLoginsCommand

        private RelayCommand _configLoginsCommand;

        public RelayCommand ConfigLoginsCommand
        {
            get
            {
                if (_configLoginsCommand == null)
                {
                    _configLoginsCommand = new RelayCommand(ExecuteConfigLoginsCommand, CanExecuteConfigLoginsCommand);
                }
                return _configLoginsCommand;
            }
        }

        private bool CanExecuteConfigLoginsCommand(object obj)
        {
            return true;
        }

        private void ExecuteConfigLoginsCommand(object obj)
        {
            new LoginConfigurator{ DataContext=new LoginViewModel()}.ShowDialog();
        }

        #endregion

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }


    }
}
