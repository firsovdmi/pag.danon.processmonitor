﻿using System;
using System.Globalization;
using System.Windows;

namespace PAG.Danon.ProcessMonitor.Visualization.Converters
{
    public class NullToCollapseConverter : ConvertorBase<NullToCollapseConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ( value==null) return Visibility.Collapsed;
            return Visibility.Visible;
        }

    }
}