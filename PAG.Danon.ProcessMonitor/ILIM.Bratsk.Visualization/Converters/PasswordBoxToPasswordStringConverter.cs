using System;
using System.Globalization;
using System.Windows.Controls;

namespace PAG.Danon.ProcessMonitor.Visualization.Converters
{
    internal class PasswordBoxToPasswordStringConverter : ConvertorBase<PasswordBoxToPasswordStringConverter>
    {
        #region ����� IValueConverter

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var passwordBox = value as PasswordBox;
            if (passwordBox != null)
            {
                return new Func<string>(() => { return passwordBox.Password; });
            }

            return new Func<string>(() => { return string.Empty; });
        }

        #endregion
    }
}