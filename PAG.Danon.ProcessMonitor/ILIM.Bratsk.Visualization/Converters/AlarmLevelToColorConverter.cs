﻿using System;
using System.Globalization;
using System.Windows.Media;

namespace PAG.Danon.ProcessMonitor.Visualization.Converters
{
    public class AlarmLevelToColorConverter : ConvertorBase<AlarmLevelToColorConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return Colors.LightGray;
            if ( (int)value==1) return Colors.Red;
            if ((int)value == 1) return Colors.Yellow;
            return Colors.Violet;
        }

    }
}