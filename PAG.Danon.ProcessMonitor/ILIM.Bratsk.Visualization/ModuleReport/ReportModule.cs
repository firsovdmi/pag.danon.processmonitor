﻿using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.MenuAction;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ToolBarAction;
using Stimulsoft.Report;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.Design.Toolbars;
using Stimulsoft.Report.Render;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport
{
    public class ReportModule : IModule
    {

        public ReportModule(IMenuManager menuManager,
            IToolBarManager toolBarManager,
            IEnumerable<ReportMenuAction> actionItems,
            ReportActionItems reportActionItems
            )
        {
            menuManager.ShowItem(new ActionItem("Отчеты", null));
            foreach (var actionItem in actionItems)
            {
                foreach (var actionItem2 in actionItem.Items)
                    menuManager.WithParent("Отчеты").WithParent(actionItem.Name)
                        .ShowItem(actionItem2);
            }


            toolBarManager.ShowItem(reportActionItems);
            reportActionItems.Deactivate(false);


            StiConfig.Load();
            var service = StiConfig.Services.GetService(typeof(StiMainMenuService)) as StiMainMenuService;
            if (service != null) service.ShowFileReportSave = false;
            var service1 = StiConfig.Services.GetServices(typeof(StiToolbarService)).ToArray().FirstOrDefault(p => p is StiStandardToolbarService) as StiStandardToolbarService;
            if (service1 != null) service1.ShowReportSave = false;
            StiDesigner.DontAskSaveReport = true;



            //Get service
            var config = StiConfig.Services.GetService(typeof(StiPreviewConfigService)) as StiPreviewConfigService;

            //Turn off all buttons of changes of the rendered report
            config.PageNewEnabled = false;
            config.PageDeleteEnabled = false;
            config.PageDesignEnabled = false;
            config.PageSizeEnabled = false;
            config.ToolEditorActive = false;

            //Save configuration if necessary
            StiConfig.Save();

        }

    }
}
