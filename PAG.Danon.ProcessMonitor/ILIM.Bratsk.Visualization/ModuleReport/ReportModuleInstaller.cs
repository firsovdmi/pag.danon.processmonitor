﻿using System.Collections.Generic;
using System.Linq;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel;
using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.MenuAction;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ToolBarAction;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport
{
    public class ReportModuleInstaller : IWindsorInstaller
    {
        #region Implementation of IWindsorInstaller

        /// <summary>
        /// Performs the installation in the <see cref="T:Castle.Windsor.IWindsorContainer"/>.
        /// </summary>
        /// <param name="container">The container.</param><param name="store">The configuration store.</param>
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Types.FromThisAssembly().BasedOn(typeof(ReportViewModelBase<>)).If(p => p != typeof(ReportViewModelBase<>)).LifestyleCustom<RealisableSingletoneReportLifestyleManager>());
            container.Register(Types.FromThisAssembly().BasedOn(typeof(ReportViewModelBase<>)).If(p => p != typeof(ReportViewModelBase<>) && !p.IsAbstract).LifestyleCustom<RealisableSingletoneReportLifestyleManager>());
            container.Register(Component.For<IModule>().ImplementedBy<ReportModule>());
            container.Register(Types.FromThisAssembly().BasedOn(typeof(ReportMenuAction<>)).If(p => p != typeof(ReportMenuAction<>) && !p.IsAbstract).WithServices(typeof(ReportMenuAction)));
            container.Register(Types.FromThisAssembly().BasedOn(typeof(ReportActionItem)).If(p => p != typeof(ReportActionItem)).WithServices(typeof(ReportActionItem)));
            container.Register(Component.For<ReportActionItems>().ImplementedBy<ReportActionItems>());
            container.Register(Component.For<IReportViewModelFactory>().AsFactory().LifestyleTransient());



            //container.Register(Component.For<IPhaseRepository>().ImplementedBy<PhaseRepository>());
            //IPhaseRepository phaseRepository = container.Resolve<IPhaseRepository>();



            container.Register(
                Component.For<ReportViewModelToReportTypeCorrespondence>()
                    .ImplementedBy<ReportViewModelToReportTypeCorrespondence>());
        }

        #endregion
    }

    public class RealisableSingletoneReportLifestyleManager : AbstractLifestyleManager
    {
        private object _instances = null;
        Dictionary<object, object> _dict = new Dictionary<object, object>();
        public override object Resolve(CreationContext context, IReleasePolicy releasePolicy)
        {
            if (context.AdditionalArguments.Contains("id"))
            {
                var id = context.AdditionalArguments["id"];
                if (!_dict.ContainsKey(id))
                    _dict.Add(id, base.Resolve(context, releasePolicy));
                return _dict[id];
            }

            return base.Resolve(context, releasePolicy);
        }


        public override void Dispose()
        {

        }

        public override bool Release(object instance)
        {
            if (_dict.ContainsValue(instance))
            {
                _dict.Remove(_dict.FirstOrDefault(p => p.Value == instance).Key);
                return true;
            }
            return false;
        }
    }
}
