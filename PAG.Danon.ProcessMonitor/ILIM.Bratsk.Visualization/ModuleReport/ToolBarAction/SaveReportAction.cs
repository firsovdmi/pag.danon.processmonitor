using System;
using Caliburn.Micro;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ToolBarAction
{
    public class SaveReportAction : ReportActionItem
    {
        private readonly Func<bool> canExecuteInToolBar;

        public SaveReportAction()
            : base("��������� �����")
        {
            this.canExecuteInToolBar = canExecuteInToolBar ?? (() => IsActive);
        }
        /// <summary>
        /// The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {

        }

        private void Release(object sender, DeactivationEventArgs e)
        {

        }

 
    }
}