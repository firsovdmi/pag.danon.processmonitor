using System.Collections.Generic;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ToolBarAction
{
    public class ReportActionItems : ActionItem
    {
        protected ReportActionItems(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        /// Initializes a new instance of ActionItem class.
        /// </summary>
        public ReportActionItems(IEnumerable<ReportActionItem> items)
            : base("������")
        {
            foreach (var item in items)
                Items.Add(item);
        }
    }
}