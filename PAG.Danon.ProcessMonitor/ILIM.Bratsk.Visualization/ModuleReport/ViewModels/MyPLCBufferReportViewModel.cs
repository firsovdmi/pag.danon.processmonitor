using System;
using System.Linq;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Views;
using Stimulsoft.Report;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels
{


    [View(typeof(ProcessReportParametersView), Context = "ReportParameters")]
    public class MyPLCBufferReportViewModel : ReportViewModelBase<ProcessRequestParameters>
    {
        private readonly IProcessRequest _processReportRequest;

        public MyPLCBufferReportViewModel(IEventAggregator eventAggregator,
            IWindowManager windowManager, ReportViewModelToReportTypeCorrespondence reportViewModelToReportTypeCorrespondence,
            IReportService reportService,
            Guid id,
            IProcessRequest processReportRequest


        )
            : base(eventAggregator, windowManager, reportViewModelToReportTypeCorrespondence, reportService, id)
        {
            _processReportRequest = processReportRequest;
        }

        #region Overrides of ReportViewModelBase<MilkReceiveRequestParameters>

        protected override void InitVariables(StiReport report, ProcessRequestParameters requestParameters, object reportData)
        {
            
        }

        protected override void Initialize()
        {
        }

        protected override object GetReportData()
        {
            var ret= _processReportRequest.Request(RequestParameters);
            return ret;
        }

        #endregion
        
    }
}