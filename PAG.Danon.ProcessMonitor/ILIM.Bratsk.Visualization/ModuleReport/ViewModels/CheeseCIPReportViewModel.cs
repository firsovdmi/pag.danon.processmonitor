using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Views;
using Stimulsoft.Report;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels
{


    [View(typeof(CheeseCIPReportParametersView), Context = "ReportParameters")]
    public class CheeseCIPReportViewModel : ReportViewModelBase<CheeseCIPRequestParameters>
    {
        private readonly ICheeseCIPRequest _CIPReportRequest;
        private readonly ICheeseCIPPhaseService _phaseRepository;
        private readonly ICipConturService _cipContur;

        public CheeseCIPReportViewModel(IEventAggregator eventAggregator,
            IWindowManager windowManager, ReportViewModelToReportTypeCorrespondence reportViewModelToReportTypeCorrespondence,
            IReportService reportService,
            Guid id,
            ICheeseCIPRequest cipReportRequest,
            ICheeseCIPPhaseService phaseRepository,
            ICipConturService cipContur

        )
            : base(eventAggregator, windowManager, reportViewModelToReportTypeCorrespondence, reportService, id)
        {
            _CIPReportRequest = cipReportRequest;
            _phaseRepository = phaseRepository;
           _cipContur = cipContur;

         _conturList = _cipContur.Get().OrderBy(e=>e.PlcN).Select(e => new ShortEntity() { ID = e.ID, Name = e.Name }).ToList();

            _seList = _phaseRepository
                 .Get()
                 .Select(e => new ShortEntity() { ID = e.ID, Name = e.Name })
                 .OrderBy(e => e.Name)
                 .ToList();
            }



        private List<ShortEntity> _conturList;

        public List<ShortEntity> ConturList
        {
            get
            {
                if (_conturList == null)
                {
                    _conturList = _cipContur.Get().Select(e => new ShortEntity() { ID = e.ID, Name = e.Name }).ToList();
                }
                return _conturList;
            }




        }



        private List<ShortEntity> _seList;

        public List<ShortEntity> seList
            {
            get
                {
                if (_seList == null)
                    {
                    _seList = _phaseRepository
                                .Get()
                                .Where(e => e.Type == PhaseType.Cip)
                                .Select(e => new ShortEntity() { ID = e.ID, Name = e.Name })
                                .ToList();
                    }
                return _seList;
                }
            }



        #region Overrides of ReportViewModelBase<MilkReceiveRequestParameters>

        protected override void InitVariables(StiReport report, CheeseCIPRequestParameters requestParameters, object reportData)
        {
            
        }

       
        protected override void Initialize()
        {
        }

        protected override object GetReportData()
        {

          //  RequestParameters.Conturs = _conturList.Where(e => e.IsSelected == true).ToList();
            RequestParameters.Phases = _seList.Where(e => e.IsSelected == true).ToList();
            var ret = _CIPReportRequest.Request(RequestParameters);
            var a = _phaseRepository
                .Get();
            return ret;
        }

        #endregion
        
    }
}