using System;
using System.Linq;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Views;
using Stimulsoft.Report;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using System.Collections.Generic;
using System.Text;
using Castle.MicroKernel.ModelBuilder.Descriptors;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels
{


    [View(typeof(CheeseReportParametersView), Context = "ReportParameters")]
    public class CheeseReportViewModel : ReportViewModelBase<CheeseRequestParameters>
    {
        private readonly ICheeseRequest _processReportRequest;
        private readonly ICheesePhaseService _phaseRepository;

        public CheeseReportViewModel(IEventAggregator eventAggregator,
            IWindowManager windowManager, ReportViewModelToReportTypeCorrespondence reportViewModelToReportTypeCorrespondence,
            IReportService reportService,
            Guid id,
            ICheeseRequest processReportRequest,
            ICheesePhaseService phaseRepository


        )
            : base(eventAggregator, windowManager, reportViewModelToReportTypeCorrespondence, reportService, id)
        {
            _processReportRequest = processReportRequest;
            _phaseRepository = phaseRepository;
            _seList = _phaseRepository
                .Get()
                .Where(e => e.Class == PhaseClass.prod)
                .Select(e => new ShortEntity() {ID = e.ID, Name = e.Description})
                .OrderBy(e=>e.Name)
                .ToList();

            //_xferIn = _phaseRepository
            //    .Get()
            //    .Where(e => e.Class == PhaseClass.xferIn)
            //    .Select(e => new ShortEntity() {ID = e.ID, Name = e.Description})
            //    .OrderBy(e => e.Name)
            //    .ToList();

            _xferIn = _phaseRepository
                .Get()
                .Where(e => e.Class == PhaseClass.xferIn)
                .Select(e=> new StringBuilder(e.Name).ToString().Split('L').FirstOrDefault())
                .Distinct()
                .Select(e=> new ShortEntity()
                {
                    Name = e
                  
                   

                }).ToList();



        }


        private List<ShortEntity> _seList ;

        public List<ShortEntity> seList
        {
            get
            {
                if (_seList == null) { _seList = _phaseRepository
                .Get()
                .Where(e => e.Class == PhaseClass.prod)
                .Select(e => new ShortEntity() { ID = e.ID, Name = e.Description })
                .ToList();
                } return _seList;
            }
        }

        private List<ShortEntity> _xferIn;

        public List<ShortEntity> XferIn
        {
            get
            {
                return _xferIn = _phaseRepository
                .Get()
                .Where(e => e.Class == PhaseClass.xferIn)
                .Select(e => new StringBuilder(e.Name).ToString().Split('L').FirstOrDefault())
                .Distinct()
                .Select(e => new ShortEntity()
                {
                    Name = e
                }).ToList();
            }
        }


        #region Overrides of ReportViewModelBase<MilkReceiveRequestParameters>



        protected override void InitVariables(StiReport report,CheeseRequestParameters requestParameters, object reportData)
        {

        }

        protected override void Initialize()
        {
        }

        protected override object GetReportData()
        {

            RequestParameters.Phases = _seList.Where(e=>e.IsSelected ==true).ToList();
            RequestParameters.XferIn = _xferIn
                .Where(e => e.IsSelected == true)
                .Select(q=>q.Name)
                .ToList();
            var ret = _processReportRequest.Request(RequestParameters);
            return ret;
        }

        #endregion

    }
}