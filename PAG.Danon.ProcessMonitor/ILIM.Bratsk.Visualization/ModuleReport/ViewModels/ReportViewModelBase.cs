using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Views;
using Stimulsoft.Report;
using Stimulsoft.Report.Design;
using Stimulsoft.Report.Dictionary;
using Action = System.Action;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels
{

    [View(typeof(ReportToolBarView), Context = "ToolBar")]
    [View(typeof(ReportView), Context = "MainView")]
    public abstract class ReportViewModelBase<T> : SelectableScreen, IEditStatus, IReportViewModel where T : INotifyPropertyChanged
    {
        protected readonly IEventAggregator EventAggregator;
        protected readonly IWindowManager WindowManager;
        private readonly ReportViewModelToReportTypeCorrespondence _reportViewModelToReportTypeCorrespondence;
        protected readonly IReportService ReportService;
        private readonly Guid _id;
        private bool _isEdited;
        private bool _isDeleted;
        private bool _isAdded;
        private Report _item;
        private RelayCommand _saveCommand;
        private RelayCommand _deleteCommand;
        private RelayCommand _refreshReport;
        private RelayCommand _editTemplateCommand;
        private RelayCommand _printCommand;
        private RelayCommand _exportPdfCommand;
        private RelayCommand _exportXlsCommand;
        private RelayCommand _exportWordCommand;
        private StiReport _reportTemplate;
        private StiReport _reportFromAssembly;
        private bool _isReportTemplateChanged;


        public T RequestParameters { get; set; }

        public ReportViewModelBase(IEventAggregator eventAggregator, IWindowManager windowManager,
            ReportViewModelToReportTypeCorrespondence reportViewModelToReportTypeCorrespondence,
            IReportService reportService, Guid id)
        {
            EventAggregator = eventAggregator;
            WindowManager = windowManager;
            _reportViewModelToReportTypeCorrespondence = reportViewModelToReportTypeCorrespondence;
            ReportService = reportService;
            _id = id;
            
        }

       



        public Report Item
        {
            get { return _item; }
            set
            {
                if (Equals(value, _item)) return;
                _item = value;
                NotifyOfPropertyChange("Item");
            }
        }

        public StiReport ReportTemplate
        {
            get
            {
                if (_reportTemplate == null)
                    TryGetReportFromTemplate();
                return _reportTemplate;
            }
            set
            {
                if (Equals(value, _reportTemplate)) return;
                _reportTemplate = value;
                NotifyOfPropertyChange(() => ReportTemplate);
            }
        }

        public StiReport ReportFromAssembly
        {
            get
            {
                if (_reportFromAssembly == null)
                    TryGetReportFromAssembly();
                return _reportFromAssembly;
            }
            set
            {
                if (Equals(value, _reportFromAssembly)) return;
                _reportFromAssembly = value;
                NotifyOfPropertyChange("ReportFromAssembly");
            }
        }

        private void TryGetReportFromAssembly()
        {
            try
            {
                _reportFromAssembly = StiReport.GetReportFromAssembly(Item.ReportAssembly);
            }
            catch
            {
            }
        }
        private void TryGetReportFromTemplate()
        {
            try
            {
                _reportTemplate = new StiReport();
                _reportTemplate.Load(Item.ReportTemplate);
            }
            catch
            {
            }
        }
        public event EventHandler<StiReportEventArgs> Refreashed;

        protected virtual void OnRefreashed()
        {
            EventHandler<StiReportEventArgs> handler = Refreashed;
            StiOptions.Viewer.Windows.ShowPrintButton = false;
            StiOptions.Viewer.Windows.ShowSaveButton = false;
            StiOptions.Viewer.Windows.ShowBookmarksPanel = false;
            StiOptions.Viewer.Windows.ShowEditorTool = false;
            StiOptions.Viewer.Windows.ShowThumbsPanel = false;
            StiOptions.Viewer.Windows.ShowThumbsButton = false;
            StiOptions.Viewer.Windows.ShowToolbar = false;

            if (handler != null) handler(this, new StiReportEventArgs(ReportFromAssembly));
        }

        protected void SetDecimalParameterAtReport(StiReport report, string name, object value, string description = "")
        {
            SetParameterAtReport(report, name, value, typeof(decimal), description);
        }

        protected void SetStringParameterAtReport(StiReport report, string name, object value, string description = "")
        {
            SetParameterAtReport(report, name, value, typeof(string), description);
        }

        protected void SetDateTimeParameterAtReport(StiReport report, string name, object value, string description = "")
        {
            SetParameterAtReport(report, name, value, typeof(DateTime), description);
        }

        private void SetParameterAtReport(StiReport report, string name, object value, Type type, string description = "")
        {
            if (name == "TimeFrom") report.Dictionary.Variables.Remove("TimeFrom"); if (name == "TimeLess") report.Dictionary.Variables.Remove("TimeLess");
            if (!report.Dictionary.Variables.Contains(name)) report.Dictionary.Variables.Add(new StiVariable("Vars", name, description, description, type, "", false));
            report[name] = value;
        }

        #region Commands

        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(ExecuteSaveCommand, CanExecuteSaveCommand, CommandType.Save, "���������", "��������� ����� � ������");
                }
                return _saveCommand;
            }
        }

        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(ExecuteDeleteCommand, CanExecuteDeleteCommand, CommandType.Delete, "�������", "�������");
                }
                return _deleteCommand;
            }
        }

        public RelayCommand RefreshReportCommand
        {
            get
            {
                if (_refreshReport == null)
                {
                    _refreshReport = new RelayCommand(ExecuteRefreashReportCommamd, CanExecuteRefreashReportCommand, CommandType.Refresh, "��������", "��������");
                }
                return _refreshReport;
            }
        }

        public RelayCommand EditTemplateCommand
        {
            get
            {
                if (_editTemplateCommand == null)
                {
                    _editTemplateCommand = new RelayCommand(ExecuteeditTemplateCommand, CanExecuteeditTemplateCommand, CommandType.Edit, "�������������", "������������� ������");
                }
                return _editTemplateCommand;
            }
        }

        public RelayCommand PrintCommand
        {
            get
            {
                if (_printCommand == null)
                {
                    _printCommand = new RelayCommand(ExecutePrintCommand, CanExecuteeditActionOverReportFromAssemblyCommand, CommandType.Print, "�����������", "�����������");
                }
                return _printCommand;
            }
        }

        private bool CanExecuteeditActionOverReportFromAssemblyCommand(object obj)
        {
            return ReportFromAssembly != null;
        }

        private void ExecutePrintCommand(object obj)
        {
            ReportFromAssembly.Print();
        }

        public RelayCommand ExportPdfCommand
        {
            get
            {
                if (_exportPdfCommand == null)
                {
                    _exportPdfCommand = new RelayCommand(ExecuteExportPdfCommand, CanExecuteeditActionOverReportFromAssemblyCommand, CommandType.ExportPdf, "��������� � Pdf", "��������� � Pdf");
                }
                return _exportPdfCommand;
            }
        }

        private void ExecuteExportPdfCommand(object obj)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog()
            {
                DefaultExt = ".Pdf",
                Filter = "����� Pdf (*.Pdf)|*.Pdf"
            };
            if (dlg.ShowDialog() == true)
            {
                ReportFromAssembly.ExportDocument(StiExportFormat.Pdf, dlg.FileName);
            }
        }

        public RelayCommand ExportXlsCommand
        {
            get
            {
                if (_exportXlsCommand == null)
                {
                    _exportXlsCommand = new RelayCommand(ExecuteExportXlsCommand, CanExecuteeditActionOverReportFromAssemblyCommand, CommandType.ExportXls, "��������� � Xls", "��������� � Xls");
                }
                return _exportXlsCommand;
            }
        }

        private void ExecuteExportXlsCommand(object obj)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog()
            {
                DefaultExt = ".Xls",
                Filter = "����� Xls (*.Xls)|*.Xls"
            };
            if (dlg.ShowDialog() == true)
            {
                ReportFromAssembly.ExportDocument(StiExportFormat.Excel, dlg.FileName);
            }
        }

        public RelayCommand ExportWordCommand
        {
            get
            {
                if (_exportWordCommand == null)
                {
                    _exportWordCommand = new RelayCommand(ExecuteExportWordCommand, CanExecuteeditActionOverReportFromAssemblyCommand, CommandType.ExportWord, "��������� � Word", "��������� � Word");
                }
                return _exportWordCommand;
            }
        }

        private void ExecuteExportWordCommand(object obj)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog()
            {
                DefaultExt = ".doc",
                Filter = "����� Word(2007) (*.doc)|*.doc"
            };
            if (dlg.ShowDialog() == true)
            {
                ReportFromAssembly.ExportDocument(StiExportFormat.Word2007, dlg.FileName);
            }
        }

        #region EditTemplateCommand

        private bool CanExecuteeditTemplateCommand(object obj)
        {
            return true;
        }

        private void ExecuteeditTemplateCommand(object obj)
        {
            try
            {
                StiDesigner.SavingReport += StiDesigner_SavingReport;
                StiDesigner.ClosedDesigner += StiDesigner_ClosedDesigner;
                if (ReportTemplate == null)
                    ReportTemplate = new StiReport();

                //���������� ������ �� ������ � ��������� ��������

                var assemblies = ReportTemplate.ReferencedAssemblies.ToList();
                var domainAssemblyName = typeof(Report).Assembly.GetName().Name;
                if (!assemblies.Contains(domainAssemblyName)) assemblies.Add(domainAssemblyName);
                var entityAssemblyName = typeof(Entity).Assembly.GetName().Name;
                if (!assemblies.Contains(entityAssemblyName)) assemblies.Add(entityAssemblyName);
                ReportTemplate.ReferencedAssemblies = assemblies.ToArray();
                var reportData = GetReportData();
                InitVariables(ReportTemplate, RequestParameters, reportData);
                ReportTemplate.RegData("Data", reportData);
                ReportTemplate.Design(true);

            }
            finally
            {

                StiDesigner.SavingReport -= StiDesigner_SavingReport;
                StiDesigner.ClosedDesigner -= StiDesigner_ClosedDesigner;
            }

        }



        #endregion

        #region RefreshReportCommand

        private bool CanExecuteRefreashReportCommand(object obj)
        {
            return true;
        }

        protected virtual void ExecuteRefreashReportCommamd(object obj)
        {
            if (ReportFromAssembly == null)
                WindowManager.Alert("������", "����������� ������ ������");
            else
            {
                ReportFromAssembly.CacheAllData = true;
                var reportData = GetReportData();
                InitVariables(ReportFromAssembly, RequestParameters, reportData);
                ReportFromAssembly.RegData("Data", reportData);
                ReportFromAssembly.Render(true);
                OnRefreashed();
            }
        }

        #endregion

        protected abstract void InitVariables(StiReport report, T requestParameters, object reportData);

        #region DeleteCommand

        private bool CanExecuteDeleteCommand(object obj)
        {
            return true;
        }

        private void ExecuteDeleteCommand(object obj)
        {
            WindowManager.Confirm("�� �������?", "�� ������������� ������ ������� �����?", () =>
            {
                if (IsAdded)
                {
                    TryClose();
                }
                else
                {
                    ReportService.Delete(Item);
                    EventAggregator.PublishOnCurrentThread(new ReportDelited(Item));
                    TryClose();
                }

            });

        }

        #endregion

        #region SaveCommand

        private bool CanExecuteSaveCommand(object obj)
        {
            return (IsAdded || IsEdited || IsDeleted);
        }

        private void ExecuteSaveCommand(object obj)
        {

            using (var stringWriter = new StringWriter())
            {
                using (var textWriter = XmlWriter.Create(stringWriter))
                {
                    var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                    serializer.Serialize(textWriter, RequestParameters);
                }
                Item.UserReportRequestParameter.RequestParameters = stringWriter.ToString();
            }


            Item.UserReportRequestParameters = new List<UserReportRequestParameters>() { Item.UserReportRequestParameter };
            if (IsAdded)
            {
                Item = ReportService.Create(Item);
                EventAggregator.PublishOnCurrentThread(new ReportAdded(Item));
            }
            else if (IsEdited)
            {
                Item = ReportService.Update(Item);
                EventAggregator.PublishOnCurrentThread(new ReportEdited(Item));
            }
            IsAdded = IsDeleted = IsEdited = false;
        }

        #endregion

        #endregion

        #region Private

        protected abstract void Initialize();

        protected void RunAsync(Action act)
        {
            Task.Factory.StartNew(act);
        }

        private void StiDesigner_SavingReport(object sender, StiSavingObjectEventArgs e)
        {
            if (e.SaveAs)
            {
                e.Processed = false;
            }
            else
            {
                _isReportTemplateChanged = true;
            }


        }

        private void StiDesigner_ClosedDesigner(object sender, EventArgs e)
        {
            if (_isReportTemplateChanged)
            {
                OnReportTemplateChanges();
                _isReportTemplateChanged = false;
            }
        }
        private void OnReportTemplateChanges()
        {
            using (var streamAssembly = new MemoryStream())
            {
                ReportTemplate.Render(false);
                ReportTemplate.Compile(streamAssembly);
                var bytes = streamAssembly.ToArray();
                Item.ReportAssembly = bytes;
                TryGetReportFromAssembly();
            }
            using (var streamTemplate = new MemoryStream())
            {
                ReportTemplate.Render(false);
                ReportTemplate.Save(streamTemplate);
                var bytes = streamTemplate.ToArray();
                Item.ReportTemplate = bytes;
            }

        }

        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name")
            {
                DisplayName = Item.Name;

            }
            IsEdited = true;
        }
        private void RequestParameters_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IsEdited = true;
        }

        protected abstract object GetReportData();
        #endregion

        #region Override
        public override void CanClose(Action<bool> callback)
        {
            if (IsAdded || IsDeleted || IsEdited)
                WindowManager.Confirm("�� �������?",
                    "���� �������� ������������� ������.\n�� �������, ��� ������ ��� �������?", () =>
                    {
                        callback(true);
                    },
                    () =>
                    {
                        callback(false);
                    });
            else
            {
                callback(true);
            }
        }

        /// <summary>
        /// Called when activating.
        /// </summary>
        protected override void OnActivate()
        {
            base.OnActivate();
            Item = ReportService.GetByID(_id);
            //���� ����� �����
            if (Item == null)
            {
                Item = new Report { ID = _id, Name = "����� �����", RequestType = _reportViewModelToReportTypeCorrespondence.GetCorrespondedRequestType(GetType()) };
                IsAdded = true;
                Item.UserReportRequestParameter = new UserReportRequestParameters() { ID = Guid.NewGuid(), ReportID = Item.ID };
                RequestParameters = Activator.CreateInstance<T>();
                
                {
                    
                }
            }
            //���� ����� �� �����, �� ��� ���������� �������
            else if (Item.UserReportRequestParameter == null)
            {
                Item.UserReportRequestParameter = new UserReportRequestParameters() { ID = Guid.NewGuid(), ReportID = Item.ID };
                RequestParameters = Activator.CreateInstance<T>();
            }
            //���� ����� �� ����� � ���� ��������� �������
            else
            {

                if (string.IsNullOrWhiteSpace(Item.UserReportRequestParameter.RequestParameters))
                    RequestParameters = Activator.CreateInstance<T>();
                try
                {
                    using (TextReader reader = new StringReader(Item.UserReportRequestParameter.RequestParameters))
                    {
                        var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                        RequestParameters = (T)serializer.Deserialize(reader);
                    }

                }
                catch (Exception)
                {
                    RequestParameters = Activator.CreateInstance<T>();
                }

            }
            Item.PropertyChanged += Item_PropertyChanged;
            RequestParameters.PropertyChanged += RequestParameters_PropertyChanged;
            DisplayName = Item.Name;
            RefreshReportCommand.Execute(null);
            Initialize();
        }
        /// <summary>
        /// Called when deactivating.
        /// </summary>
        /// <param name="close">Inidicates whether this instance will be closed.</param>
        protected override void OnDeactivate(bool close)
        {
            Item.PropertyChanged -= Item_PropertyChanged;
            RequestParameters.PropertyChanged -= RequestParameters_PropertyChanged;

        }
        #endregion

        #region Implementation of IEditStatus

        public bool IsEdited
        {
            get { return _isEdited; }
            set
            {
                if (value.Equals(_isEdited)) return;
                _isEdited = value;
                NotifyOfPropertyChange("IsEdited");
            }
        }

        public bool IsDeleted
        {
            get { return _isDeleted; }
            set
            {
                if (value.Equals(_isDeleted)) return;
                _isDeleted = value;
                NotifyOfPropertyChange("IsDeleted");
            }
        }

        public bool IsAdded
        {
            get { return _isAdded; }
            set
            {
                if (value.Equals(_isAdded)) return;
                _isAdded = value;
                NotifyOfPropertyChange("IsAdded");
            }
        }

        #endregion


    }
}