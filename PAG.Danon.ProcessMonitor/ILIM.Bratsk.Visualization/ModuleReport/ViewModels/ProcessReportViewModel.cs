using System;
using System.Linq;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Views;
using Stimulsoft.Report;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using System.Collections.Generic;
using Castle.MicroKernel.ModelBuilder.Descriptors;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure.Helpers;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels
{


    [View(typeof(ProcessReportParametersView), Context = "ReportParameters")]
    public class ProcessReportViewModel : ReportViewModelBase<ProcessRequestParameters>
    {
        private readonly IProcessRequest _processReportRequest;
        private readonly IPhaseService _phaseRepository;

        public ProcessReportViewModel(IEventAggregator eventAggregator,
            IWindowManager windowManager, ReportViewModelToReportTypeCorrespondence reportViewModelToReportTypeCorrespondence,
            IReportService reportService,
            Guid id,
            IProcessRequest processReportRequest,
            IPhaseService phaseRepository


        )
            : base(eventAggregator, windowManager, reportViewModelToReportTypeCorrespondence, reportService, id)
        {
            _processReportRequest = processReportRequest;
            _phaseRepository = phaseRepository;
            _seList = _phaseRepository
                .Get()
                .Where(e => e.Class == PhaseClass.prod)
                .Select(e => new ShortEntity() {ID = e.ID, Name = e.Name})
                .OrderBy(e=>e.Name)
                .ToList();

          
        }


        private List<ShortEntity> _seList ;

        public List<ShortEntity> seList
        {
            get
            {
                if (_seList == null) { _seList = _phaseRepository
                .Get()
                .Where(e => e.Class == PhaseClass.prod)
                .Select(e => new ShortEntity() { ID = e.ID, Name = e.Name })
                .ToList();
                } return _seList;
            }
        }



        #region Overrides of ReportViewModelBase<MilkReceiveRequestParameters>



        protected override void InitVariables(StiReport report, ProcessRequestParameters requestParameters, object reportData)
        {

        }

        protected override void Initialize()
        {
        }

        protected override object GetReportData()
        {

            RequestParameters.Phases = _seList.Where(e=>e.IsSelected ==true).ToList();
            var ret = _processReportRequest.Request(RequestParameters);
            return ret;
        }

        #endregion

    }
}