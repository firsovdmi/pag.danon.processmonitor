using System;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels
{
    public interface IReportViewModel
    {
        event EventHandler<StiReportEventArgs> Refreashed;
    }
}