﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Stimulsoft.Report;

namespace PAG.WBD.Milk.Visualization.ModuleReports.Views
{
    public partial class ShowReportControl : Control
    {
        public ShowReportControl(StiReport report)
        {
            if (report!=null)
                InitializeComponent(report);
        }
    }
}
