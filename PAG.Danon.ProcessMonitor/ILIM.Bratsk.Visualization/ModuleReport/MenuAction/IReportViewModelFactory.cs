using System;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.MenuAction
{
    public interface  IReportViewModelFactory
    {
        T Create<T>(Guid id) where T : IReportViewModel;
        void Release(object reportViewModelBase);
    }
}