using System;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.MenuAction
{
    public class ShowMyPlcBufferMenuAction : ReportMenuAction<MyPlcBufferReportViewModel>, IHandle<AccountDataChanged>
    {
         IActiveAccountManager _activeAccountManager;
        public ShowMyPlcBufferMenuAction(IActiveAccountManager activeAccountManager, IRadDockingManager radDockingManager, IReportViewModelFactory reportViewModelFactory, IWindowManager windowManager, IReportService reportService, ReportViewModelToReportTypeCorrespondence reportViewModelToReportTypeCorrespondence) : base(radDockingManager, reportViewModelFactory, windowManager, reportService, reportViewModelToReportTypeCorrespondence, "��� �����")
        {
        }

        public void Handle(AccountDataChanged message)
        {
            
        }
    }
}