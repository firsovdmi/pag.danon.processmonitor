﻿namespace PAG.WBD.Milk.Visualization.ModuleReports.Views
{
    partial class ShowReportControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(Stimulsoft.Report.StiReport Report)
        {
            this.PreviewControl.AllowDrop = true;
            this.PreviewControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PreviewControl.Location = new System.Drawing.Point(152, 0);
            this.PreviewControl.Name = "PreviewControl";
            this.PreviewControl.Report = Report;
            this.PreviewControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PreviewControl.ShowZoom = true;
            this.PreviewControl.Size = new System.Drawing.Size(573, 668);
            this.PreviewControl.TabIndex = 0;
            //this.PreviewControl.ToolBar.Controls.Add(btnDesign);

            // лишние кнопки
            this.PreviewControl.ShowOpen = false;
            this.PreviewControl.ShowSendEMail = false;
            this.PreviewControl.ShowPageNew = false;
            this.PreviewControl.ShowPageDelete = false;
            this.PreviewControl.ShowPageDesign = false;
            this.PreviewControl.ShowPageSize = false;
            this.PreviewControl.ShowFind = false;
            this.PreviewControl.ShowFullScreen = false;
            this.PreviewControl.ShowPageViewMode = false;
            this.PreviewControl.ShowZoomPageWidth = false;
            this.PreviewControl.ShowCloseButton = false;
            this.PreviewControl.ShowViewModeMultiplePages = false;
            this.PreviewControl.ShowZoomTwoPages = false;
            this.PreviewControl.ShowZoomMultiplePages = false;
            this.PreviewControl.ShowZoomOnePage = false;
            this.PreviewControl.ShowViewModeSinglePage = false;
            this.PreviewControl.ShowViewModeContinuous = false;


            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Name = "PreviewReport";
            this.ResumeLayout(false);
            this.Controls.Add(PreviewControl);

            //
            this.ClientSize = new System.Drawing.Size(725, 668);
            this.Controls.Add(this.PreviewControl);
            this.Text = Report.ReportName;

        }

        #endregion

        private Stimulsoft.Report.Viewer.StiViewerControl PreviewControl = new Stimulsoft.Report.Viewer.StiViewerControl();
    }
}
