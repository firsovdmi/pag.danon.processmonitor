using System;
using System.Linq;
using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Domain.RemoteFacade;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels;
using Action = System.Action;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.MenuAction
{
    public abstract class ReportMenuAction<T> : ReportMenuAction, IHandle<ReportChanges> where T : SelectableScreen, IReportViewModel
    {
        protected readonly IRadDockingManager RadDockingManager;
        protected readonly IReportViewModelFactory ReportViewModelFactory;
        protected readonly IWindowManager WindowManager;
        protected readonly IReportService ReportService;
        private readonly ReportViewModelToReportTypeCorrespondence _reportViewModelToReportTypeCorrespondence;
        protected object _executeLock = new object();


        protected ReportMenuAction(string displayName)
            : base(displayName)
        {
        }

        /// <summary>
        /// Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public ReportMenuAction(IActiveAccountManager activeAccountManager, string displayName, Action execute, Func<bool> canExecute = null)
            : base(activeAccountManager,displayName, execute, canExecute)
        {
            if (activeAccountManager.WhiteListObjects.Contains("Action1"))
            {
                IsEnable = true;
            }
            else
            {
                IsEnable = false;
            }
        }

        public ReportMenuAction(IRadDockingManager radDockingManager,
            IReportViewModelFactory reportViewModelFactory,
            IWindowManager windowManager,
            IReportService reportService,
            ReportViewModelToReportTypeCorrespondence reportViewModelToReportTypeCorrespondence,
            string displayName)
            : base(displayName)
        {
            RadDockingManager = radDockingManager;
            ReportViewModelFactory = reportViewModelFactory;
            WindowManager = windowManager;
            ReportService = reportService;
            _reportViewModelToReportTypeCorrespondence = reportViewModelToReportTypeCorrespondence;


            GetSubActions();
        }

        private void GetSubActions()
        {
            Items.Clear();
            foreach (var rep in ReportService.GetShortListByRequestType(_reportViewModelToReportTypeCorrespondence.GetCorrespondedRequestType<T>()).OrderBy(p => p.Name))
            {
                ShortEntity rep1 = rep;
                Items.Add(new ActionItem(rep.Name, () =>
                {
                    lock (_executeLock)
                    {
                        var viewmodel = ReportViewModelFactory.Create<T>(rep1.ID);
                        //Anti-double subscribe hack
                        viewmodel.Deactivated -= Release;
                        viewmodel.Deactivated += Release;

                        RadDockingManager.ShowWindow(viewmodel, "MainView");
                    }
                }));
            }
            Items.Add(new SeparatorMenuItem());
            Items.Add(new ActionItem("�������� �����", () =>
            {
                lock (_executeLock)
                {
                    var viewmodel = ReportViewModelFactory.Create<T>(Guid.NewGuid());
                    //Anti-double subscribe hack
                    viewmodel.Deactivated -= Release;
                    viewmodel.Deactivated += Release;

                    RadDockingManager.ShowWindow(viewmodel, "MainView");
                }
            }));
        }


        protected void Release(object sender, DeactivationEventArgs e)
        {
            var model = (T)sender;
            model.Deactivated -= Release;
            ReportViewModelFactory.Release(model);
        }

        #region Overrides of ActionItem

        /// <summary>
        /// The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {
           
        }

        #endregion

        #region Implementation of IHandle<ReportChanges>

        /// <summary>
        /// Handles the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(ReportChanges message)
        {
            GetSubActions();
        }

        #endregion
    }
    public abstract class ReportMenuAction : ActionItem
    {
        protected ReportMenuAction(string displayName) : base(displayName)
        {
        }

        /// <summary>
        /// Initializes a new instance of ActionItem class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="execute">The execute.</param>
        /// <param name="canExecute">The can execute.</param>
        public ReportMenuAction(IActiveAccountManager activeAccountManager, string displayName, Action execute, Func<bool> canExecute = null)
            : base(displayName, execute, canExecute)
        {
            if (activeAccountManager.WhiteListObjects.Contains("Action1"))
            {
                IsEnable = true;
            }
            else
            {
                IsEnable = false;
            }
        }
    }
}