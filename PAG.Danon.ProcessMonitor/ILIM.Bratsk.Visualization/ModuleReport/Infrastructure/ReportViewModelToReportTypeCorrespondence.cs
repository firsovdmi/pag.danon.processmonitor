using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure
{
    public class ReportViewModelToReportTypeCorrespondence
    {
        public RequestType GetCorrespondedRequestType<T>()
        {
            return GetCorrespondedRequestType(typeof (T));
        }

        public RequestType GetCorrespondedRequestType(Type type)
        {
            //if (type == typeof(ProcessReportViewModel))
            //{
            //    return RequestType.Process;
            //}

            //if (type == typeof(CIPReportViewModel))
            //{
            //    return RequestType.CIP;
            //}

            if (type == typeof(ReceptionReportViewModel)
                //|| type == typeof(MXOReportViewModel)
                //|| type == typeof(AppReportViewModel)
                //|| type == typeof(CheeseReportViewModel)
                )
            {
                return RequestType.Reception;
            }

            if (type == typeof(ReceptionCIPReportViewModel)
                //|| type == typeof(CheeseCIPReportViewModel)
                //|| type == typeof(MXOCIPReportViewModel)
                //|| type == typeof(AppCIPReportViewModel)
                )
            {
                return RequestType.CIP;
            }

            if (type == typeof(MXOReportViewModel))
                {
                return RequestType.MXO;
                }

            if (type == typeof(MXOCIPReportViewModel))
                {
                return RequestType.MXOCIP;
                }
            if (type == typeof(AppReportViewModel))
                {
                return RequestType.App;
                }

            if (type == typeof(AppCIPReportViewModel))
                {
                return RequestType.AppCIP;
                }
            if (type == typeof(CheeseReportViewModel))
                {
                return RequestType.Cheese;
                }

            if (type == typeof(CheeseCIPReportViewModel))
                {
                return RequestType.CheeseCIP;
                }
            return default(RequestType);


           
            
        }
    }
}