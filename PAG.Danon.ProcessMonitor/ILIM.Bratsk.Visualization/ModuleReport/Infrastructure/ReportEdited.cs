using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure
{
    public class ReportEdited : ReportChanges
    {
        public ReportEdited(Report content)
            : base(content)
        {
        }
    }
}