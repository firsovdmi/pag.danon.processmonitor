using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using Stimulsoft.Report;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure
{
    public class ReportAdded : ReportChanges
    {
        public ReportAdded(Report content) : base(content)
        {
        }
    }

    public class StiReportEventArgs:EventArgs
    {
        public StiReport StiReport { get; private set; }

        public StiReportEventArgs(StiReport stiReport)
        {
            StiReport = stiReport;
        }
    }
}