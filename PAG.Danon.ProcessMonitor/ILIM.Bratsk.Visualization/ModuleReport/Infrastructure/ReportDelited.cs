using PAG.Danon.ProcessMonitor.Domain.Model;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure
{
    public class ReportDelited : ReportChanges
    {
        public ReportDelited(Report content)
            : base(content)
        {
        }
    }
}