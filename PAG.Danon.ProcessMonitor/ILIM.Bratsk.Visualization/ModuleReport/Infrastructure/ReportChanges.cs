using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure
{
    public class ReportChanges : Message<Report>
    {
        public Report Content { get; set; }

        public ReportChanges(Report content)
        {
            Content = content;
        }
    }
}