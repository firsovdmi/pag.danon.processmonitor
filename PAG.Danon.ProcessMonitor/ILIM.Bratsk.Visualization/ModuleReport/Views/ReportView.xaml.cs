﻿using System.Windows;
using System.Windows.Controls;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleReport.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleReport.Views
{
    /// <summary>
    /// Логика взаимодействия для ReportView.xaml
    /// </summary>
    public partial class ReportView : UserControl
    {
        public ReportView()
        {
            InitializeComponent();
        }


        private void CipReportView_OnUnloaded(object sender, RoutedEventArgs e)
        {
            var reportvieModel = DataContext as IReportViewModel;
            if (reportvieModel != null)
                reportvieModel.Refreashed -= reportvieModel_Refreashed;
        }

        private void CipReportView_OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {

            var reportvieModel = e.OldValue as IReportViewModel;
            if (reportvieModel != null)
                reportvieModel.Refreashed -= reportvieModel_Refreashed;
            reportvieModel = e.NewValue as IReportViewModel;
            if (reportvieModel != null)
                reportvieModel.Refreashed += reportvieModel_Refreashed;
        }

        private void reportvieModel_Refreashed(object sender, StiReportEventArgs stiReport)
        {
            var showControl = new ShowReportControl(stiReport.StiReport);
            ReportWinForm.Child = showControl;
        }


    }


}
