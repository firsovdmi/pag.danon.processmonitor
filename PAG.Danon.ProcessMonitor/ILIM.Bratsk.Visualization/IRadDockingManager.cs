using System;
using Caliburn.Micro;
using PAG.WBD.Milk.Visualization.ViewModels;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Docking;

namespace PAG.WBD.Milk.Visualization
{
    public interface IRadDockingManager
    {
        void Init(IViewAware trackingShellViewModel);

        void ShowWindow(IViewAware viewModel,
            object context,
            Action<RadPaneEx> configRadPane = null,
            Action<RadPaneGroup> configRadPaneGroup = null,
            Action<RadSplitContainer> configSplitContainer = null
            );

        void ShowDockedWindow(IViewAware viewModel,
            object context,
            DockPosition dockPosition = DockPosition.Center,
            Action<RadPaneEx> configRadPane = null,
            Action<RadPaneGroup> configRadPaneGroup = null,
            Action<RadSplitContainer> configSplitContainer = null
            );

        void ShowFloatingDockableWindow(IViewAware viewModel,
            object context,
            Action<RadPaneEx> configRadPane = null,
            Action<RadPaneGroup> configRadPaneGroup = null,
            Action<RadSplitContainer> configSplitContainer = null);

        void ShowFloatingWindow(IViewAware viewModel,
            object context,
            Action<RadPaneEx> configRadPane = null,
            Action<RadPaneGroup> configRadPaneGroup = null,
            Action<RadSplitContainer> configSplitContainer = null);
 
        void CloseAllWindows();
        void LoadLayout();
        string SaveLayout();
    }


    public interface IViewAwareFactory
    {
        IViewAware Create();
        void Release(IViewAware viewAware);
    }
    public interface IFarmDictionaryViewModelFactory : IViewAwareFactory
    {
        FarmDictionaryViewModel Create();
        void Release(FarmDictionaryViewModel viewAware);
    }

    public interface ISortDictionaryViewModelFactory : IViewAwareFactory
    {
        SortDictionaryViewModel Create();
        void Release(SortDictionaryViewModel viewAware);
    }
}