namespace PAG.WBD.Milk.Visualization.ViewModels
{
    public class ShowSortDictionaryAction : ShowDictionaryAction
    {
        protected ISortDictionaryViewModelFactory _viewAwareFactory;
        public ShowSortDictionaryAction(IRadDockingManager radDockingManager, ISortDictionaryViewModelFactory viewAwareFactory)
            : base("����")
        {
            _radDockingManager = radDockingManager;
            _viewAwareFactory = viewAwareFactory;
        }
        /// <summary>
        /// The action associated to the ActionItem
        /// </summary>
        public override void Execute()
        {
            _radDockingManager.ShowWindow(_viewAwareFactory.Create(), null);
        }
    }
}