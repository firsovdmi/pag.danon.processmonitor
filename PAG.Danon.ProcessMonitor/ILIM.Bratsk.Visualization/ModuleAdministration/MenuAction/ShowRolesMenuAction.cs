using Caliburn.Micro;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleAdministration.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleAdministration.MenuAction
{
    public class ShowRolesMenuAction : AdministrationMenuAction<RolesViewModel>
    {
        public ShowRolesMenuAction(string displayName) : base(displayName)
        {
        }

        public ShowRolesMenuAction(IRadDockingManager radDockingManager, IScreenFactory screenFactory,
            IEventAggregator eventAggregator, IActiveAccountManager activeAccountManager)
            : base(radDockingManager, screenFactory, "����", eventAggregator, activeAccountManager)
            {
            IsEnable = activeAccountManager.ActiveAccount.Account.Login.Equals("Admin1");

            }

        protected override string AuthorizeObjectName
        {
            get { return "GetRolesShorInfo"; }
        }
    }
}