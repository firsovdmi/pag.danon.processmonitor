using System.Collections.Generic;
using AutoMapper;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.Model;
using PAG.Danon.ProcessMonitor.Visualization.Infrastructure;
using PAG.Danon.ProcessMonitor.Visualization.ModuleAdministration.MenuAction;
using PAG.Danon.ProcessMonitor.Visualization.ModuleAdministration.ViewModels;

namespace PAG.Danon.ProcessMonitor.Visualization.ModuleAdministration
{
    public class AdministrationModule : IModule
    {
        public AdministrationModule(IMenuManager menuManager,
            IToolBarManager toolBarManager,
            IEnumerable<AdministrationMenuAction> actionItems,
            AdministrationMenuAction administrationActionItems
            )
        {
            menuManager.ShowItem(new ActionItem("�����������������", null),3);
            foreach (var actionItem in actionItems)
                menuManager.WithParent("�����������������")
                    .ShowItem(actionItem);

            Mapper.CreateMap<Account, AccountViewModel>();
            Mapper.CreateMap<AccountViewModel, Account>();
        }
    }
}