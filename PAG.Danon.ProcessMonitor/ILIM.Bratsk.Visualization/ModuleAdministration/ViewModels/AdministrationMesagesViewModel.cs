﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Caliburn.Micro;
using PAG.WBD.Milk.Infrastructure.AAA.AuthenticationClasses;
using PAG.WBD.Milk.Visualization.Infrastructure;

namespace PAG.WBD.Milk.Visualization.ModuleAdministration.ViewModels
{
    //public class AdministrationMesagesViewModel : SelectableScreen
    //{
    //    protected readonly IToolBarManager _toolBarManager;
    //    private readonly IAAARemoteFasade _remoteFacade;
    //    private readonly IWindowManager _windowManager;

    //    public ObservableCollection<LoginInfoItemViewModel> Logins { get; set; }
    //    public ObservableCollection<MessageTypesViewModel> Types { get; set; }
    //    public QueryParameters Parameters { get; set; }
    //    public ObservableCollection<MessageItemViewModel> Items { get; set; }

    //    private bool _autoScan;

    //    public bool AutoScan
    //    {
    //        get { return _autoScan; }
    //        set
    //        {
    //            lock (_lock)
    //            {
    //                if (value && _fOff)
    //                {
    //                    StartScan();
    //                }
    //                _fOff = !value;
    //            }
    //            _autoScan = value;
    //        }
    //    }

    //    public AdministrationMesagesViewModel(IToolBarManager toolBarManager, IAAARemoteFasade autentificator, IWindowManager windowManager)
    //    {
    //        _toolBarManager = toolBarManager;
    //        _remoteFacade = autentificator;
    //        _windowManager = windowManager;
    //        DisplayName = "Просмотр системных логов";
    //        RefreshAll();
    //    }

    //    private void RefreshAll()
    //    {
    //        Logins = new ObservableCollection<LoginInfoItemViewModel>(_remoteFacade.GetAllLogins().Select(p => new LoginInfoItemViewModel
    //            {
    //                Id = p.Id,
    //                Login = p.Login,
    //                IsWindowsIdentity = p.IsWindowsIdentity,
    //                AutoLogoffTime = (int)p.AutoLogoffTime.TotalMinutes,
    //                Email = p.Email,
    //                IsPasswordChanged = p.IsChangePassword,
    //                IsSelected = true,
    //                AcessGroups = new ObservableCollection<RoleViewModel>()
    //            }));
    //        Types = new ObservableCollection<MessageTypesViewModel>(_remoteFacade.GetMessagesTypes().Select(p => new MessageTypesViewModel { CustomName = p, IsSelected = true }));
    //    }

    //    #region RefreshItemsCommand
    //    private RelayCommand _refreshItemsCommandCommand;

    //    public RelayCommand RefreshItemsCommand
    //    {
    //        get
    //        {
    //            return _refreshItemsCommandCommand ??
    //                   (_refreshItemsCommandCommand = new RelayCommand(ExecuteRefreshItemCommand, CanExecuteRefreshItemCommand, CommandType.None, "Обновить", "Обновить отчет"));
    //        }
    //    }


    //    protected virtual bool CanExecuteRefreshItemCommand(object obj)
    //    {
    //        return !AutoScan;
    //    }

    //    protected virtual void ExecuteRefreshItemCommand(object obj)
    //    {
    //        var parameters = new MessageQuery
    //        {
    //            FromTime = Parameters.FromTime,
    //            ToTime = Parameters.ToTime,
    //            LoginsId = Logins.Where(p => p.IsSelected).Select(p => p.Login).ToList(),
    //            Types = Types.Where(p => p.IsSelected).Select(p => p.CustomName).ToList(),
    //            Par1 = Parameters.Par1,
    //            Par2 = Parameters.Par2,
    //            Par3 = Parameters.Par3,
    //            TopCount = Parameters.TopCount
    //        };
    //        var _newItems = new ObservableCollection<MessageItemViewModel>(_remoteFacade.GetMessageItems(parameters).Select(p => new MessageItemViewModel
    //                            {
    //                                Time = p.Time,
    //                                Login = p.Login,
    //                                Type = p.Type,
    //                                Par1 = p.Par1,
    //                                Par2 = p.Par2,
    //                                Par3 = p.Par3,
    //                                Message = p.Message,
    //                                MessageKind = p.MessageKind
    //                            }));
    //        if (Items == null) Items = new ObservableCollection<MessageItemViewModel>();
    //        var addedItems = _newItems.Except<MessageItemViewModel>(Items, new Cmp1()).ToList();
    //        foreach (var i in addedItems)
    //        {
    //            Items.Add(i);
    //        }
    //        var deletedItems = Items.Except<MessageItemViewModel>(_newItems, new Cmp1()).ToList();
    //        foreach (var i in deletedItems)
    //        {
    //            Items.Remove(i);
    //        }
    //        NotifyOfPropertyChange(() => Items);
    //    }
    //    #endregion

    //    private Timer _timer;
    //    private readonly object _lock = new object();
    //    private bool _fOff = true;
    //    private int _scanIntervalInMilliseconds = 500;

    //    public void StartScan()
    //    {
    //        _timer = new Timer(Callback, null, _scanIntervalInMilliseconds, Timeout.Infinite);
    //    }

    //    public void StopScan()
    //    {
    //        lock (_lock)
    //        {
    //            _fOff = true;
    //            _timer.Change(Timeout.Infinite, Timeout.Infinite);
    //        }
    //    }

    //    private void Callback(Object state)
    //    {
    //        ExecuteRefreshItemCommand(null);
    //        lock (_lock)
    //        {
    //            if (!_fOff) _timer.Change(_scanIntervalInMilliseconds, Timeout.Infinite);
    //        }
    //    }


    //}

    //public class QueryParameters : BaseViewModel
    //{
    //    public QueryParameters()
    //    {
    //        ToTime = DateTime.Now;
    //        FromTime = DateTime.Now.AddDays(-1);
    //        NotifyOfPropertyChange((() => ToTime));
    //        NotifyOfPropertyChange((() => FromTime));
    //    }
    //    public DateTime FromTime { get; set; }
    //    public DateTime ToTime { get; set; }
    //    public List<LoginInfoItemViewModel> Logins { get; set; }
    //    public List<string> Types { get; set; }
    //    public string Par1 { get; set; }
    //    public string Par2 { get; set; }
    //    public string Par3 { get; set; }

    //    public int TopCount
    //    {
    //        get { return _topCount; }
    //        set { _topCount = value; }
    //    }

    //    private int _topCount = 100;
    //}

    //public class MessageItemViewModel
    //{
    //    public DateTime Time { get; set; }
    //    public string Login { get; set; }
    //    public string Type { get; set; }
    //    public string Par1 { get; set; }
    //    public string Par2 { get; set; }
    //    public string Par3 { get; set; }
    //    public string Message { get; set; }

    //    public string FirstLineMessage
    //    {
    //        get { return Message.Split('\n')[0]; }
    //    }
    //    public int MessageKind { get; set; }
    //}

    //public class MessageTypesViewModel : BaseViewModel
    //{
    //    public string Name { get; set; }
    //    public string CustomName { get; set; }
    //}

    //public class Cmp1 : IEqualityComparer<MessageItemViewModel>
    //{
    //    #region IEqualityComparer<YourClass> Members

    //    public bool Equals(MessageItemViewModel x, MessageItemViewModel y)
    //    {
    //        return
    //            x.Login == y.Login && x.Message == y.Message && x.Time == y.Time;
    //    }

    //    public int GetHashCode(MessageItemViewModel obj)
    //    {
    //        int hCode = obj.Message.GetHashCode() ^ obj.Message.GetHashCode();
    //        return hCode.GetHashCode();
    //    }

    //    #endregion
    //}
}
