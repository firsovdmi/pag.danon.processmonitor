using System.Linq;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class CipItemsRepository : RepositoryEntityWithMarkedAsDelete<CipItem>, ICipItemsRepository
    {
        public CipItemsRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public CipItem GetLastItem(int plcItemID)
        {
            return
                _unitOfWork.Get<CipItem>()
                    .Where(p => p.PlcItemID == plcItemID)
                    .OrderByDescending(p => p.PlcDateTime)
                    .FirstOrDefault();
        }
    }
}