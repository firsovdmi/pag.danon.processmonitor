using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class PhaseStatusRepository : PlcNBaseRepository<PhaseStatus>, IPhaseStatusRepository
    {
        public PhaseStatusRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override PhaseStatus Create(PhaseStatus entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}