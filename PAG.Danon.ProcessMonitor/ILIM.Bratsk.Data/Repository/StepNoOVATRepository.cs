using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class StepNoOVATRepository : RepositoryEntityWithMarkedAsDelete<StepNoOVAT>, IStepNoOVATRepository
    {
        public StepNoOVATRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override StepNoOVAT Create(StepNoOVAT entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}