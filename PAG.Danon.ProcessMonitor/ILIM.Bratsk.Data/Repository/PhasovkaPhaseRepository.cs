using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class PhasovkaPhaseRepository : RepositoryEntityWithMarkedAsDelete<PhasovkaPhase>, IPhasovkaPhaseRepository{
        public PhasovkaPhaseRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}