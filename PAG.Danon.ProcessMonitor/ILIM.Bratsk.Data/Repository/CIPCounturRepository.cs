using System;
using PAG.Danon.ProcessMonitor.Domain.Model;


namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class CipConturRepository : RepositoryEntityWithMarkedAsDelete<CipContur>,  ICipConturRepository 
    {
        public CipConturRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override CipContur Create(CipContur entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}