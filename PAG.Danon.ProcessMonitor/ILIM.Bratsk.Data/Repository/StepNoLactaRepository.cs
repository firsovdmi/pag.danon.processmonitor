using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class StepNoLactaRepository : RepositoryEntityWithMarkedAsDelete<StepNoLacta>, IStepNoLactaRepository
    {
        public StepNoLactaRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override StepNoLacta Create(StepNoLacta entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}