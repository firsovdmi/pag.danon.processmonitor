using System.Linq;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class WorkDetailsRepository : RepositoryEntityWithMarkedAsDelete<WorkDetail>, IWorkDetailsRepository
    {
        public WorkDetailsRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public WorkDetail GetLastItem(int plcIWorkID)
        {
            return
                _unitOfWork.Get<WorkDetail>()
                    .Where(p => p.PlcIWorkID == plcIWorkID)
                    .OrderByDescending(p => p.PlcDateTime)
                    .FirstOrDefault();
        }
    
    }
}