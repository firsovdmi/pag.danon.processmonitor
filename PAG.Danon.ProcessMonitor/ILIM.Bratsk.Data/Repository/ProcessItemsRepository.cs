using System.Linq;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class ProcessItemRepository : RepositoryEntityWithMarkedAsDelete<ProcessItemRepository>, IProcessReportItemsRepository
    {
        public ProcessItemRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public ProcessItemRepository GetLastItem(int plcItemID)
        {
            return _unitOfWork.GetTracked<ProcessItemRepository>()
                    .Where(p => p.PlcItemID == plcItemID)
                    .OrderByDescending(p => p.PlcDateTime)
                    .FirstOrDefault() ?? _unitOfWork.Get<ProcessItemRepository>()
                .Where(p => p.PlcItemID == plcItemID)
                .OrderByDescending(p => p.PlcDateTime)
                .FirstOrDefault();
        }
    }
}