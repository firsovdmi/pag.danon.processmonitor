using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class AccountDataRepository : RepositoryEntity<AccountData>, IAccountDataRepository
    {
        public AccountDataRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}