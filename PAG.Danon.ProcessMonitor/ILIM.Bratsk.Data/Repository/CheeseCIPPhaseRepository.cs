using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class CheeseCIPPhaseRepository : RepositoryEntityWithMarkedAsDelete<CheeseCIPPhase>, ICheeseCIPPhaseRepository{
        public CheeseCIPPhaseRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}