using System.Linq;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class AppDetailsRepository : RepositoryEntityWithMarkedAsDelete<AppDetails>, IAppDetailsRepository
    {
        public AppDetailsRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        //public CipDetail GetLastItem(int plcIWorkID)
        //{
        //    return
        //        _unitOfWork.Get<CipDetail>()
        //            .Where(p => p.PlcIWorkID == plcIWorkID)
        //            .OrderByDescending(p => p.PlcDateTime)
        //            .FirstOrDefault();
     //   }
    
    }
}