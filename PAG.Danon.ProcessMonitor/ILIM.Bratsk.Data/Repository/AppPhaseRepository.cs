using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class AppPhaseRepository : RepositoryEntityWithMarkedAsDelete<AppPhase>, IAppPhaseRepository{
        public AppPhaseRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}