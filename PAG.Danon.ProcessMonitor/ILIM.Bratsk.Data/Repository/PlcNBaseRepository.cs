﻿using System;
using System.Linq;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public abstract class PlcNBaseRepository<T> : RepositoryEntityWithMarkedAsDelete<T>, IPlcNBaseRepository<T>
        where T : PlcNEntity, IMarkableForDelete
    {
        public PlcNBaseRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public T GetItemByPlcN(int plcN)
        {
            return _unitOfWork.Get<T>().Where(p => p.PlcN == plcN && !p.IsDeleted).OrderByDescending(p => p.DateTimeCreate).FirstOrDefault();
        }
    }
}