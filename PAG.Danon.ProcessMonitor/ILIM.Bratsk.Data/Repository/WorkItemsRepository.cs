using System.Linq;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class WorkItemsRepository : RepositoryEntityWithMarkedAsDelete<WorkItem>, IWorkItemsRepository
    {
        public WorkItemsRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public WorkItem GetLastItem(int plcItemID)
        {
            return _unitOfWork.GetTracked<WorkItem>()
                    .Where(p => p.PlcItemID == plcItemID)
                    .OrderByDescending(p => p.PlcDateTime)
                    .FirstOrDefault() ?? _unitOfWork.Get<WorkItem>()
                .Where(p => p.PlcItemID == plcItemID)
                .OrderByDescending(p => p.PlcDateTime)
                .FirstOrDefault();
        }
    }
}