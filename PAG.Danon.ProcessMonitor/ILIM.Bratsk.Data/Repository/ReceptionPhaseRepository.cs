using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class ReceptionPhaseRepository : RepositoryEntityWithMarkedAsDelete<ReceptionPhase>, IReceptionPhaseRepository{
        public ReceptionPhaseRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}