using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class MyPlcBufferRepository : RepositoryEntityWithMarkedAsDelete<MyPLCBuffer>, IMyPlcBufferRepository
    {
        public MyPlcBufferRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}