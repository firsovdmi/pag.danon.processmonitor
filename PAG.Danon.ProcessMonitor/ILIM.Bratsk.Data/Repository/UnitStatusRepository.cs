using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class UnitStatusRepository : PlcNBaseRepository<UnitStatus>, IUnitStatusRepository
    {
        public UnitStatusRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override UnitStatus Create(UnitStatus entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}