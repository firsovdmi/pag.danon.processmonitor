﻿using System.Linq;
using PAG.Danon.ProcessMonitor.Domain.Repository;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public abstract class RepositoryEntityWithMarkedAsDelete<T> : RepositoryEntity<T>, IRepositoryWithMarkedAsDelete<T>
        where T : EntityBase, IMarkableForDelete
    {
        public RepositoryEntityWithMarkedAsDelete(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public virtual T MarkAsDelete(T entity)
        {
            var entityToUpdate = _unitOfWork.Get<T>().FirstOrDefault(p => p.ID == entity.ID);
            if (entityToUpdate != null)
            {
                entityToUpdate.IsDeleted = true;
                return Update(entityToUpdate);
            }
            return null;
        }
    }
}