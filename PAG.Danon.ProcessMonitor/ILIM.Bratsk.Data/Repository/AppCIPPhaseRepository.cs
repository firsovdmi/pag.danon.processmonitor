using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class AppCIPPhaseRepository : RepositoryEntityWithMarkedAsDelete<AppCIPPhase>, IAppCIPPhaseRepository{
        public AppCIPPhaseRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}