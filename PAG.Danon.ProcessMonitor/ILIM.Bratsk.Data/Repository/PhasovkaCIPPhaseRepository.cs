using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class PhasovkaCIPPhaseRepository : RepositoryEntityWithMarkedAsDelete<PhasovkaCIPPhase>, IPhasovkaCIPPhaseRepository{
        public PhasovkaCIPPhaseRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}