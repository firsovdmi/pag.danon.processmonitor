using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class ProgrammNumberRepository : PlcNBaseRepository<ProgrammNumber>, IProgrammNumberRepository
    {
        public ProgrammNumberRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override ProgrammNumber Create(ProgrammNumber entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}