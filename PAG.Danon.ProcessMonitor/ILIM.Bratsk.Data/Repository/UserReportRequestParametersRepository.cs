﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class UserReportRequestParametersRepository : RepositoryEntityWithMarkedAsDelete<UserReportRequestParameters>, IUserReportRequestParametersRepository
    {
        public UserReportRequestParametersRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }


    }
}