using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class MXOCIPPhaseRepository : RepositoryEntityWithMarkedAsDelete<MXOCIPPhase>, IMXOCIPPhaseRepository{
        public MXOCIPPhaseRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}