using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class PlcBufferRepository : RepositoryEntityWithMarkedAsDelete<PlcBuffer>, IPlcBufferRepository
    {
        public PlcBufferRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}