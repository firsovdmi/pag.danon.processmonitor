using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class StepNoProdRepository : PlcNBaseRepository<StepNoProd>, IStepNoProdRepository
    {
        public StepNoProdRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override StepNoProd Create(StepNoProd entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}