using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class CheesePhaseRepository : RepositoryEntityWithMarkedAsDelete<CheesePhase>, ICheesePhaseRepository{
        public CheesePhaseRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}