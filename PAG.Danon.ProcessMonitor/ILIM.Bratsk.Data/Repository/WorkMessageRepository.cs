using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class WorkMessageRepository : PlcNBaseRepository<WorkMessage>, IWorkMessageRepository
    {
        public WorkMessageRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override WorkMessage Create(WorkMessage entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}