using System;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Domain.Repository;

namespace PAG.Danon.ProcessMonitor.Data.Repository
{
    public class EventInfoRepository : PlcNBaseRepository<EventInfo>, IEventInfoRepository
    {
        public EventInfoRepository(IUnitOfWorkDO unitOfWork)
            : base(unitOfWork)
        {
        }

        public override EventInfo Create(EventInfo entity)
        {
            entity.ID = Guid.NewGuid();
            return _unitOfWork.Create(entity);
        }
    }
}