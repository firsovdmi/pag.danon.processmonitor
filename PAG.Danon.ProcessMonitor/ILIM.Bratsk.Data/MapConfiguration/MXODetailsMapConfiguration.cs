using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.MapConfiguration
{
    public class MXODetailsMapConfiguration : EntityMapConfiguration<MXODetails>
    {
        private const string TableName = "MXODetails";

        public MXODetailsMapConfiguration()
        {
          

            HasOptional(e => e.Phase)      .WithMany().HasForeignKey(m => m.PhaseID)      .WillCascadeOnDelete(false);
            HasOptional(e => e.WorkMessage).WithMany().HasForeignKey(m => m.WorkMessageID).WillCascadeOnDelete(false);
            HasOptional(e => e.PhaseStatus).WithMany().HasForeignKey(m => m.PhaseStatusID).WillCascadeOnDelete(false);
            HasOptional(e => e.EventInfo)  .WithMany().HasForeignKey(m => m.EventInfoID)  .WillCascadeOnDelete(false);
            HasOptional(e => e.StepNoProd) .WithMany().HasForeignKey(m => m.StepNoProdID) .WillCascadeOnDelete(false);
            HasOptional(e => e.UnitStatus) .WithMany().HasForeignKey(m => m.UnitStatusID) .WillCascadeOnDelete(false);
            HasOptional(e=>e.Operator)     .WithMany().HasForeignKey(m=>m.OperatorID).WillCascadeOnDelete(false);
            
            //����������� ��� OperatorID
            //   HasOptional(e => e.PhaseID).WithMany().HasForeignKey(m => m.Phase_D).WillCascadeOnDelete(false);



            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}