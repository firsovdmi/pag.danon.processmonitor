using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;



namespace PAG.Danon.ProcessMonitor.Data.MapConfiguration
{
    public class ReportMapConfiguration : EntityMapConfiguration<Report>
    {
        private const string TableName = "Report";
        public ReportMapConfiguration()
        {
            Ignore(p => p.UserReportRequestParameter);
            HasMany(p => p.UserReportRequestParameters).WithRequired(p => p.Report);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}