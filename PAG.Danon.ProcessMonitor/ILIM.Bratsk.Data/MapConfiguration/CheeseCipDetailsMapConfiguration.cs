using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.MapConfiguration
{
    public class CheeseCipDetailsMapConfiguration : EntityMapConfiguration<CheeseCipDetail>
    {
        private const string TableName = "CheeseCIPDetails";

        public CheeseCipDetailsMapConfiguration()
        {
            //HasOptional(e => e.CipItem).WithMany(p => p.Details).HasForeignKey(m => m.CipItemId).WillCascadeOnDelete(true);
            HasOptional(e => e.Message).WithMany().HasForeignKey(m => m.MessageID).WillCascadeOnDelete(false);
            HasOptional(e => e.EventInfo).WithMany().HasForeignKey(m => m.EventInfoID).WillCascadeOnDelete(false);
            HasOptional(e => e.PhaseStatus).WithMany().HasForeignKey(m => m.PhaseStatusID).WillCascadeOnDelete(false);
            HasOptional(e => e.StepNoCIP).WithMany().HasForeignKey(m => m.StepNoCIPID).WillCascadeOnDelete(false);
            HasOptional(e => e.UnitStatus).WithMany().HasForeignKey(m => m.UnitStatusID).WillCascadeOnDelete(false);
            HasOptional(e => e.Phase).WithMany().HasForeignKey(m => m.PhaseID).WillCascadeOnDelete(false);
            HasOptional(e => e.CipContur).WithMany().HasForeignKey(m => m.CipConturID).WillCascadeOnDelete(false);
            HasOptional(e => e.ALCIPPrNo).WithMany().HasForeignKey(m => m.ALCIPPrNoID).WillCascadeOnDelete(false);

            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}