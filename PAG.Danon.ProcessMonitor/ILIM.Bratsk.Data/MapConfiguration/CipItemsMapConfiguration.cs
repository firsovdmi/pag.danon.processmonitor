using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.MapConfiguration
{
    public class CipItemsMapConfiguration : EntityMapConfiguration<CipItem>
    {
        private const string TableName = "CipItem";

        public CipItemsMapConfiguration()
        {
          //  HasMany(e => e.Details).WithOptional().HasForeignKey(p => p.CipItemId).WillCascadeOnDelete(true);
            HasOptional(e => e.Phase).WithMany().HasForeignKey(m => m.PhaseID).WillCascadeOnDelete(false);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}