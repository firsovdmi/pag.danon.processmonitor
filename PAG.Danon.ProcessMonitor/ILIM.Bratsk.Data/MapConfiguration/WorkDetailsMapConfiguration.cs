using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.MapConfiguration
{
    public class WorkDetailsMapConfiguration : EntityMapConfiguration<WorkDetail>
    {
        private const string TableName = "WorkDetail";

        public WorkDetailsMapConfiguration()
        {

            HasOptional(e => e.Message      ).WithMany().HasForeignKey(m => m.MessageID).WillCascadeOnDelete(false);
            HasOptional(e => e.EventInfo    ).WithMany().HasForeignKey(m => m.EventInfoID).WillCascadeOnDelete(false);
            HasOptional(e => e.PhaseStatus  ).WithMany().HasForeignKey(m => m.PhaseStatusID).WillCascadeOnDelete(false);
            HasOptional(e => e.StepNoProd   ).WithMany().HasForeignKey(m => m.StepNoProdID).WillCascadeOnDelete(false);
            HasOptional(e => e.UnitStatus   ).WithMany().HasForeignKey(m => m.UnitStatusID).WillCascadeOnDelete(false);
            HasOptional(e => e.Phase        ).WithMany().HasForeignKey(m => m.PhaseID).WillCascadeOnDelete(false);

            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}