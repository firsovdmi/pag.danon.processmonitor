using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.MapConfiguration
{
    public class ApplicationSettingsSettingsMapConfiguration : EntityMapConfiguration<ApplicationSettings>
    {
        private const string TableName = "CommonSettings";

        public ApplicationSettingsSettingsMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}