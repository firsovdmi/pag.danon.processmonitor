using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.MapConfiguration
{
    public class ProcessReportItemMapConfiguration : EntityMapConfiguration<ProcessReportItem>
    {
        private const string TableName = "ProcessReportItem";

        public ProcessReportItemMapConfiguration()
        {
            //  HasOptional(e => e.Message).WithMany().HasForeignKey(m => m.MessageID).WillCascadeOnDelete(false);

         //   HasMany(e => e.SourceDestions).WithRequired().HasForeignKey(m => m).WillCascadeOnDelete(false);

            // ����������� ��� OperatorID
            //   HasOptional(e => e.PhaseID).WithMany().HasForeignKey(m => m.Phase_D).WillCascadeOnDelete(false);



            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}