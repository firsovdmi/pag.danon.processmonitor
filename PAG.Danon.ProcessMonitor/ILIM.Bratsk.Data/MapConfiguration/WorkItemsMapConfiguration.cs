using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;

namespace PAG.Danon.ProcessMonitor.Data.MapConfiguration
{
    public class WorkItemsMapConfiguration : EntityMapConfiguration<WorkItem>
    {
        private const string TableName = "WorkItem";

        public WorkItemsMapConfiguration()
        {
            HasMany(e => e.Details).WithRequired().HasForeignKey(m => m.WorkItemId).WillCascadeOnDelete(false);
            HasOptional(e => e.Phase).WithMany().HasForeignKey(m => m.PhaseID).WillCascadeOnDelete(false);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}