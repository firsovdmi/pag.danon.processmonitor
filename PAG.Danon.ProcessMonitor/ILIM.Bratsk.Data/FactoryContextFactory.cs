using System.Data.Entity.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;

namespace PAG.Danon.ProcessMonitor.Data
{
    public class FactoryContextFactory : IDbContextFactory<FactoryContext>
    {
        #region Implementation of IDbContextFactory<out FactoryContext>

        /// <summary>
        ///     Creates a new instance of a derived <see cref="T:System.Data.Entity.DbContext" /> type.
        /// </summary>
        /// <returns>
        ///     An instance of TContext
        /// </returns>
        public FactoryContext Create()
        {
            return new FactoryContext(new ActiveAccountManagerServer(), "PAG.Danon.ProcessMonitor");
        }

        #endregion
    }
}