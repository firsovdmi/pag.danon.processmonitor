﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PAG.WBD.Milk.Data.Model;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Data.Login
{
    public class LoginModel
    {
        public List<ILoginItem> Logins { get; set; }
        public List<IStaff> Staffs { get; set; }
        public List<IAcessLevel> AcessLevels { get; set; }

        private void InitStaffs(SqlConnection connection)
        {
            Staffs = new List<IStaff>();
            GetRecords(connection, "SELECT Id, Name, IsDeleted FROM Staff", (SqlDataReader reader) => Staffs.Add(new Staff
            {
                Id = reader.GetValue<int>(0),
                Name = reader.GetValue<string>(1),
                IsDeleted = reader.GetValue<bool>(2)
            }));
        }

        private void InitAcessLevels(SqlConnection connection)
        {
            AcessLevels = new List<IAcessLevel>();
            GetRecords(connection, "SELECT ID, Name, AcessLevelN FROM AcessLevel", AddAcessLevel);
        }

        private void GetRecords(SqlConnection connection, string sqlText, Action<SqlDataReader> readRecord )
        {
            var command = new SqlCommand(sqlText, connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    readRecord(reader);
                }
            }
            reader.Close();
        }

        private void AddStuff(SqlDataReader reader)
        {
            Staffs.Add(new Staff
            {
                Id = reader.GetValue<int>(0),
                Name = reader.GetValue<string>(1),
                IsDeleted = reader.GetValue<bool>(2)
            });
        }

        private void AddAcessLevel(SqlDataReader reader)
        {
            AcessLevels.Add(new AcessLevel
            {
                Id = reader.GetValue<int>(0),
                Name = reader.GetValue<string>(1),
                AcessLevelN = reader.GetValue<int>(2)
            });
        }

        public void InitLogins()
        {
            using (var connection = new SqlConnection(Settings.ConnectionString))
            {
                connection.Open();
                InitStaffs(connection);
                InitAcessLevels(connection);
                Logins = new List<ILoginItem>();
                GetRecords(connection, "SELECT  ID, Staff, UserLogin, Sha512Password, AcessLevel   FROM  Login", (SqlDataReader reader) => Logins.Add(new LoginItem
                {
                    Id = reader.GetValue<int>(0),
                    Staff= Staffs.FirstOrDefault(p=>p.Id== reader.GetValue<int>(1)),
                    UserLogin = reader.GetValue<string>(2),
                    Sha512Password = reader.GetValue<string>(3),
                    AcessLevel = AcessLevels.FirstOrDefault(p => p.Id == reader.GetValue<int>(4))
                }));
                connection.Close();
            }
        }
    }
}
