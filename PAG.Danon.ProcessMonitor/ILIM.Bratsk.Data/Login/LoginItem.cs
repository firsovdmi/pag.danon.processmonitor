﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Data.Login
{
    public class LoginItem : ILoginItem
    {
        public int Id { get; set; }
        public string UserLogin { get; set; }
        public IStaff Staff { get; set; }
        public IAcessLevel AcessLevel { get; set; }
        public string Sha512Password { get; set; }

        public string CalculateSha512(string text)
        {
            return text == null ? null : Convert.ToBase64String(new SHA512Managed().ComputeHash(Encoding.ASCII.GetBytes(text)));
        }

        public bool CheckSha512Password(string sha512Password)
        {
            return sha512Password == Sha512Password;
        }
    }

    public interface ILoginItem
    {
        int Id { get; set; }
        string UserLogin { get; set; }
        string Sha512Password { get; set; }
        IStaff Staff { get; set; }
        IAcessLevel AcessLevel { get; set; }
    }
}
