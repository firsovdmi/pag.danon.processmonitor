﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PAG.WBD.Milk.Data.Login
{
    public class AcessLevel : IAcessLevel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AcessLevelN { get; set; }
        public bool IsDeleted { get; set; }
    }

    public interface IAcessLevel
    {
        int Id { get; set; }
        string Name { get; set; }
        int AcessLevelN { get; set; }
        bool IsDeleted { get; set; }
    }
}
