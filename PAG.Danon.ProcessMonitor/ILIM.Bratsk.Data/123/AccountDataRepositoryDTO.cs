﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using PAG.Danon.ProcessMonitor.Data.123.Infrastructure;
using PAG.Danon.ProcessMonitor.Data.Repository;

namespace PAG.Danon.ProcessMonitor.Data.123
{
    public class AccountDataRepositoryDTO : RepositoryEntity<AccountData>DTO
{
    ////BCC/ BEGIN CUSTOM CODE SECTION 
    ////ECC/ END CUSTOM CODE SECTION 

}

public class AccountDataRepositoryMapper : MapperBase<AccountDataRepository, AccountDataRepositoryDTO>
{
    ////BCC/ BEGIN CUSTOM CODE SECTION 
    ////ECC/ END CUSTOM CODE SECTION 
    private RepositoryEntity<AccountData>Mapper _repositoryEntity<AccountData>Mapper = new RepositoryEntity<AccountData>Mapper();
    public override Expression<Func<AccountDataRepository, AccountDataRepositoryDTO>> SelectorExpression
    {
        get
        {
            return ((Expression<Func<AccountDataRepository, AccountDataRepositoryDTO>>)(p => new AccountDataRepositoryDTO()
            {
                ////BCC/ BEGIN CUSTOM CODE SECTION 
                ////ECC/ END CUSTOM CODE SECTION 

            })).MergeWith(this._repositoryEntity<AccountData>Mapper.SelectorExpression);
        }
    }

    public override void MapToModel(AccountDataRepositoryDTO dto, AccountDataRepository model)
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        this._repositoryEntity<AccountData>Mapper.MapToModel(dto, model);
    }
}
}
