using System;

namespace PAG.WBD.Milk.Data.EF
{
    public class DateTimeManager : IDateTimeManager
    {
        public DateTime GetDateTimeNow()
        {
            return DateTime.Now;
        }
    }
}