using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class MilkReceivingTaskMapConfiguration : EntityTypeConfiguration<MilkReceivingTask>
    {
        private const string TableName = "MilkReceivingTask";
        public MilkReceivingTaskMapConfiguration()
        {
            HasMany(e => e.MilkReceivingSubTasks).WithRequired().HasForeignKey(m => m.MilkReceivingTaskID).WillCascadeOnDelete(false);
            HasOptional(e => e.Operator).WithMany().HasForeignKey(m => m.OperatorID).WillCascadeOnDelete(false);
            HasOptional(e => e.Master).WithMany().HasForeignKey(m => m.MasterID).WillCascadeOnDelete(false);
            HasOptional(e => e.Laborant).WithMany().HasForeignKey(m => m.LaborantID).WillCascadeOnDelete(false);
            HasOptional(e => e.Driver).WithMany().HasForeignKey(m => m.DriverID).WillCascadeOnDelete(false);
            HasOptional(e => e.Contract).WithMany().HasForeignKey(m => m.ContractID).WillCascadeOnDelete(false);
            HasOptional(e => e.Manufacture).WithMany().HasForeignKey(m => m.ManufactureID).WillCascadeOnDelete(false);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}