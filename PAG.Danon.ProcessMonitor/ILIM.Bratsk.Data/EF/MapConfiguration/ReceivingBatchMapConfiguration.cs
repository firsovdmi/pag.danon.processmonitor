using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class ReceivingBatchMapConfiguration : EntityTypeConfiguration<ReceivingBatch>
    {
        private const string TableName = "ReceivingBatch";
        public ReceivingBatchMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}