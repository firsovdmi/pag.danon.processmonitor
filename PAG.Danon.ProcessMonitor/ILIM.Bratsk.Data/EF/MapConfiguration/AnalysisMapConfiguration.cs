using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class AnalysisMapConfiguration : EntityTypeConfiguration<Analysis>
    {
        private const string TableName = "Analysis";
        public AnalysisMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}