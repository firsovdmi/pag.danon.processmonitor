using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class FarmMapConfiguration : EntityTypeConfiguration<Farm>
    {
        private const string TableName = "Farm";
        public FarmMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
     
        }
    }
}