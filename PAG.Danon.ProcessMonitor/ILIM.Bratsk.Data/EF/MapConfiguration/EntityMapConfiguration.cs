using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class EntityMapConfiguration : EntityTypeConfiguration<Entity>
    {
        private const string TableName = "Entity";
        public EntityMapConfiguration()
        {
            HasKey(p => p.ID);
            Property(p => p.DateTimeCreate).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(p => p.Name).HasMaxLength(255).IsRequired();
            Property(p => p.TimeStamp).IsRowVersion().IsConcurrencyToken();
            Ignore(p => p.RowVersion);

        }
    }
}