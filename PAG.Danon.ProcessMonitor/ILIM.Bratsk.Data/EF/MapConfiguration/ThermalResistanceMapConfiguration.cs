using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class ThermalResistanceMapConfiguration : EntityTypeConfiguration<ThermalResistance>
    {
        private const string TableName = "ThermalResistance";
        public ThermalResistanceMapConfiguration()
        {
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });
        }
    }
}