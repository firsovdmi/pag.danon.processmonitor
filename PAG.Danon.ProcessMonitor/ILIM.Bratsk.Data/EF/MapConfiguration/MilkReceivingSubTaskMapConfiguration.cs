using System.Data.Entity.ModelConfiguration;
using PAG.WBD.Milk.Domain.Model;

namespace PAG.WBD.Milk.Data.EF
{
    public class MilkReceivingSubTaskMapConfiguration : EntityTypeConfiguration<MilkReceivingSubTask>
    {
        private const string TableName = "MilkReceivingSubTask";
        public MilkReceivingSubTaskMapConfiguration()
        {
            HasOptional(e => e.GroupTask).WithMany().HasForeignKey(m => m.GroupTaskID).WillCascadeOnDelete(false);
            HasOptional(e => e.ShippedAnalysis).WithMany().HasForeignKey(m => m.ShippedAnalysisID).WillCascadeOnDelete(false);
            HasOptional(e => e.ReceivingAnalysis).WithMany().HasForeignKey(m => m.ReceivingAnalysisID).WillCascadeOnDelete(false);
            HasMany(e => e.ReceivingBatches).WithRequired().HasForeignKey(m => m.MilkReceivingSubTaskID).WillCascadeOnDelete(false);
            Map(p =>
            {
                p.MapInheritedProperties();
                p.ToTable(TableName);
            });

        }
    }
}