using System.Linq;
using PAG.WBD.Milk.Domain.Repository;

namespace PAG.WBD.Milk.Data.EF
{
    public interface IUnitOfWorkDO : IUnitOfWork
    {
        IQueryable<T> Get<T>() where T : class;
        T Create<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;

    }
}