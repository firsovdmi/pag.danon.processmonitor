namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class somethingchanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MyPLCBuffers", "Step_No", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MyPLCBuffers", "Step_No", c => c.Int(nullable: false));
        }
    }
}
