namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeVersion1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CipDetail",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        PlcDateTime = c.DateTime(nullable: false),
                        PlcIWorkID = c.Int(nullable: false),
                        CipItemId = c.Guid(),
                        MessageID = c.Guid(),
                        EventInfoID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        OperatedID = c.Int(nullable: false),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        RetTEMPMVLye = c.Int(nullable: false),
                        RetCONCMVLye = c.Int(nullable: false),
                        RetTEMPMVAcid = c.Int(nullable: false),
                        RetCONCMVAcid = c.Int(nullable: false),
                        RetTEMPMVHW = c.Int(nullable: false),
                        RetCONCMV3 = c.Int(nullable: false),
                        ALCIPPrNo = c.Int(nullable: false),
                        StepTimeVolumePreset = c.Int(nullable: false),
                        StepTimeVolumeRun = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CipItem", t => t.CipItemId, cascadeDelete: true)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .Index(t => t.PhaseID)
                .Index(t => t.CipItemId)
                .Index(t => t.MessageID)
                .Index(t => t.EventInfoID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID);
            
            CreateTable(
                "dbo.CipItem",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        PlcItemID = c.Int(nullable: false),
                        ParentItemID = c.Guid(),
                        PlcParentItemID = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        PlcDateTime = c.DateTime(nullable: false),
                        IsAutoCreate = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CipItem", t => t.ParentItemID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .Index(t => t.PhaseID)
                .Index(t => t.ParentItemID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CipDetail", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.CipDetail", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.CipDetail", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.CipDetail", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.CipDetail", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.CipDetail", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.CipDetail", "CipItemId", "dbo.CipItem");
            DropForeignKey("dbo.CipItem", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.CipItem", "ParentItemID", "dbo.CipItem");
            DropIndex("dbo.CipItem", new[] { "ParentItemID" });
            DropIndex("dbo.CipItem", new[] { "PhaseID" });
            DropIndex("dbo.CipDetail", new[] { "UnitStatusID" });
            DropIndex("dbo.CipDetail", new[] { "StepNoProdID" });
            DropIndex("dbo.CipDetail", new[] { "PhaseStatusID" });
            DropIndex("dbo.CipDetail", new[] { "EventInfoID" });
            DropIndex("dbo.CipDetail", new[] { "MessageID" });
            DropIndex("dbo.CipDetail", new[] { "CipItemId" });
            DropIndex("dbo.CipDetail", new[] { "PhaseID" });
            DropTable("dbo.CipItem");
            DropTable("dbo.CipDetail");
        }
    }
}
