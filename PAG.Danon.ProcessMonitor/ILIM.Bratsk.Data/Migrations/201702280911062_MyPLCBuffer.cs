namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyPLCBuffer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MyPLCBuffers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Status = c.Int(nullable: false),
                        PlcIp = c.Int(nullable: false),
                        PhaseID = c.Guid(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        WorkMessage = c.Guid(nullable: false),
                        PhaseStatus = c.Guid(nullable: false),
                        EventInfo = c.Guid(nullable: false),
                        OperatedID = c.Int(nullable: false),
                        Step_No = c.Int(nullable: false),
                        UnitStatus = c.Guid(nullable: false),
                        Bool_0 = c.Boolean(nullable: false),
                        Bool_1 = c.Boolean(nullable: false),
                        Bool_2 = c.Boolean(nullable: false),
                        Bool_3 = c.Boolean(nullable: false),
                        Bool_4 = c.Boolean(nullable: false),
                        Bool_5 = c.Boolean(nullable: false),
                        Bool_6 = c.Boolean(nullable: false),
                        Bool_7 = c.Boolean(nullable: false),
                        Byte_1 = c.Int(nullable: false),
                        Int_1 = c.Int(nullable: false),
                        Int_2 = c.Int(nullable: false),
                        Int_3 = c.Int(nullable: false),
                        Int_4 = c.Int(nullable: false),
                        Int_5 = c.Int(nullable: false),
                        Int_6 = c.Int(nullable: false),
                        Int_7 = c.Int(nullable: false),
                        Int_8 = c.Int(nullable: false),
                        Int_9 = c.Int(nullable: false),
                        Dint_1 = c.Int(nullable: false),
                        Real_1 = c.Single(nullable: false),
                        Real_2 = c.Single(nullable: false),
                        RecordDate = c.DateTime(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MyPLCBuffers");
        }
    }
}
