// <auto-generated />
namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Init8 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Init8));
        
        string IMigrationMetadata.Id
        {
            get { return "201704101313504_Init8"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
