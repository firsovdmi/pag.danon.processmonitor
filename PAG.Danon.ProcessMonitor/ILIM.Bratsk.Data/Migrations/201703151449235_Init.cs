namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountDatas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        DataID = c.String(),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EventInfoes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MyPLCBuffers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        WorkMessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        MaterialID = c.Int(nullable: false),
                        TargetAmount = c.Single(nullable: false),
                        ActualAmount = c.Single(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        RecordDate = c.DateTime(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        MyPLCBuffer_ID = c.Guid(),
                        StepNoProd_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.MyPLCBuffers", t => t.MyPLCBuffer_ID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProd_ID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .ForeignKey("dbo.WorkMessages", t => t.WorkMessageID)
                .Index(t => t.PhaseID)
                .Index(t => t.WorkMessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.MyPLCBuffer_ID)
                .Index(t => t.StepNoProd_ID);
            
            CreateTable(
                "dbo.Operators",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Phases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PhaseStatus",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StepNoProds",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UnitStatus",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WorkMessages",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PlcBuffers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Status = c.Int(nullable: false),
                        PlcIp = c.Int(nullable: false),
                        PhaseID = c.Int(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        PlcID = c.Int(nullable: false),
                        PhaseStatus = c.Int(nullable: false),
                        EventInfo = c.Int(nullable: false),
                        OperatorID = c.Int(nullable: false),
                        Step_No = c.Int(nullable: false),
                        UnitStatus = c.Int(nullable: false),
                        Bool_0 = c.Boolean(nullable: false),
                        Bool_1 = c.Boolean(nullable: false),
                        Bool_2 = c.Boolean(nullable: false),
                        Bool_3 = c.Boolean(nullable: false),
                        Bool_4 = c.Boolean(nullable: false),
                        Bool_5 = c.Boolean(nullable: false),
                        Bool_6 = c.Boolean(nullable: false),
                        Bool_7 = c.Boolean(nullable: false),
                        Byte_1 = c.Int(nullable: false),
                        Int_1 = c.Int(nullable: false),
                        Int_2 = c.Int(nullable: false),
                        Int_3 = c.Int(nullable: false),
                        Int_4 = c.Int(nullable: false),
                        Int_5 = c.Int(nullable: false),
                        Int_6 = c.Int(nullable: false),
                        Int_7 = c.Int(nullable: false),
                        Int_8 = c.Int(nullable: false),
                        Int_9 = c.Int(nullable: false),
                        Dint_1 = c.Int(nullable: false),
                        Real_1 = c.Single(nullable: false),
                        Real_2 = c.Single(nullable: false),
                        RecordDate = c.DateTime(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Report",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        RequestType = c.Int(nullable: false),
                        ReportAssembly = c.Binary(),
                        ReportTemplate = c.Binary(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserReportRequestParameter",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserID = c.String(),
                        RequestParameters = c.String(storeType: "xml"),
                        ReportID = c.Guid(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Report", t => t.ReportID, cascadeDelete: true)
                .Index(t => t.ReportID);
            
            CreateTable(
                "dbo.CommonSettings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SystemName = c.String(),
                        UserName = c.String(),
                        Value = c.String(),
                        Description = c.String(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StepNoCIPs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StepNoLactas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StepNoOVATs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WorkDetail",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        PlcDateTime = c.DateTime(nullable: false),
                        PlcIWorkID = c.Int(nullable: false),
                        WorkItemId = c.Guid(nullable: false),
                        MessageID = c.Guid(),
                        EventInfoID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        OperatedID = c.Int(nullable: false),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        MaterialID = c.Int(nullable: false),
                        TargetAmount = c.Single(nullable: false),
                        ActualAmount = c.Single(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .ForeignKey("dbo.WorkItem", t => t.WorkItemId)
                .Index(t => t.PhaseID)
                .Index(t => t.WorkItemId)
                .Index(t => t.MessageID)
                .Index(t => t.EventInfoID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID);
            
            CreateTable(
                "dbo.WorkItem",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        PlcItemID = c.Int(nullable: false),
                        ParentItemID = c.Guid(),
                        PlcParentItemID = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        PlcDateTime = c.DateTime(nullable: false),
                        MaxPhaseTime = c.DateTime(nullable: false),
                        IsAutoCreate = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.WorkItem", t => t.ParentItemID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .Index(t => t.PhaseID)
                .Index(t => t.ParentItemID);
            
            CreateTable(
                "dbo.CipDetail",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        PlcDateTime = c.DateTime(nullable: false),
                        PlcIWorkID = c.Int(nullable: false),
                        CipItemId = c.Guid(),
                        MessageID = c.Guid(),
                        EventInfoID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        OperatedID = c.Int(nullable: false),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        RetTEMPMVLye = c.Int(nullable: false),
                        RetCONCMVLye = c.Int(nullable: false),
                        RetTEMPMVAcid = c.Int(nullable: false),
                        RetCONCMVAcid = c.Int(nullable: false),
                        RetTEMPMVHW = c.Int(nullable: false),
                        RetCONCMV3 = c.Int(nullable: false),
                        ALCIPPrNo = c.Int(nullable: false),
                        StepTimeVolumePreset = c.Int(nullable: false),
                        StepTimeVolumeRun = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CipItem", t => t.CipItemId, cascadeDelete: true)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .Index(t => t.PhaseID)
                .Index(t => t.CipItemId)
                .Index(t => t.MessageID)
                .Index(t => t.EventInfoID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID);
            
            CreateTable(
                "dbo.CipItem",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        PlcItemID = c.Int(nullable: false),
                        ParentItemID = c.Guid(),
                        PlcParentItemID = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        PlcDateTime = c.DateTime(nullable: false),
                        IsAutoCreate = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CipItem", t => t.ParentItemID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .Index(t => t.PhaseID)
                .Index(t => t.ParentItemID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CipDetail", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.CipDetail", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.CipDetail", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.CipDetail", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.CipDetail", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.CipDetail", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.CipDetail", "CipItemId", "dbo.CipItem");
            DropForeignKey("dbo.CipItem", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.CipItem", "ParentItemID", "dbo.CipItem");
            DropForeignKey("dbo.WorkItem", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.WorkItem", "ParentItemID", "dbo.WorkItem");
            DropForeignKey("dbo.WorkDetail", "WorkItemId", "dbo.WorkItem");
            DropForeignKey("dbo.WorkDetail", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.WorkDetail", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.WorkDetail", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.WorkDetail", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.WorkDetail", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.WorkDetail", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.UserReportRequestParameter", "ReportID", "dbo.Report");
            DropForeignKey("dbo.MyPLCBuffers", "WorkMessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.MyPLCBuffers", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.MyPLCBuffers", "StepNoProd_ID", "dbo.StepNoProds");
            DropForeignKey("dbo.MyPLCBuffers", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.MyPLCBuffers", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.MyPLCBuffers", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.MyPLCBuffers", "MyPLCBuffer_ID", "dbo.MyPLCBuffers");
            DropForeignKey("dbo.MyPLCBuffers", "EventInfoID", "dbo.EventInfoes");
            DropIndex("dbo.CipItem", new[] { "ParentItemID" });
            DropIndex("dbo.CipItem", new[] { "PhaseID" });
            DropIndex("dbo.CipDetail", new[] { "UnitStatusID" });
            DropIndex("dbo.CipDetail", new[] { "StepNoProdID" });
            DropIndex("dbo.CipDetail", new[] { "PhaseStatusID" });
            DropIndex("dbo.CipDetail", new[] { "EventInfoID" });
            DropIndex("dbo.CipDetail", new[] { "MessageID" });
            DropIndex("dbo.CipDetail", new[] { "CipItemId" });
            DropIndex("dbo.CipDetail", new[] { "PhaseID" });
            DropIndex("dbo.WorkItem", new[] { "ParentItemID" });
            DropIndex("dbo.WorkItem", new[] { "PhaseID" });
            DropIndex("dbo.WorkDetail", new[] { "UnitStatusID" });
            DropIndex("dbo.WorkDetail", new[] { "StepNoProdID" });
            DropIndex("dbo.WorkDetail", new[] { "PhaseStatusID" });
            DropIndex("dbo.WorkDetail", new[] { "EventInfoID" });
            DropIndex("dbo.WorkDetail", new[] { "MessageID" });
            DropIndex("dbo.WorkDetail", new[] { "WorkItemId" });
            DropIndex("dbo.WorkDetail", new[] { "PhaseID" });
            DropIndex("dbo.UserReportRequestParameter", new[] { "ReportID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "StepNoProd_ID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "MyPLCBuffer_ID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "UnitStatusID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "OperatorID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "EventInfoID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "PhaseStatusID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "WorkMessageID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "PhaseID" });
            DropTable("dbo.CipItem");
            DropTable("dbo.CipDetail");
            DropTable("dbo.WorkItem");
            DropTable("dbo.WorkDetail");
            DropTable("dbo.StepNoOVATs");
            DropTable("dbo.StepNoLactas");
            DropTable("dbo.StepNoCIPs");
            DropTable("dbo.CommonSettings");
            DropTable("dbo.UserReportRequestParameter");
            DropTable("dbo.Report");
            DropTable("dbo.PlcBuffers");
            DropTable("dbo.WorkMessages");
            DropTable("dbo.UnitStatus");
            DropTable("dbo.StepNoProds");
            DropTable("dbo.PhaseStatus");
            DropTable("dbo.Phases");
            DropTable("dbo.Operators");
            DropTable("dbo.MyPLCBuffers");
            DropTable("dbo.EventInfoes");
            DropTable("dbo.AccountDatas");
        }
    }
}
