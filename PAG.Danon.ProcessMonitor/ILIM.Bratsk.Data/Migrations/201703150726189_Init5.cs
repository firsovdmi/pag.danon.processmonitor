namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MyPLCBuffers", "XFerIn_ID", "dbo.Phases");
            DropForeignKey("dbo.MyPLCBuffers", "XFerOut_ID", "dbo.Phases");
            DropIndex("dbo.MyPLCBuffers", new[] { "XFerIn_ID" });
            DropIndex("dbo.MyPLCBuffers", new[] { "XFerOut_ID" });
            AddColumn("dbo.MyPLCBuffers", "MyPLCBuffer_ID", c => c.Guid());
            CreateIndex("dbo.MyPLCBuffers", "MyPLCBuffer_ID");
            AddForeignKey("dbo.MyPLCBuffers", "MyPLCBuffer_ID", "dbo.MyPLCBuffers", "ID");
            DropColumn("dbo.MyPLCBuffers", "XFerIn_ID");
            DropColumn("dbo.MyPLCBuffers", "XFerOut_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MyPLCBuffers", "XFerOut_ID", c => c.Guid());
            AddColumn("dbo.MyPLCBuffers", "XFerIn_ID", c => c.Guid());
            DropForeignKey("dbo.MyPLCBuffers", "MyPLCBuffer_ID", "dbo.MyPLCBuffers");
            DropIndex("dbo.MyPLCBuffers", new[] { "MyPLCBuffer_ID" });
            DropColumn("dbo.MyPLCBuffers", "MyPLCBuffer_ID");
            CreateIndex("dbo.MyPLCBuffers", "XFerOut_ID");
            CreateIndex("dbo.MyPLCBuffers", "XFerIn_ID");
            AddForeignKey("dbo.MyPLCBuffers", "XFerOut_ID", "dbo.Phases", "ID");
            AddForeignKey("dbo.MyPLCBuffers", "XFerIn_ID", "dbo.Phases", "ID");
        }
    }
}
