namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init9 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CipDetail", "CipConturID", c => c.Guid());
            AddColumn("dbo.CipDetail", "CipDetail_ID", c => c.Guid());
            CreateIndex("dbo.CipDetail", "CipConturID");
            CreateIndex("dbo.CipDetail", "CipDetail_ID");
            AddForeignKey("dbo.CipDetail", "CipConturID", "dbo.CipConturs", "ID");
            AddForeignKey("dbo.CipDetail", "CipDetail_ID", "dbo.CipDetail", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CipDetail", "CipDetail_ID", "dbo.CipDetail");
            DropForeignKey("dbo.CipDetail", "CipConturID", "dbo.CipConturs");
            DropIndex("dbo.CipDetail", new[] { "CipDetail_ID" });
            DropIndex("dbo.CipDetail", new[] { "CipConturID" });
            DropColumn("dbo.CipDetail", "CipDetail_ID");
            DropColumn("dbo.CipDetail", "CipConturID");
        }
    }
}
