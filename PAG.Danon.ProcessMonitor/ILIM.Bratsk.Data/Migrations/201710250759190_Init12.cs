namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init12 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppCIPDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkID = c.Int(nullable: false),
                        MessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoCIPID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        RetTEMPMVLye = c.Int(nullable: false),
                        RetCONCMVLye = c.Int(nullable: false),
                        RetTEMPMVAcid = c.Int(nullable: false),
                        RetCONCMVAcid = c.Int(nullable: false),
                        RetTEMPMVHW = c.Int(nullable: false),
                        RetCONCMV3 = c.Int(nullable: false),
                        ALCIPPrNoID = c.Guid(),
                        StepTimeVolumePreset = c.Int(nullable: false),
                        StepTimeVolumeRun = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        CipConturID = c.Guid(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        AppCipDetail_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProgrammNumbers", t => t.ALCIPPrNoID)
                .ForeignKey("dbo.CipConturs", t => t.CipConturID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.AppCIPDetails", t => t.AppCipDetail_ID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.AppCIPPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoCips", t => t.StepNoCIPID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .Index(t => t.PhaseID)
                .Index(t => t.MessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoCIPID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.ALCIPPrNoID)
                .Index(t => t.CipConturID)
                .Index(t => t.AppCipDetail_ID);
            
            CreateTable(
                "dbo.ProgrammNumbers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Ip = c.Int(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AppCIPPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AppDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkMessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        MaterialID = c.Int(nullable: false),
                        TargetAmount = c.Single(nullable: false),
                        ActualAmount = c.Single(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        AppDetails_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.AppDetails", t => t.AppDetails_ID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.AppPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .ForeignKey("dbo.WorkMessages", t => t.WorkMessageID)
                .Index(t => t.PhaseID)
                .Index(t => t.WorkMessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.AppDetails_ID);
            
            CreateTable(
                "dbo.AppPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        Description = c.String(),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CheeseCIPDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkID = c.Int(nullable: false),
                        MessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoCIPID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        RetTEMPMVLye = c.Int(nullable: false),
                        RetCONCMVLye = c.Int(nullable: false),
                        RetTEMPMVAcid = c.Int(nullable: false),
                        RetCONCMVAcid = c.Int(nullable: false),
                        RetTEMPMVHW = c.Int(nullable: false),
                        RetCONCMV3 = c.Int(nullable: false),
                        ALCIPPrNoID = c.Guid(),
                        StepTimeVolumePreset = c.Int(nullable: false),
                        StepTimeVolumeRun = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        CipConturID = c.Guid(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        CheeseCipDetail_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProgrammNumbers", t => t.ALCIPPrNoID)
                .ForeignKey("dbo.CipConturs", t => t.CipConturID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.CheeseCIPDetails", t => t.CheeseCipDetail_ID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.CheeseCIPPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoCips", t => t.StepNoCIPID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .Index(t => t.PhaseID)
                .Index(t => t.MessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoCIPID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.ALCIPPrNoID)
                .Index(t => t.CipConturID)
                .Index(t => t.CheeseCipDetail_ID);
            
            CreateTable(
                "dbo.CheeseCIPPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CheesenDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkMessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        MaterialID = c.Int(nullable: false),
                        TargetAmount = c.Single(nullable: false),
                        ActualAmount = c.Single(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        CheesenDetails_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.CheesenDetails", t => t.CheesenDetails_ID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.CheesePhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .ForeignKey("dbo.WorkMessages", t => t.WorkMessageID)
                .Index(t => t.PhaseID)
                .Index(t => t.WorkMessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.CheesenDetails_ID);
            
            CreateTable(
                "dbo.CheesePhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        Description = c.String(),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MXOCIPDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkID = c.Int(nullable: false),
                        MessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoCIPID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        RetTEMPMVLye = c.Int(nullable: false),
                        RetCONCMVLye = c.Int(nullable: false),
                        RetTEMPMVAcid = c.Int(nullable: false),
                        RetCONCMVAcid = c.Int(nullable: false),
                        RetTEMPMVHW = c.Int(nullable: false),
                        RetCONCMV3 = c.Int(nullable: false),
                        ALCIPPrNoID = c.Guid(),
                        StepTimeVolumePreset = c.Int(nullable: false),
                        StepTimeVolumeRun = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        CipConturID = c.Guid(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        MXOCipDetail_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProgrammNumbers", t => t.ALCIPPrNoID)
                .ForeignKey("dbo.CipConturs", t => t.CipConturID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.MXOCIPDetails", t => t.MXOCipDetail_ID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.MXOCIPPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoCips", t => t.StepNoCIPID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .Index(t => t.PhaseID)
                .Index(t => t.MessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoCIPID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.ALCIPPrNoID)
                .Index(t => t.CipConturID)
                .Index(t => t.MXOCipDetail_ID);
            
            CreateTable(
                "dbo.MXOCIPPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.MXODetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkMessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        MaterialID = c.Int(nullable: false),
                        TargetAmount = c.Single(nullable: false),
                        ActualAmount = c.Single(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        MXODetails_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.MXODetails", t => t.MXODetails_ID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.MXOPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .ForeignKey("dbo.WorkMessages", t => t.WorkMessageID)
                .Index(t => t.PhaseID)
                .Index(t => t.WorkMessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.MXODetails_ID);
            
            CreateTable(
                "dbo.MXOPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        Description = c.String(),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PhasovkaCIPDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkID = c.Int(nullable: false),
                        MessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoCIPID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        RetTEMPMVLye = c.Int(nullable: false),
                        RetCONCMVLye = c.Int(nullable: false),
                        RetTEMPMVAcid = c.Int(nullable: false),
                        RetCONCMVAcid = c.Int(nullable: false),
                        RetTEMPMVHW = c.Int(nullable: false),
                        RetCONCMV3 = c.Int(nullable: false),
                        ALCIPPrNoID = c.Guid(),
                        StepTimeVolumePreset = c.Int(nullable: false),
                        StepTimeVolumeRun = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        CipConturID = c.Guid(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        PhasovkaCipDetails_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProgrammNumbers", t => t.ALCIPPrNoID)
                .ForeignKey("dbo.CipConturs", t => t.CipConturID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.PhasovkaCIPDetails", t => t.PhasovkaCipDetails_ID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.PhasovkaCIPPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoCips", t => t.StepNoCIPID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .Index(t => t.PhaseID)
                .Index(t => t.MessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoCIPID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.ALCIPPrNoID)
                .Index(t => t.CipConturID)
                .Index(t => t.PhasovkaCipDetails_ID);
            
            CreateTable(
                "dbo.PhasovkaCIPPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        Description = c.String(),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PhasovkaDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkMessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        MaterialID = c.Int(nullable: false),
                        TargetAmount = c.Single(nullable: false),
                        ActualAmount = c.Single(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        PhasovkaDetails_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.PhasovkaDetails", t => t.PhasovkaDetails_ID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.PhasovkaPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .ForeignKey("dbo.WorkMessages", t => t.WorkMessageID)
                .Index(t => t.PhaseID)
                .Index(t => t.WorkMessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.PhasovkaDetails_ID);
            
            CreateTable(
                "dbo.PhasovkaPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        Description = c.String(),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ReceptionCIPDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkID = c.Int(nullable: false),
                        MessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoCIPID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        RetTEMPMVLye = c.Int(nullable: false),
                        RetCONCMVLye = c.Int(nullable: false),
                        RetTEMPMVAcid = c.Int(nullable: false),
                        RetCONCMVAcid = c.Int(nullable: false),
                        RetTEMPMVHW = c.Int(nullable: false),
                        RetCONCMV3 = c.Int(nullable: false),
                        ALCIPPrNoID = c.Guid(),
                        StepTimeVolumePreset = c.Int(nullable: false),
                        StepTimeVolumeRun = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        CipConturID = c.Guid(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        ReceptionCipDetail_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProgrammNumbers", t => t.ALCIPPrNoID)
                .ForeignKey("dbo.CipConturs", t => t.CipConturID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.ReceptionCIPDetails", t => t.ReceptionCipDetail_ID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.ReceptionCIPPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoCips", t => t.StepNoCIPID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .Index(t => t.PhaseID)
                .Index(t => t.MessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoCIPID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.ALCIPPrNoID)
                .Index(t => t.CipConturID)
                .Index(t => t.ReceptionCipDetail_ID);
            
            CreateTable(
                "dbo.ReceptionCIPPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ReceptionDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        IP = c.Int(nullable: false),
                        PhaseID = c.Guid(),
                        WorkMessageID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        EventInfoID = c.Guid(),
                        OperatorID = c.Guid(),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        MaterialID = c.Int(nullable: false),
                        TargetAmount = c.Single(nullable: false),
                        ActualAmount = c.Single(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        RecordDate = c.DateTime(),
                        RecordDateStart = c.DateTime(),
                        RecordDateEnd = c.DateTime(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                        ReceptionDetails_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.ReceptionDetails", t => t.ReceptionDetails_ID)
                .ForeignKey("dbo.Operators", t => t.OperatorID)
                .ForeignKey("dbo.ReceptionPhases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .ForeignKey("dbo.WorkMessages", t => t.WorkMessageID)
                .Index(t => t.PhaseID)
                .Index(t => t.WorkMessageID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.EventInfoID)
                .Index(t => t.OperatorID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID)
                .Index(t => t.ReceptionDetails_ID);
            
            CreateTable(
                "dbo.ReceptionPhases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Ip = c.Int(nullable: false),
                        Description = c.String(),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReceptionDetails", "WorkMessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.ReceptionDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.ReceptionDetails", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.ReceptionDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.ReceptionDetails", "PhaseID", "dbo.ReceptionPhases");
            DropForeignKey("dbo.ReceptionDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.ReceptionDetails", "ReceptionDetails_ID", "dbo.ReceptionDetails");
            DropForeignKey("dbo.ReceptionDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.ReceptionCIPDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.ReceptionCIPDetails", "StepNoCIPID", "dbo.StepNoCips");
            DropForeignKey("dbo.ReceptionCIPDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.ReceptionCIPDetails", "PhaseID", "dbo.ReceptionCIPPhases");
            DropForeignKey("dbo.ReceptionCIPDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.ReceptionCIPDetails", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.ReceptionCIPDetails", "ReceptionCipDetail_ID", "dbo.ReceptionCIPDetails");
            DropForeignKey("dbo.ReceptionCIPDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.ReceptionCIPDetails", "CipConturID", "dbo.CipConturs");
            DropForeignKey("dbo.ReceptionCIPDetails", "ALCIPPrNoID", "dbo.ProgrammNumbers");
            DropForeignKey("dbo.PhasovkaDetails", "WorkMessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.PhasovkaDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.PhasovkaDetails", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.PhasovkaDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.PhasovkaDetails", "PhaseID", "dbo.PhasovkaPhases");
            DropForeignKey("dbo.PhasovkaDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.PhasovkaDetails", "PhasovkaDetails_ID", "dbo.PhasovkaDetails");
            DropForeignKey("dbo.PhasovkaDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.PhasovkaCIPDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.PhasovkaCIPDetails", "StepNoCIPID", "dbo.StepNoCips");
            DropForeignKey("dbo.PhasovkaCIPDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.PhasovkaCIPDetails", "PhaseID", "dbo.PhasovkaCIPPhases");
            DropForeignKey("dbo.PhasovkaCIPDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.PhasovkaCIPDetails", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.PhasovkaCIPDetails", "PhasovkaCipDetails_ID", "dbo.PhasovkaCIPDetails");
            DropForeignKey("dbo.PhasovkaCIPDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.PhasovkaCIPDetails", "CipConturID", "dbo.CipConturs");
            DropForeignKey("dbo.PhasovkaCIPDetails", "ALCIPPrNoID", "dbo.ProgrammNumbers");
            DropForeignKey("dbo.MXODetails", "WorkMessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.MXODetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.MXODetails", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.MXODetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.MXODetails", "PhaseID", "dbo.MXOPhases");
            DropForeignKey("dbo.MXODetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.MXODetails", "MXODetails_ID", "dbo.MXODetails");
            DropForeignKey("dbo.MXODetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.MXOCIPDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.MXOCIPDetails", "StepNoCIPID", "dbo.StepNoCips");
            DropForeignKey("dbo.MXOCIPDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.MXOCIPDetails", "PhaseID", "dbo.MXOCIPPhases");
            DropForeignKey("dbo.MXOCIPDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.MXOCIPDetails", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.MXOCIPDetails", "MXOCipDetail_ID", "dbo.MXOCIPDetails");
            DropForeignKey("dbo.MXOCIPDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.MXOCIPDetails", "CipConturID", "dbo.CipConturs");
            DropForeignKey("dbo.MXOCIPDetails", "ALCIPPrNoID", "dbo.ProgrammNumbers");
            DropForeignKey("dbo.CheesenDetails", "WorkMessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.CheesenDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.CheesenDetails", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.CheesenDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.CheesenDetails", "PhaseID", "dbo.CheesePhases");
            DropForeignKey("dbo.CheesenDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.CheesenDetails", "CheesenDetails_ID", "dbo.CheesenDetails");
            DropForeignKey("dbo.CheesenDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.CheeseCIPDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.CheeseCIPDetails", "StepNoCIPID", "dbo.StepNoCips");
            DropForeignKey("dbo.CheeseCIPDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.CheeseCIPDetails", "PhaseID", "dbo.CheeseCIPPhases");
            DropForeignKey("dbo.CheeseCIPDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.CheeseCIPDetails", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.CheeseCIPDetails", "CheeseCipDetail_ID", "dbo.CheeseCIPDetails");
            DropForeignKey("dbo.CheeseCIPDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.CheeseCIPDetails", "CipConturID", "dbo.CipConturs");
            DropForeignKey("dbo.CheeseCIPDetails", "ALCIPPrNoID", "dbo.ProgrammNumbers");
            DropForeignKey("dbo.AppDetails", "WorkMessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.AppDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.AppDetails", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.AppDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.AppDetails", "PhaseID", "dbo.AppPhases");
            DropForeignKey("dbo.AppDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.AppDetails", "AppDetails_ID", "dbo.AppDetails");
            DropForeignKey("dbo.AppDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.AppCIPDetails", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.AppCIPDetails", "StepNoCIPID", "dbo.StepNoCips");
            DropForeignKey("dbo.AppCIPDetails", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.AppCIPDetails", "PhaseID", "dbo.AppCIPPhases");
            DropForeignKey("dbo.AppCIPDetails", "OperatorID", "dbo.Operators");
            DropForeignKey("dbo.AppCIPDetails", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.AppCIPDetails", "AppCipDetail_ID", "dbo.AppCIPDetails");
            DropForeignKey("dbo.AppCIPDetails", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.AppCIPDetails", "CipConturID", "dbo.CipConturs");
            DropForeignKey("dbo.AppCIPDetails", "ALCIPPrNoID", "dbo.ProgrammNumbers");
            DropIndex("dbo.ReceptionDetails", new[] { "ReceptionDetails_ID" });
            DropIndex("dbo.ReceptionDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.ReceptionDetails", new[] { "StepNoProdID" });
            DropIndex("dbo.ReceptionDetails", new[] { "OperatorID" });
            DropIndex("dbo.ReceptionDetails", new[] { "EventInfoID" });
            DropIndex("dbo.ReceptionDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.ReceptionDetails", new[] { "WorkMessageID" });
            DropIndex("dbo.ReceptionDetails", new[] { "PhaseID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "ReceptionCipDetail_ID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "CipConturID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "ALCIPPrNoID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "StepNoCIPID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "OperatorID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "EventInfoID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "MessageID" });
            DropIndex("dbo.ReceptionCIPDetails", new[] { "PhaseID" });
            DropIndex("dbo.PhasovkaDetails", new[] { "PhasovkaDetails_ID" });
            DropIndex("dbo.PhasovkaDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.PhasovkaDetails", new[] { "StepNoProdID" });
            DropIndex("dbo.PhasovkaDetails", new[] { "OperatorID" });
            DropIndex("dbo.PhasovkaDetails", new[] { "EventInfoID" });
            DropIndex("dbo.PhasovkaDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.PhasovkaDetails", new[] { "WorkMessageID" });
            DropIndex("dbo.PhasovkaDetails", new[] { "PhaseID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "PhasovkaCipDetails_ID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "CipConturID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "ALCIPPrNoID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "StepNoCIPID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "OperatorID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "EventInfoID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "MessageID" });
            DropIndex("dbo.PhasovkaCIPDetails", new[] { "PhaseID" });
            DropIndex("dbo.MXODetails", new[] { "MXODetails_ID" });
            DropIndex("dbo.MXODetails", new[] { "UnitStatusID" });
            DropIndex("dbo.MXODetails", new[] { "StepNoProdID" });
            DropIndex("dbo.MXODetails", new[] { "OperatorID" });
            DropIndex("dbo.MXODetails", new[] { "EventInfoID" });
            DropIndex("dbo.MXODetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.MXODetails", new[] { "WorkMessageID" });
            DropIndex("dbo.MXODetails", new[] { "PhaseID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "MXOCipDetail_ID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "CipConturID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "ALCIPPrNoID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "StepNoCIPID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "OperatorID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "EventInfoID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "MessageID" });
            DropIndex("dbo.MXOCIPDetails", new[] { "PhaseID" });
            DropIndex("dbo.CheesenDetails", new[] { "CheesenDetails_ID" });
            DropIndex("dbo.CheesenDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.CheesenDetails", new[] { "StepNoProdID" });
            DropIndex("dbo.CheesenDetails", new[] { "OperatorID" });
            DropIndex("dbo.CheesenDetails", new[] { "EventInfoID" });
            DropIndex("dbo.CheesenDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.CheesenDetails", new[] { "WorkMessageID" });
            DropIndex("dbo.CheesenDetails", new[] { "PhaseID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "CheeseCipDetail_ID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "CipConturID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "ALCIPPrNoID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "StepNoCIPID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "OperatorID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "EventInfoID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "MessageID" });
            DropIndex("dbo.CheeseCIPDetails", new[] { "PhaseID" });
            DropIndex("dbo.AppDetails", new[] { "AppDetails_ID" });
            DropIndex("dbo.AppDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.AppDetails", new[] { "StepNoProdID" });
            DropIndex("dbo.AppDetails", new[] { "OperatorID" });
            DropIndex("dbo.AppDetails", new[] { "EventInfoID" });
            DropIndex("dbo.AppDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.AppDetails", new[] { "WorkMessageID" });
            DropIndex("dbo.AppDetails", new[] { "PhaseID" });
            DropIndex("dbo.AppCIPDetails", new[] { "AppCipDetail_ID" });
            DropIndex("dbo.AppCIPDetails", new[] { "CipConturID" });
            DropIndex("dbo.AppCIPDetails", new[] { "ALCIPPrNoID" });
            DropIndex("dbo.AppCIPDetails", new[] { "UnitStatusID" });
            DropIndex("dbo.AppCIPDetails", new[] { "StepNoCIPID" });
            DropIndex("dbo.AppCIPDetails", new[] { "OperatorID" });
            DropIndex("dbo.AppCIPDetails", new[] { "EventInfoID" });
            DropIndex("dbo.AppCIPDetails", new[] { "PhaseStatusID" });
            DropIndex("dbo.AppCIPDetails", new[] { "MessageID" });
            DropIndex("dbo.AppCIPDetails", new[] { "PhaseID" });
            DropTable("dbo.ReceptionPhases");
            DropTable("dbo.ReceptionDetails");
            DropTable("dbo.ReceptionCIPPhases");
            DropTable("dbo.ReceptionCIPDetails");
            DropTable("dbo.PhasovkaPhases");
            DropTable("dbo.PhasovkaDetails");
            DropTable("dbo.PhasovkaCIPPhases");
            DropTable("dbo.PhasovkaCIPDetails");
            DropTable("dbo.MXOPhases");
            DropTable("dbo.MXODetails");
            DropTable("dbo.MXOCIPPhases");
            DropTable("dbo.MXOCIPDetails");
            DropTable("dbo.CheesePhases");
            DropTable("dbo.CheesenDetails");
            DropTable("dbo.CheeseCIPPhases");
            DropTable("dbo.CheeseCIPDetails");
            DropTable("dbo.AppPhases");
            DropTable("dbo.AppDetails");
            DropTable("dbo.AppCIPPhases");
            DropTable("dbo.ProgrammNumbers");
            DropTable("dbo.AppCIPDetails");
        }
    }
}
