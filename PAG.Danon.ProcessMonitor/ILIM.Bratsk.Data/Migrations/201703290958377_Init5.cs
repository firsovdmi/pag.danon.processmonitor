namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PhaseStatus", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PhaseStatus", "Description");
        }
    }
}
