namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CipDetail", "RecordDateStart", c => c.DateTime(nullable: false));
            AddColumn("dbo.CipDetail", "RecordDateEnd", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CipDetail", "RecordDateEnd");
            DropColumn("dbo.CipDetail", "RecordDateStart");
        }
    }
}
