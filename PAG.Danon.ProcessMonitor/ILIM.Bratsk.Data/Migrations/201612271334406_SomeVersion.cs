namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeVersion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountDatas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                        DataID = c.String(),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EventInfoes",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Phases",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WorkMessages",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PhaseStatus",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PlcBuffers",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Status = c.Int(nullable: false),
                        PlcIp = c.Int(nullable: false),
                        PhaseID = c.Int(nullable: false),
                        WorkID = c.Int(nullable: false),
                        WorkID_SD = c.Int(nullable: false),
                        PlcID = c.Int(nullable: false),
                        PhaseStatus = c.Int(nullable: false),
                        EventInfo = c.Int(nullable: false),
                        OperatedID = c.Int(nullable: false),
                        Step_No = c.Int(nullable: false),
                        UnitStatus = c.Int(nullable: false),
                        Bool_0 = c.Boolean(nullable: false),
                        Bool_1 = c.Boolean(nullable: false),
                        Bool_2 = c.Boolean(nullable: false),
                        Bool_3 = c.Boolean(nullable: false),
                        Bool_4 = c.Boolean(nullable: false),
                        Bool_5 = c.Boolean(nullable: false),
                        Bool_6 = c.Boolean(nullable: false),
                        Bool_7 = c.Boolean(nullable: false),
                        Byte_1 = c.Int(nullable: false),
                        Int_1 = c.Int(nullable: false),
                        Int_2 = c.Int(nullable: false),
                        Int_3 = c.Int(nullable: false),
                        Int_4 = c.Int(nullable: false),
                        Int_5 = c.Int(nullable: false),
                        Int_6 = c.Int(nullable: false),
                        Int_7 = c.Int(nullable: false),
                        Int_8 = c.Int(nullable: false),
                        Int_9 = c.Int(nullable: false),
                        Dint_1 = c.Int(nullable: false),
                        Real_1 = c.Single(nullable: false),
                        Real_2 = c.Single(nullable: false),
                        RecordDate = c.DateTime(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Report",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        RequestType = c.Int(nullable: false),
                        ReportAssembly = c.Binary(),
                        ReportTemplate = c.Binary(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserReportRequestParameter",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        UserID = c.String(),
                        RequestParameters = c.String(storeType: "xml"),
                        ReportID = c.Guid(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Report", t => t.ReportID, cascadeDelete: true)
                .Index(t => t.ReportID);
            
            CreateTable(
                "dbo.CommonSettings",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SystemName = c.String(),
                        UserName = c.String(),
                        Value = c.String(),
                        Description = c.String(),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StepNoCIPs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StepNoLactas",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StepNoOVATs",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.StepNoProds",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UnitStatus",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlcN = c.Int(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(),
                        RowVersion = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WorkDetail",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        PlcDateTime = c.DateTime(nullable: false),
                        PlcIWorkID = c.Int(nullable: false),
                        WorkItemId = c.Guid(),
                        MessageID = c.Guid(),
                        EventInfoID = c.Guid(),
                        PhaseStatusID = c.Guid(),
                        OperatedID = c.Int(nullable: false),
                        StepNoProdID = c.Guid(),
                        UnitStatusID = c.Guid(),
                        MaterialID = c.Int(nullable: false),
                        TargetAmount = c.Single(nullable: false),
                        ActualAmount = c.Single(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EventInfoes", t => t.EventInfoID)
                .ForeignKey("dbo.WorkMessages", t => t.MessageID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .ForeignKey("dbo.PhaseStatus", t => t.PhaseStatusID)
                .ForeignKey("dbo.StepNoProds", t => t.StepNoProdID)
                .ForeignKey("dbo.UnitStatus", t => t.UnitStatusID)
                .ForeignKey("dbo.WorkItem", t => t.WorkItemId, cascadeDelete: true)
                .Index(t => t.PhaseID)
                .Index(t => t.WorkItemId)
                .Index(t => t.MessageID)
                .Index(t => t.EventInfoID)
                .Index(t => t.PhaseStatusID)
                .Index(t => t.StepNoProdID)
                .Index(t => t.UnitStatusID);
            
            CreateTable(
                "dbo.WorkItem",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PhaseID = c.Guid(),
                        PlcItemID = c.Int(nullable: false),
                        ParentItemID = c.Guid(),
                        PlcParentItemID = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        PlcDateTime = c.DateTime(nullable: false),
                        IsAutoCreate = c.Boolean(nullable: false),
                        Name = c.String(maxLength: 255),
                        UserCreated = c.Guid(),
                        UserUpdated = c.Guid(),
                        IsSystem = c.Boolean(nullable: false),
                        DateTimeCreate = c.DateTime(nullable: false),
                        DateTimeUpdate = c.DateTime(nullable: false),
                        TimeStamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.WorkItem", t => t.ParentItemID)
                .ForeignKey("dbo.Phases", t => t.PhaseID)
                .Index(t => t.PhaseID)
                .Index(t => t.ParentItemID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkDetail", "WorkItemId", "dbo.WorkItem");
            DropForeignKey("dbo.WorkItem", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.WorkItem", "ParentItemID", "dbo.WorkItem");
            DropForeignKey("dbo.WorkDetail", "UnitStatusID", "dbo.UnitStatus");
            DropForeignKey("dbo.WorkDetail", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.WorkDetail", "PhaseStatusID", "dbo.PhaseStatus");
            DropForeignKey("dbo.WorkDetail", "PhaseID", "dbo.Phases");
            DropForeignKey("dbo.WorkDetail", "MessageID", "dbo.WorkMessages");
            DropForeignKey("dbo.WorkDetail", "EventInfoID", "dbo.EventInfoes");
            DropForeignKey("dbo.UserReportRequestParameter", "ReportID", "dbo.Report");
            DropIndex("dbo.WorkItem", new[] { "ParentItemID" });
            DropIndex("dbo.WorkItem", new[] { "PhaseID" });
            DropIndex("dbo.WorkDetail", new[] { "UnitStatusID" });
            DropIndex("dbo.WorkDetail", new[] { "StepNoProdID" });
            DropIndex("dbo.WorkDetail", new[] { "PhaseStatusID" });
            DropIndex("dbo.WorkDetail", new[] { "EventInfoID" });
            DropIndex("dbo.WorkDetail", new[] { "MessageID" });
            DropIndex("dbo.WorkDetail", new[] { "WorkItemId" });
            DropIndex("dbo.WorkDetail", new[] { "PhaseID" });
            DropIndex("dbo.UserReportRequestParameter", new[] { "ReportID" });
            DropTable("dbo.WorkItem");
            DropTable("dbo.WorkDetail");
            DropTable("dbo.UnitStatus");
            DropTable("dbo.StepNoProds");
            DropTable("dbo.StepNoOVATs");
            DropTable("dbo.StepNoLactas");
            DropTable("dbo.StepNoCIPs");
            DropTable("dbo.CommonSettings");
            DropTable("dbo.UserReportRequestParameter");
            DropTable("dbo.Report");
            DropTable("dbo.PlcBuffers");
            DropTable("dbo.PhaseStatus");
            DropTable("dbo.WorkMessages");
            DropTable("dbo.Phases");
            DropTable("dbo.EventInfoes");
            DropTable("dbo.AccountDatas");
        }
    }
}
