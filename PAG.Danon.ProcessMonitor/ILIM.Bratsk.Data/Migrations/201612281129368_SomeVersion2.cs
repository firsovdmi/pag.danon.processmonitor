namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeVersion2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WorkDetail", "WorkItemId", "dbo.WorkItem");
            DropIndex("dbo.WorkDetail", new[] { "WorkItemId" });
            AlterColumn("dbo.WorkDetail", "WorkItemId", c => c.Guid(nullable: false));
            CreateIndex("dbo.WorkDetail", "WorkItemId");
            AddForeignKey("dbo.WorkDetail", "WorkItemId", "dbo.WorkItem", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkDetail", "WorkItemId", "dbo.WorkItem");
            DropIndex("dbo.WorkDetail", new[] { "WorkItemId" });
            AlterColumn("dbo.WorkDetail", "WorkItemId", c => c.Guid());
            CreateIndex("dbo.WorkDetail", "WorkItemId");
            AddForeignKey("dbo.WorkDetail", "WorkItemId", "dbo.WorkItem", "ID", cascadeDelete: true);
        }
    }
}
