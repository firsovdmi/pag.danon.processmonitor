namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeVersion3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkItem", "MaxPhaseTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkItem", "MaxPhaseTime");
        }
    }
}
