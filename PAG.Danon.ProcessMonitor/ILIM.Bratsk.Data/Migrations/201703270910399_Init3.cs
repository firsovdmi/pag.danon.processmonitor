namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MyPLCBuffers", "DateTimeCreate", c => c.DateTime(nullable: false));
        //    AlterColumn("dbo.MyPLCBuffers", "TimeStamp", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            DropColumn("dbo.MyPLCBuffers", "RowVersion");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MyPLCBuffers", "RowVersion", c => c.String());
         //   AlterColumn("dbo.MyPLCBuffers", "TimeStamp", c => c.Binary());
            AlterColumn("dbo.MyPLCBuffers", "DateTimeCreate", c => c.DateTime(nullable: false));
        }
    }
}
