namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CipDetail", "StepNoProdID", "dbo.StepNoProds");
            DropForeignKey("dbo.CipDetail", "CipItemId", "dbo.CipItem");
            DropIndex("dbo.CipDetail", new[] { "StepNoProdID" });
            RenameColumn(table: "dbo.CipDetail", name: "CipItemId", newName: "CipItem_ID");
            RenameIndex(table: "dbo.CipDetail", name: "IX_CipItemId", newName: "IX_CipItem_ID");
            AddColumn("dbo.EventInfoes", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.MyPLCBuffers", "IP", c => c.Int(nullable: false));
            AddColumn("dbo.Operators", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.Phases", "Class", c => c.Int(nullable: false));
            AddColumn("dbo.Phases", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.PhaseStatus", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.StepNoProds", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.UnitStatus", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.WorkMessages", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.PlcBuffers", "OperatedID", c => c.Int(nullable: false));
            AddColumn("dbo.PlcBuffers", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.StepNoCIPs", "Ip", c => c.Int(nullable: false));
            AddColumn("dbo.CipDetail", "IP", c => c.Int(nullable: false));
            AddColumn("dbo.CipDetail", "WorkID", c => c.Int(nullable: false));
            AddColumn("dbo.CipDetail", "OperatorID", c => c.Guid());
            AddColumn("dbo.CipDetail", "StepNoCIPID", c => c.Guid());
            AddColumn("dbo.CipDetail", "RecordDate", c => c.DateTime(nullable: false));
            CreateIndex("dbo.CipDetail", "OperatorID");
            CreateIndex("dbo.CipDetail", "StepNoCIPID");
            AddForeignKey("dbo.CipDetail", "OperatorID", "dbo.Operators", "ID");
            AddForeignKey("dbo.CipDetail", "StepNoCIPID", "dbo.StepNoCIPs", "ID");
            AddForeignKey("dbo.CipDetail", "CipItem_ID", "dbo.CipItem", "ID");
            DropColumn("dbo.PlcBuffers", "OperatorID");
            DropColumn("dbo.CipDetail", "PlcDateTime");
            DropColumn("dbo.CipDetail", "PlcIWorkID");
            DropColumn("dbo.CipDetail", "OperatedID");
            DropColumn("dbo.CipDetail", "StepNoProdID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CipDetail", "StepNoProdID", c => c.Guid());
            AddColumn("dbo.CipDetail", "OperatedID", c => c.Int(nullable: false));
            AddColumn("dbo.CipDetail", "PlcIWorkID", c => c.Int(nullable: false));
            AddColumn("dbo.CipDetail", "PlcDateTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.PlcBuffers", "OperatorID", c => c.Int(nullable: false));
            DropForeignKey("dbo.CipDetail", "CipItem_ID", "dbo.CipItem");
            DropForeignKey("dbo.CipDetail", "StepNoCIPID", "dbo.StepNoCIPs");
            DropForeignKey("dbo.CipDetail", "OperatorID", "dbo.Operators");
            DropIndex("dbo.CipDetail", new[] { "StepNoCIPID" });
            DropIndex("dbo.CipDetail", new[] { "OperatorID" });
            DropColumn("dbo.CipDetail", "RecordDate");
            DropColumn("dbo.CipDetail", "StepNoCIPID");
            DropColumn("dbo.CipDetail", "OperatorID");
            DropColumn("dbo.CipDetail", "WorkID");
            DropColumn("dbo.CipDetail", "IP");
            DropColumn("dbo.StepNoCIPs", "Ip");
            DropColumn("dbo.PlcBuffers", "Ip");
            DropColumn("dbo.PlcBuffers", "OperatedID");
            DropColumn("dbo.WorkMessages", "Ip");
            DropColumn("dbo.UnitStatus", "Ip");
            DropColumn("dbo.StepNoProds", "Ip");
            DropColumn("dbo.PhaseStatus", "Ip");
            DropColumn("dbo.Phases", "Ip");
            DropColumn("dbo.Phases", "Class");
            DropColumn("dbo.Operators", "Ip");
            DropColumn("dbo.MyPLCBuffers", "IP");
            DropColumn("dbo.EventInfoes", "Ip");
            RenameIndex(table: "dbo.CipDetail", name: "IX_CipItem_ID", newName: "IX_CipItemId");
            RenameColumn(table: "dbo.CipDetail", name: "CipItem_ID", newName: "CipItemId");
            CreateIndex("dbo.CipDetail", "StepNoProdID");
            AddForeignKey("dbo.CipDetail", "CipItemId", "dbo.CipItem", "ID", cascadeDelete: true);
            AddForeignKey("dbo.CipDetail", "StepNoProdID", "dbo.StepNoProds", "ID");
        }
    }
}
