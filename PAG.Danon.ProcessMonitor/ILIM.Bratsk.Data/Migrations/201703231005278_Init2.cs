namespace PAG.Danon.ProcessMonitor.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.MyPLCBuffers", name: "StepNoProd_ID", newName: "StepNoProdID");
            RenameIndex(table: "dbo.MyPLCBuffers", name: "IX_StepNoProd_ID", newName: "IX_StepNoProdID");
            DropColumn("dbo.MyPLCBuffers", "StepNoID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MyPLCBuffers", "StepNoID", c => c.Guid());
            RenameIndex(table: "dbo.MyPLCBuffers", name: "IX_StepNoProdID", newName: "IX_StepNoProd_ID");
            RenameColumn(table: "dbo.MyPLCBuffers", name: "StepNoProdID", newName: "StepNoProd_ID");
        }
    }
}
