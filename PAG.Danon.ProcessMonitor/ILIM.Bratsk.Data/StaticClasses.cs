﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PAG.WBD.Milk.Data
{
    public static class StaticClasses
    {
        public static T GetValue<T>(this SqlDataReader reader, string fieldName, T defaultVal = default(T))
        {
            return reader.IsDBNull(reader.GetOrdinal(fieldName)) ? defaultVal : (T)Convert.ChangeType(reader[fieldName], typeof(T));
        }

        public static T GetValue<T>(this SqlDataReader reader, int fieldNumber, T defaultVal = default(T))
        {
            return reader.IsDBNull(fieldNumber) ? defaultVal : (T)Convert.ChangeType(reader[fieldNumber], typeof(T));
        }

        public static int GetHashCodeByProperties<T>(this T obj)
        {
            var type = typeof(T);

            var hashCode = 0;

            foreach (var item in type.GetProperties())
            {
                var val = item.GetValue(obj, null);

                if (val != null)
                    hashCode ^= val.GetHashCode();
            }

            return hashCode;
        }
    }
}
