﻿using System;
using System.Linq;
using System.Linq.Expressions;
using PAG.Danon.ProcessMonitor.Infrastructure;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using RefactorThis.GraphDiff;

namespace PAG.Danon.ProcessMonitor.Data
{
    public interface IUnitOfWorkDO : IUnitOfWork
    {
        IQueryable<T> Get<T>() where T : class, IEntity;
        IQueryable<T> GetTracked<T>() where T : class, IEntity;
        T Create<T>(T entity) where T : class, IEntity;
        T Update<T>(T entity) where T : class, IEntity;
        T CreateOrUpdate<T>(T entity) where T : class, IEntity;
        void Delete<T>(T entity) where T : class, IEntity;
        T Update<T>(T entity, Expression<Func<IUpdateConfiguration<T>, object>> config) where T : class, IEntity, new();
    }
}