﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using PAG.Danon.ProcessMonitor.Data.MapConfiguration;
using PAG.Danon.ProcessMonitor.Domain.Model;
using PAG.Danon.ProcessMonitor.Infrastructure.AAA.AuthenticationClasses;
using PAG.Danon.ProcessMonitor.Infrastructure.DateTimeManager;
using PAG.Danon.ProcessMonitor.Infrastructure.Model;
using RefactorThis.GraphDiff;

namespace PAG.Danon.ProcessMonitor.Data
{
    public class FactoryContext : DbContext, IUnitOfWorkDO
    {
        private readonly IActiveAccountManager _activeAccountManager;
        private readonly IDateTimeManager _dateTimeManager;

        public FactoryContext(IActiveAccountManager activeAccountManager, string connectionString)
            : this(new DateTimeManager(), activeAccountManager, connectionString)
        {
          
        }

        public FactoryContext(IDateTimeManager dateTimeManager, IActiveAccountManager activeAccountManager,
            string connectionString)
            : base(connectionString)
        {
            _dateTimeManager = dateTimeManager;
            _activeAccountManager = activeAccountManager;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }

      


        public DbSet<Report> Reports { get; set; }
        public DbSet<UserReportRequestParameters> UsersReportRequestParameters { get; set; }
        public DbSet<AccountData> AccountDatas { get; set; }
        public DbSet<ApplicationSettings> Settings { get; set; }
        public DbSet<PlcBuffer> PlcBuffers { get; set; }
        public DbSet<Phase> Phase { get; set; }
        public DbSet<WorkMessage> PhaseID { get; set; }
        public DbSet<EventInfo> EventInfo { get; set; }
        public DbSet<PhaseStatus> PhaseStatus { get; set; }
        public DbSet<UnitStatus> UnitStatus { get; set; }
        public DbSet<StepNoProd> StepNoProd { get; set; }
        public DbSet<StepNoCip> StepNoCIP { get; set; }
        public DbSet<StepNoLacta> StepNoLacta { get; set; }
        public DbSet<StepNoOVAT> StepNoOVAT { get; set; }
        public DbSet<WorkItem> WorkItem { get; set; }
        public DbSet<WorkDetail> WorkDetail { get; set; }
        public DbSet<MyPLCBuffer> MyPlcBuffers { get; set; }
        public DbSet<Operator> Operator { get; set; }
        public DbSet<CipContur> CipContur { get; set; }
        public DbSet<CipDetail> CipDetail { get; set; }
        public DbSet<ReceptionPhase> ReceptionPhase { get; set; }
        public DbSet<ReceptionDetails> ReceptionDetails { get; set; }
        public DbSet<ReceptionCipDetail> ReceptionCipDetail { get; set; }
        public DbSet<ReceptionCIPPhase> ReceptionCIPPhase { get; set; }

        public DbSet<MXOPhase> MXOPhase { get; set; }
        public DbSet<AppPhase> AppPhase { get; set; }
        public DbSet<CheesePhase> CheesePhase { get; set; }

        public DbSet<MXOCIPPhase> MXOCIPPhase { get; set; }
        public DbSet<AppCIPPhase> AppCIPPhase { get; set; }
        public DbSet<CheeseCIPPhase> CheeseCIPPhase { get; set; }

        public DbSet<MXODetails> MXODetails { get; set; }
        public DbSet<MXOCipDetail> MXOCipDetail { get; set; }

        public DbSet<AppDetails> AppDetails { get; set; }
        public DbSet<AppCipDetail> AppCipDetail { get; set; }

        public DbSet<CheesenDetails> CheesenDetails { get; set; }
        public DbSet<CheeseCipDetail> CheeseCipDetail { get; set; }

        public DbSet<PhasovkaPhase> PhasovkaPhase { get; set; }
        public DbSet<PhasovkaDetails> PhasovkaDetails { get; set; }
        public DbSet<PhasovkaCIPPhase> PhasovkaCIPPhase { get; set; }
        public DbSet<PhasovkaCipDetails> PhasovkaCipDetails { get; set; }
        #region Implementation of IUnitOfWork


        public DbSet<ProgrammNumber> ProgrammNumber { get; set; }
        public void Save()
        {
            SaveChanges();
        }

        #endregion

        #region Overrides of DbContext

        /// <summary>
        ///     This method is called when the model for a derived context has been initialized, but
        ///     before the model has been locked down and used to initialize the context.  The default
        ///     implementation of this method does nothing, but it can be overridden in a derived class
        ///     such that the model can be further configured before it is locked down.
        /// </summary>
        /// <remarks>
        ///     Typically, this method is called only once when the first instance of a derived context
        ///     is created.  The model for that context is then cached and is for all further instances of
        ///     the context in the app domain.  This caching can be disabled by setting the ModelCaching
        ///     property on the given ModelBuidler, but note that this can seriously degrade performance.
        ///     More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        ///     classes directly.
        /// </remarks>
        /// <param name="modelBuilder">The builder that defines the model for the context being created. </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Ignore<Entity>();
            modelBuilder.Ignore<EntityBase>();
            //modelBuilder.Configurations.Add(new AccountDataMapConfiguration());
            modelBuilder.Configurations.Add(new ApplicationSettingsSettingsMapConfiguration());
            modelBuilder.Configurations.Add(new UserReportRequestParametersMapConfiguration());
            modelBuilder.Configurations.Add(new ReportMapConfiguration());
            modelBuilder.Configurations.Add(new WorkDetailsMapConfiguration());
            modelBuilder.Configurations.Add(new WorkItemsMapConfiguration());
            modelBuilder.Configurations.Add(new CipDetailsMapConfiguration());
            modelBuilder.Configurations.Add(new CipItemsMapConfiguration());
            modelBuilder.Configurations.Add(new MyPLCBufferMapConfiguration());
            modelBuilder.Configurations.Add(new ReceptionDetailsMapConfiguration());

            modelBuilder.Configurations.Add(new MXODetailsMapConfiguration());
            modelBuilder.Configurations.Add(new AppDetailsMapConfiguration());
            modelBuilder.Configurations.Add(new CheesenDetailsMapConfiguration());
            modelBuilder.Configurations.Add(new PhasovkaDetailsMapConfiguration());

            modelBuilder.Configurations.Add(new MXOCipDetailsMapConfiguration());
            modelBuilder.Configurations.Add(new AppCipDetailsMapConfiguration());
            modelBuilder.Configurations.Add(new CheeseCipDetailsMapConfiguration());
            modelBuilder.Configurations.Add(new PhasovkaCipDetailsMapConfiguration());

            modelBuilder.Configurations.Add(new ReceptionCipDetailsMapConfiguration());
        }

        #endregion

        static object lockObject = new object();
        public override int SaveChanges()
        {
            lock (lockObject)
            {
                var saveTime = _dateTimeManager.GetDateTimeNow().ToUniversalTime();
                var activeAccountID = _activeAccountManager == null
                    ? null
                    : _activeAccountManager.ActiveAccount == null
                        ? null
                        : _activeAccountManager.ActiveAccount.Account == null
                            ? (Guid?) null
                            : _activeAccountManager.ActiveAccount.Account.ID;
                foreach (
                    var entry in
                        ChangeTracker.Entries()
                            .Where(p => p.Entity is Entity)
                            .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
                {
                    var entityWithVersion = entry.Entity as Entity;
                    
                    if (entityWithVersion != null)
                    {
                        if (entityWithVersion.TimeStamp == null) entityWithVersion.TimeStamp = new byte[] { 0x0 };
                        entityWithVersion.DateTimeUpdate = saveTime;
                        entityWithVersion.UserUpdated = activeAccountID;
                        if (Entry(entityWithVersion).State == EntityState.Added)
                        {
                            entityWithVersion.DateTimeCreate = saveTime;
                            entityWithVersion.UserCreated = activeAccountID;
                        }
                    }
                }
                return base.SaveChanges();
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        #region Implementation of IUnitOfWorkDO

        public IQueryable<T> Get<T>() where T : class, IEntity
        {
            return Set<T>().AsNoTracking();
        }

        public IQueryable<T> GetTracked<T>() where T : class, IEntity
        {
            return Set<T>();
        }

        public T Create<T>(T entity) where T : class, IEntity
        {
            if (entity.ID == Guid.Empty) entity.ID = Guid.NewGuid();
            if (Entry(entity).State == EntityState.Detached)
                Set<T>().Add(entity);
            return entity;
        }

        public T Update<T>(T entity) where T : class, IEntity
        {
            //var existEntity = Set<T>().FirstOrDefault(p => p.ID == entity.ID);
            //if (existEntity != null)
            //{
            //    Set<T>().Remove(existEntity);
            //    Set<T>().Add(entity);
            //}
            Set<T>().Attach(entity);
            var entry = Entry(entity);

            entry.State = EntityState.Modified;
            return entity;
        }

        public T CreateOrUpdate<T>(T entity) where T : class, IEntity
        {
            if (Set<T>().Any(p => p.ID == entity.ID))
            {
                Update(entity);
            }
            else
            {
                Create(entity);
            }
            return entity;
        }

        public void Delete<T>(T entity) where T : class, IEntity
        {
            Set<T>().Attach(entity);
            if (Entry(entity).State != EntityState.Deleted)
                Set<T>().Remove(entity);
        }


        public T Update<T>(T entity, Expression<Func<IUpdateConfiguration<T>, object>> config)
            where T : class, IEntity, new()
        {
            this.UpdateGraph(entity, config);
            return entity;
        }

        #endregion
    }
}