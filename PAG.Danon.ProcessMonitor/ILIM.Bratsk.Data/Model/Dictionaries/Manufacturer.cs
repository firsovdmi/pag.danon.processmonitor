﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAG.WBD.Milk.Data.Model.Dictionaries
{
    public class Manufacturer : BaseDictionary, IManufacturer
    {
    }
    public interface IManufacturer : IBaseDictionary
    {
    }
}
