﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PAG.WBD.Milk.Data.Model.Dictionaries
{
    public class BaseDictionary : IBaseDictionary
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
    }

    public interface IBaseDictionary
    {
        int Id { get; set; }
        string Name { get; set; }
        bool IsDeleted { get; set; }
    }
}
