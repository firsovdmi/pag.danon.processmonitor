﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.WBD.Milk.Data.Model.Dictionaries;


namespace PAG.WBD.Milk.Data.Model
{
    public class Tank : ITank
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ISort Sort { get; set; }
        public IThermalResistance ThermalResistance { get; set; }
        public float AcidLab { get; set; }
        public float DensityLab { get; set; }
        public float FatLab { get; set; }
        public float ProteinLab { get; set; }
    }

    public interface ITank
    {
        int Id { get; set; }
        string Name { get; set; }
        ISort Sort { get; set; }
        IThermalResistance ThermalResistance { get; set; }
        float AcidLab { get; set; }
        float DensityLab { get; set; }
        float FatLab { get; set; }
        float ProteinLab { get; set; }
    }
}
