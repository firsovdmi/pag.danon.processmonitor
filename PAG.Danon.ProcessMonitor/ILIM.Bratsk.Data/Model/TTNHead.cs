﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Data.Model
{
    public class TtnHead : ITtnHead
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string RawCode { get; set; }
        public string Client { get; set; }
        public string Invoice { get; set; }
        public IContract Contract { get; set; }
        public IManufacturer Manufacturer { get; set; }
        public bool IsSelf { get; set; }
        public IFioDriver FioDrivers { get; set; }
        public IStaff Master { get; set; }
        public IStaff Operator { get; set; }
        public IEnumerable<ISection> Section { get; set; }
    }
    public interface ITtnHead
    {
        int Id { get; set; }
        int Number { get; set; }
        string RawCode { get; set; }
        string Client { get; set; }
        string Invoice { get; set; }
        IContract Contract { get; set; }
        IManufacturer Manufacturer { get; set; }
        bool IsSelf { get; set; }
        IFioDriver FioDrivers { get; set; }
        IStaff Master { get; set; }
        IStaff Operator { get; set; }
        IEnumerable<ISection> Section { get; set; }
    }
}
