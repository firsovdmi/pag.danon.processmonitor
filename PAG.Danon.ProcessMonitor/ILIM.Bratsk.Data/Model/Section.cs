﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PAG.WBD.Milk.Data.Model.Dictionaries;

namespace PAG.WBD.Milk.Data.Model
{
    public class Section : ISection
    {
        public int Id { get; set; }
        public float AcidTtn { get; set; }
        public float DensityTtn { get; set; }
        public float FatTtn { get; set; }
        public float ProteinTtn { get; set; }
        public IThermalResistance ThermalResistance { get; set; }
        public IFarm Farm { get; set; }
        public ISectionsName SectionN { get; set; }
        public string CarNumber { get; set; }
        public float TtnVolume { get; set; }
        public ISort Sort { get; set; }
        public float AcidLab { get; set; }
        public float DensityLab { get; set; }
        public float FatLab { get; set; }
        public float ProteinLab { get; set; }
        public IRecivingPost RecommendationRecivePost { get; set; }
        public IEnumerable<ISection> SubSections { get; set; }

        public bool IsGroup
        {
            get { return SubSections != null && SubSections.Any(); }
        }
        public string DisplayName
        {
            get
            {
                if (SubSections == null || !SubSections.Any()) return SectionN.Name;
                return string.Join(", ", SubSections.Select(p => p.SectionN.Name).ToArray());
            }
        }
    }

    public interface ISection
    {
        int Id { get; set; }
        float AcidTtn { get; set; }
        float DensityTtn { get; set; }
        float FatTtn { get; set; }
        float ProteinTtn { get; set; }
        IThermalResistance ThermalResistance { get; set; }
        IFarm Farm { get; set; }
        ISectionsName SectionN { get; set; } 
        string CarNumber { get; set; }
        float TtnVolume { get; set; }
        ISort Sort { get; set; }
        float AcidLab { get; set; }
        float DensityLab { get; set; }
        float FatLab { get; set; }
        float ProteinLab { get; set; }
        IRecivingPost RecommendationRecivePost { get; set; }
        IEnumerable<ISection> SubSections { get; set; }
        bool IsGroup { get; }
        string DisplayName { get; }
    }
}
