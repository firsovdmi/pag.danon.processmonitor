﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace PAG.Danon.ProcessMonitor.HostService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            new ServiceController(serviceInstaller1.ServiceName).Start();
        }

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
        }

        private void serviceProcessInstaller1_BeforeInstall(object sender, InstallEventArgs e)
        {
            //serviceProcessInstaller1.Account= ServiceAccount.User;
            //serviceProcessInstaller1.Username = @".\Администратор";
            //serviceProcessInstaller1.Password = @"1Q2w3e4r";
        }
    }
}