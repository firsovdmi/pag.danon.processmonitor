﻿using System.ServiceProcess;
using System.Threading;
using Castle.Windsor;
using PAG.Danon.ProcessMonitor.Domain;
using PAG.Danon.ProcessMonitor.Service;

namespace PAG.Danon.ProcessMonitor.HostService
{
    public partial class OPCService : ServiceBase
    {
        public OPCService()
        {
            InitializeComponent();
        }

        public void OnDebug()
        {
            OnStart(null);
            Thread.Sleep(Timeout.Infinite);
        }

        protected override void OnStart(string[] args)
        {
            var container = new WindsorContainer();
            container.Install(new RemoteServiceInstaller());
            container.ResolveAll<IModule>();
        }

        protected override void OnStop()
        {
        }
    }
}