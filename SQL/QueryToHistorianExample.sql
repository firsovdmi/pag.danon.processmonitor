SET NOCOUNT ON
DECLARE @StartDate DateTime
DECLARE @EndDate DateTime
SET @StartDate = DateAdd(dd,-1,GetDate())
SET @EndDate = GetDate()
SET NOCOUNT OFF
SELECT * FROM (
SELECT DateTime,  TagName, Value, QualityDetail
 FROM INSQL.Runtime.dbo.History
 WHERE History.TagName IN ('IOI_ALCIP_K_Out_Flow_MV')
 AND wwRetrievalMode = 'full'
 --AND wwCycleCount = 100
 --AND wwQualityRule = 'Extended'
 AND wwVersion = 'Latest'
 AND DateTime >= @StartDate
 AND DateTime <= @EndDate) temp
 LEFT JOIN INSQL.Runtime.dbo.QualityMap qm ON qm.QualityDetail = temp.QualityDetail
 WHERE temp.DateTime >= @StartDate AND qm.QualityString =N'Good'


